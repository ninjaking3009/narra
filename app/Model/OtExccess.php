<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property Group $Group
 * @property AccountType $AccountType
 */
class OtExccess extends AppModel {

	public $recursive = -1;

	public $actsAs = array('Containable');

	public function bind($model = array('Group')) {
	  $this->bindModel(array(
	    'belongsTo' => array(
	      'User' => array(
	        'className' => 'User',
	        'foreignKey' => 'user_id',
	        'conditions' => false,
	        'dependent' => false,
	        'fields' => '',
	        'order' => ''
	      )
	    )
	  ),false);
	  $this->contain($model);
	}

	public function getExccessAttendance($weeks,$dept,$year='')
	{
		$this->bind(array('User'));
		$this->virtualFields['count'] = 'Count(OtExccess.id)';
		$this->virtualFields['totalWorkHours'] = 'SUM(OtExccess.hours_work)';
		$this->virtualFields['totalBasicPay'] = 'SUM(OtExccess.per_hour)';
		$this->virtualFields['totalOtPay'] = 'SUM(OtExccess.ot_pay)';
		$this->virtualFields['totalOthrs'] = 'SUM(OtExccess.ot_hours)';
		$attendances = $this->find('all',array(
			'conditions' => array(
				'OtExccess.week' => $weeks,
				'User.department_id' => $dept,
				'OtExccess.year' => $year,
			),
			'group' => 'user_id'
		));
		return $attendances;
	}

	public function getExcessTotal($weeks,$year,$dept)
	{
		App::uses('NumberHelper', 'View/Helper');
		$number = new NumberHelper(new View());

		$this->virtualFields['totalWorkHours'] = 'SUM(OtExccess.hours_work)';
		$this->virtualFields['totalBasicPay'] = 'SUM(OtExccess.per_hour)';
		$this->virtualFields['totalOtHours'] = 'SUM(OtExccess.ot_hours)';
		$this->virtualFields['totalOtPay'] = 'SUM(OtExccess.ot_pay)';

		$this->virtualFields['totalCola'] = 'SUM(OtExccess.cola)';
		$this->virtualFields['totalSea'] = 'SUM(OtExccess.sea)';
		$this->bind(array('User'));
		$value = $this->find('first',array(
			'fields' => array(
				'OtExccess.totalWorkHours',
				'OtExccess.totalBasicPay',
				'OtExccess.totalOtHours',
				'OtExccess.totalOtPay',
				'OtExccess.totalCola',
				'OtExccess.totalSea',
			),
			'conditions' => array(
				'OtExccess.week' => $weeks,
				'User.department_id' => $dept,
				'OtExccess.year' => $year,
			)
		));
		$value['OtExccess']['total'] = $value['OtExccess']['totalOtPay'] + $value['OtExccess']['totalBasicPay'];
		$value['Attendance']['grossPay'] = $number->currency($value['OtExccess']['total'],'');
		$value['Attendance']['netPay'] = $number->currency($value['OtExccess']['total'],'');
		$value['Attendance']['grandTotal'] = $number->currency($value['OtExccess']['total'],'');
		$value['Attendance']['totalBasicPay'] = $number->currency($value['OtExccess']['totalBasicPay'],'');
		$value['Attendance']['totalWorkHours'] = $value['OtExccess']['totalWorkHours'];
		$value['Attendance']['totalOtHours'] = $value['OtExccess']['totalOtHours'];
		$value['Attendance']['totalOtPay'] = $number->currency($value['OtExccess']['totalOtPay'],'');
		$value['Attendance']['totalNightPay'] = 0;
		$value['Attendance']['totalCola'] = 0;
		$value['Attendance']['totalSea'] = 0;
		$value['Attendance']['totalOmitHours'] = 0;
		$value['Attendance']['totalOmitPay'] = 0;
		$value['Attendance']['totalDeductHours'] = 0;
		$value['Attendance']['totalDeductPay'] = 0;
		$value['Attendance']['totalDeductBasics'] = 0;
		$value['Attendance']['totalLegalPay'] = 0;
		$value['Attendance']['totalLegalHours'] = 0;
		$value['Attendance']['totalLpHours'] = 0;
		$value['Attendance']['totalLpPay'] = 0;
		$value['Attendance']['totalOtLpHours'] = 0;
		$value['Attendance']['totalOtLpPay'] = 0;
		$value['Attendance']['totalSpecialHours'] = 0;
		$value['Attendance']['totalSpecialPay'] = 0;
		$value['Attendance']['totalNightHours'] = 0;
		$value['Attendance']['totalSss'] = 0;
		$value['Attendance']['totalPhilhealth'] = 0;
		$value['Attendance']['totalPagibig'] = 0;
		$value['Attendance']['totalDays'] = 0;
		$value['Attendance']['grandDedudction']['Deduction']['sss'] = 0;
		$value['Attendance']['grandDedudction']['Deduction']['philhealth'] = 0;
		$value['Attendance']['grandDedudction']['Deduction']['pagibig'] = 0;
		$value['Attendance']['grandSss'] = array();
		$value['Attendance']['grandSss']['Deduction']['sss'] = 0;
		$value['Attendance']['grandPhilhealth'] = array();
		$value['Attendance']['grandSss']['Deduction']['philhealth'] = 0;
		$value['Attendance']['grandPagibig'] = array();
		$value['Attendance']['grandSss']['Deduction']['pagibig'] = 0;
		$value['Attendance']['totalDeductions'] = 0;
		$value['Attendance']['sss_loan'] = 0;
		$value['Attendance']['pagibig_loan'] = 0;
		$value['Attendance']['calamity_loan'] = 0;
		$value['Attendance']['advance'] = 0;
		return $value;
	}

	public function getExccess($weeks,$dept,$year='')
	{
		$attendances = $this->getExccessAttendance($weeks,$dept,$year);
		App::uses('NumberHelper', 'View/Helper');
		$number = new NumberHelper(new View());
		foreach ($attendances as $key => &$value) {

			$value['Days'] = $this->find('all',array('conditions' => array(
				'user_id' => $value['OtExccess']['user_id'],
				'OtExccess.week' => $weeks,
				'OtExccess.year' => $year,
			),'group' => 'id'));


			foreach ($value['Days'] as $key1 => &$value1) {
				$value1['Attendance']['hours_work'] = $value1['OtExccess']['hours_work'];
				$value1['Attendance']['night_hours'] = 0;
				$value1['Attendance']['is_night'] = 0;
				$value1['Attendance']['legal_hours'] = 0;
				$value1['Attendance']['ot_work'] = $value1['OtExccess']['ot_hours'];
				$value1['Attendance']['date'] = $value1['OtExccess']['date'];
				$value1['Attendance']['omit_realdate'] = '';
				unset($value1['OtExccess']);
			}

			$value['OtExccess']['total'] = $value['OtExccess']['totalOtPay'] + $value['OtExccess']['totalBasicPay'];
			$value['Attendance']['grossPay'] = $number->currency($value['OtExccess']['total'],'');
			$value['Attendance']['netPay'] = $number->currency($value['OtExccess']['total'],'');
			$value['Attendance']['grandTotal'] = $number->currency($value['OtExccess']['total'],'');
			$value['Attendance']['totalBasicPay'] = $number->currency($value['OtExccess']['totalBasicPay'],'');
			$value['Attendance']['totalWorkHours'] = $value['OtExccess']['totalWorkHours'];
			$value['Attendance']['totalOtHours'] = $value['OtExccess']['totalOthrs'];
			$value['Attendance']['totalOtPay'] = $number->currency($value['OtExccess']['totalOtPay'],'');
			$value['Attendance']['totalNightPay'] = 0;
			$value['Attendance']['totalCola'] = 0;
			$value['Attendance']['totalSea'] = 0;
			$value['Attendance']['totalOmitHours'] = 0;
			$value['Attendance']['totalOmitPay'] = 0;
			$value['Attendance']['totalDeductHours'] = 0;
			$value['Attendance']['totalDeductPay'] = 0;
			$value['Attendance']['totalDeductBasics'] = 0;
			$value['Attendance']['totalLegalPay'] = 0;
			$value['Attendance']['totalLegalHours'] = 0;
			$value['Attendance']['totalLpHours'] = 0;
			$value['Attendance']['totalLpPay'] = 0;
			$value['Attendance']['totalOtLpHours'] = 0;
			$value['Attendance']['totalOtLpPay'] = 0;
			$value['Attendance']['totalSpecialHours'] = 0;
			$value['Attendance']['totalSpecialPay'] = 0;
			$value['Attendance']['totalNightHours'] = 0;
			$value['Attendance']['totalSss'] = 0;
			$value['Attendance']['totalPhilhealth'] = 0;
			$value['Attendance']['totalPagibig'] = 0;
			$value['Attendance']['totalDays'] = 0;
			$value['Attendance']['grandSss'] = array();
			$value['Attendance']['grandSss']['Deduction']['sss'] = 0;
			$value['Attendance']['grandPhilhealth'] = array();
			$value['Attendance']['grandSss']['Deduction']['philhealth'] = 0;
			$value['Attendance']['grandPagibig'] = array();
			$value['Attendance']['grandSss']['Deduction']['pagibig'] = 0;
			$value['Attendance']['totalDeductions'] = 0;
			$value['Attendance']['sss_loan'] = 0;
			$value['Attendance']['pagibig_loan'] = 0;
			$value['Attendance']['calamity_loan'] = 0;
			$value['Attendance']['advance'] = 0;
		}
		return $attendances;
	}

}