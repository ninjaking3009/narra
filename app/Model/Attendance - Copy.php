<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');


/**
 * User Model
 *
 * @property Group $Group
 * @property AccountType $AccountType
 */
class Attendance extends AppModel {

	public $recursive = -1;

	public $actsAs = array('Containable');

	public function bind($model = array('Group')) {
	  $this->bindModel(array(
	    'belongsTo' => array(
	      'User' => array(
	        'className' => 'User',
	        'foreignKey' => 'user_id',
	        'conditions' => false,
	        'dependent' => false,
	        'fields' => '',
	        'order' => ''
	      ),
	      'Role' => array(
	        'className' => 'Role',
	        'foreignKey' => 'position',
	        'conditions' => false,
	        'dependent' => false,
	        'fields' => '',
	        'order' => ''
	      ),
	    )
	  ),false);
	  $this->contain($model);
	}

	public function getTotal($userid,$day,$week,$year)
	{
		$this->virtualFields['grandTotal'] = 'SUM(Attendance.grandtotal)';
		$this->virtualFields['totalBasicPay'] = 'SUM(Attendance.total)';
		$this->virtualFields['totalWorkHours'] = 'SUM(Attendance.hours_work)';
		$this->virtualFields['totalOtHours'] = 'SUM(Attendance.ot_work)';
		$this->virtualFields['totalOtPay'] = 'SUM(Attendance.ot_pay)';
		$this->virtualFields['totalNightPay'] = 'SUM(Attendance.night_pay)';
		$this->virtualFields['totalCola'] = 'SUM(Attendance.cola)';
		$this->virtualFields['totalSea'] = 'SUM(Attendance.sea)';
		$this->virtualFields['totalLegalPay'] = 'SUM(Attendance.legal_pay)';
		$this->virtualFields['totalOmitHours'] = 'SUM(Attendance.omit_hours)';
		$this->virtualFields['totalOmitPay'] = 'SUM(Attendance.omit_pay)';
		$this->virtualFields['totalLegalHours'] = 'SUM(Attendance.legal_hours)';
		$this->virtualFields['totalNightHours'] = 'SUM(Attendance.night_hours)';
		$data = $this->find('first',array('fields' => array('Attendance.grandTotal','Attendance.totalOmitPay','Attendance.totalLegalPay','Attendance.totalOmitHours','Attendance.totalLegalHours','Attendance.totalNightHours','Attendance.totalBasicPay','Attendance.totalWorkHours','Attendance.totalOtHours','Attendance.totalOtPay','Attendance.totalNightPay','Attendance.totalCola','Attendance.totalSea'), 
			'conditions' => array(
				'Attendance.user_id' => $userid,
				'Attendance.week' => $week, 
			)));


		$this->bind(array('User'));
		$this->User->bind(array('Role'));
		$empinfo = $this->User->findById($userid);
		if(!empty($data['Attendance']['totalCola']) ) {
			$data['Attendance']['grossPay'] = $data['Attendance']['grandTotal'] + $data['Attendance']['totalOtPay'] + $data['Attendance']['totalNightPay'] + $data['Attendance']['totalSea'] + $data['Attendance']['totalCola'];

			$sss = ClassRegistry::init('Ss')->query("SELECT `Ss`.`id`, `Ss`.`from`, `Ss`.`to`, `Ss`.`premium`, `Ss`.`er`, `Ss`.`ee`, `Ss`.`ecc`, `Ss`.`total`, `Ss`.`date_added` FROM `narradb`.`sses` AS `Ss`   WHERE `SS`.`premium` != '' AND `Ss`.`from` <= ".$data['Attendance']['grossPay']." AND `Ss`.`to` >= ".$data['Attendance']['grossPay']."");
			$data['Attendance']['totalSss']  = $sss[0]['Ss']['premium'];

			$ph = ClassRegistry::init('Philhealth')->query("SELECT `Philhealth`.`id`, `Philhealth`.`from`, `Philhealth`.`to`, `Philhealth`.`premium`, `Philhealth`.`er`, `Philhealth`.`ee`, `Philhealth`.`ecc`, `Philhealth`.`total`, `Philhealth`.`date_added` FROM `narradb`.`philhealths` AS `Philhealth`   WHERE `Philhealth`.`premium` != '' AND `Philhealth`.`from` <= ".$data['Attendance']['grossPay']." AND `Philhealth`.`to` >= ".$data['Attendance']['grossPay']."");
			$data['Attendance']['totalPhilhealth']  = $ph[0]['Philhealth']['premium'];

			$data['Attendance']['totalPagibig']  = $empinfo['Role']['per_hour'] * .20;

			App::uses('NumberHelper', 'View/Helper');
			$number = new NumberHelper(new View());

			$paidloan = ClassRegistry::init('Paidloan')->find('first',array(
				'conditions' => array(
					'Paidloan.user_id' => $userid,
					'Paidloan.week' => $week,
					'Paidloan.year' => $year,
				)
			));
			
			if(!isset($paidloan['Paidloan'])) {
				$paidloan['Paidloan']['sss_loan'] = 0;
				$paidloan['Paidloan']['pagibig_loan'] = 0;
				$paidloan['Paidloan']['calamity_loan'] = 0;
				$paidloan['Paidloan']['advance'] = 0;
			}
			
			$totalDeductions = $paidloan['Paidloan']['sss_loan'] + $paidloan['Paidloan']['pagibig_loan'] + $paidloan['Paidloan']['calamity_loan'] + $data['Attendance']['totalPagibig'] + $data['Attendance']['totalPhilhealth'] + $data['Attendance']['totalSss'];
			$data['Attendance']['netPay'] = $data['Attendance']['grossPay'] - $totalDeductions;

			$data['Attendance']['totalBasicPay'] = $number->currency($data['Attendance']['totalBasicPay'], '');
			$data['Attendance']['totalOtPay'] = $number->currency($data['Attendance']['totalOtPay'], '');
			$data['Attendance']['totalOmitPay'] = $number->currency($data['Attendance']['totalOmitPay'], '');
			$data['Attendance']['totalNightPay'] = $number->currency($data['Attendance']['totalNightPay'], '');
			$data['Attendance']['totalCola'] = $number->currency($data['Attendance']['totalCola'], '');
			$data['Attendance']['totalSea'] = $number->currency($data['Attendance']['totalSea'], '');
			$data['Attendance']['grossPay'] = $number->currency($data['Attendance']['grossPay'], '');
			$data['Attendance']['totalSss'] = $number->currency($data['Attendance']['totalSss'], '');
			$data['Attendance']['totalPhilhealth'] = $number->currency($data['Attendance']['totalPhilhealth'], '');
			$data['Attendance']['totalPagibig'] = $number->currency($data['Attendance']['totalPagibig'], '');
			$data['Attendance']['netPay'] = $number->currency($data['Attendance']['netPay'], '');
			$data['Attendance']['totalLegalPay'] = $number->currency($data['Attendance']['totalLegalPay'], '');
			if(!empty($paidloan)) {
				$data['Attendance']['sss_loan'] = $number->currency($paidloan['Paidloan']['sss_loan'], '');
				$data['Attendance']['pagibig_loan'] = $number->currency($paidloan['Paidloan']['pagibig_loan'], '');
				$data['Attendance']['calamity_loan'] = $number->currency($paidloan['Paidloan']['calamity_loan'], '');
				$data['Attendance']['advance'] = $number->currency($paidloan['Paidloan']['advance'], '');
			} else {
				$data['Attendance']['sss_loan'] = 0;
				$data['Attendance']['pagibig_loan'] = 0;
				$data['Attendance']['calamity_loan'] = 0;
				$data['Attendance']['advance'] = 0;
			}

		} else {
			$data['Attendance']['totalBasicPay'] = 0;
			$data['Attendance']['totalWorkHours'] = 0;
			$data['Attendance']['totalOtHours'] = 0;
			$data['Attendance']['totalOtPay'] = 0;
			$data['Attendance']['totalNightPay'] = 0;
			$data['Attendance']['totalCola'] = 0;
			$data['Attendance']['totalSea'] = 0;
			$data['Attendance']['grossPay'] = 0;
			$data['Attendance']['totalSss'] = 0;
			$data['Attendance']['totalPhilhealth'] = 0;
			$data['Attendance']['totalPagibig'] = 0;
			$data['Attendance']['netPay'] = 0;
			$data['Attendance']['totalLegalPay'] = 0;
			$data['Attendance']['totalOmitHours'] = 0;
			$data['Attendance']['totalLegalHours'] = 0;
			$data['Attendance']['totalNightHours'] = 0;
			$data['Attendance']['sss_loan'] = 0;
			$data['Attendance']['pagibig_loan'] = 0;
			$data['Attendance']['calamity_loan'] = 0;
			$data['Attendance']['advance'] = 0;
		}

		return $data;

	}

	public function getTotalMonth($userid,$day,$week,$year)
	{
		$this->virtualFields['grandTotal'] = 'SUM(Attendance.grandtotal)';
		$this->virtualFields['totalBasicPay'] = 'SUM(Attendance.total)';
		$this->virtualFields['totalWorkHours'] = 'SUM(Attendance.hours_work)';
		$this->virtualFields['totalOtHours'] = 'SUM(Attendance.ot_work)';
		$this->virtualFields['totalOtPay'] = 'SUM(Attendance.ot_pay)';
		$this->virtualFields['totalNightPay'] = 'SUM(Attendance.night_pay)';
		$this->virtualFields['totalCola'] = 'SUM(Attendance.cola)';
		$this->virtualFields['totalSea'] = 'SUM(Attendance.sea)';
		$this->virtualFields['totalLegalPay'] = 'SUM(Attendance.legal_pay)';
		$this->virtualFields['totalOmitHours'] = 'SUM(Attendance.omit_hours)';
		$this->virtualFields['totalOmitPay'] = 'SUM(Attendance.omit_pay)';
		$this->virtualFields['totalLegalHours'] = 'SUM(Attendance.legal_hours)';
		$this->virtualFields['totalNightHours'] = 'SUM(Attendance.night_hours)';
		$data = $this->find('first',array('fields' => array('Attendance.grandTotal','Attendance.totalOmitPay','Attendance.totalLegalPay','Attendance.totalOmitHours','Attendance.totalLegalHours','Attendance.totalNightHours','Attendance.totalBasicPay','Attendance.totalWorkHours','Attendance.totalOtHours','Attendance.totalOtPay','Attendance.totalNightPay','Attendance.totalCola','Attendance.totalSea'), 
			'conditions' => array(
				'Attendance.user_id' => $userid,
				'Attendance.month' => $week, 
			)));


		$this->bind(array('User'));
		$this->User->bind(array('Role'));
		$empinfo = $this->User->findById($userid);
		if(!empty($data['Attendance']['totalCola']) ) {
			$data['Attendance']['grossPay'] = $data['Attendance']['grandTotal'] + $data['Attendance']['totalOtPay'] + $data['Attendance']['totalNightPay'] + $data['Attendance']['totalSea'] + $data['Attendance']['totalCola'];

			$sss = ClassRegistry::init('Ss')->query("SELECT `Ss`.`id`, `Ss`.`from`, `Ss`.`to`, `Ss`.`premium`, `Ss`.`er`, `Ss`.`ee`, `Ss`.`ecc`, `Ss`.`total`, `Ss`.`date_added` FROM `narradb`.`sses` AS `Ss`   WHERE `SS`.`premium` = '' AND `Ss`.`from` <= ".$data['Attendance']['grossPay']." AND `Ss`.`to` >= ".$data['Attendance']['grossPay']."");
			$data['Attendance']['totalSss']  = $sss[0]['Ss']['premium'];

			$ph = ClassRegistry::init('Philhealth')->query("SELECT `Philhealth`.`id`, `Philhealth`.`from`, `Philhealth`.`to`, `Philhealth`.`premium`, `Philhealth`.`er`, `Philhealth`.`ee`, `Philhealth`.`ecc`, `Philhealth`.`total`, `Philhealth`.`date_added` FROM `narradb`.`philhealths` AS `Philhealth`   WHERE `Philhealth`.`premium` = '' AND `Philhealth`.`from` <= ".$data['Attendance']['grossPay']." AND `Philhealth`.`to` >= ".$data['Attendance']['grossPay']."");
			$data['Attendance']['totalPhilhealth']  = $ph[0]['Philhealth']['premium'];

			$data['Attendance']['totalPagibig']  = $empinfo['Role']['per_hour'] * .20;

			App::uses('NumberHelper', 'View/Helper');
			$number = new NumberHelper(new View());

			$paidloan = ClassRegistry::init('Paidloan')->find('first',array(
				'conditions' => array(
					'Paidloan.user_id' => $userid,
					'Paidloan.week' => $week,
					'Paidloan.year' => $year,
				)
			));
			
			if(!isset($paidloan['Paidloan'])) {
				$paidloan['Paidloan']['sss_loan'] = 0;
				$paidloan['Paidloan']['pagibig_loan'] = 0;
				$paidloan['Paidloan']['calamity_loan'] = 0;
				$paidloan['Paidloan']['advance'] = 0;
			}
			
			$totalDeductions = $paidloan['Paidloan']['sss_loan'] + $paidloan['Paidloan']['pagibig_loan'] + $paidloan['Paidloan']['calamity_loan'] + $data['Attendance']['totalPagibig'] + $data['Attendance']['totalPhilhealth'] + $data['Attendance']['totalSss'];
			$data['Attendance']['netPay'] = $data['Attendance']['grossPay'] - $totalDeductions;

			$data['Attendance']['totalBasicPay'] = $number->currency($data['Attendance']['totalBasicPay'], '');
			$data['Attendance']['totalOtPay'] = $number->currency($data['Attendance']['totalOtPay'], '');
			$data['Attendance']['totalOmitPay'] = $number->currency($data['Attendance']['totalOmitPay'], '');
			$data['Attendance']['totalNightPay'] = $number->currency($data['Attendance']['totalNightPay'], '');
			$data['Attendance']['totalCola'] = $number->currency($data['Attendance']['totalCola'], '');
			$data['Attendance']['totalSea'] = $number->currency($data['Attendance']['totalSea'], '');
			$data['Attendance']['grossPay'] = $number->currency($data['Attendance']['grossPay'], '');
			$data['Attendance']['totalSss'] = $number->currency($data['Attendance']['totalSss'], '');
			$data['Attendance']['totalPhilhealth'] = $number->currency($data['Attendance']['totalPhilhealth'], '');
			$data['Attendance']['totalPagibig'] = $number->currency($data['Attendance']['totalPagibig'], '');
			$data['Attendance']['netPay'] = $number->currency($data['Attendance']['netPay'], '');
			$data['Attendance']['totalLegalPay'] = $number->currency($data['Attendance']['totalLegalPay'], '');
			if(!empty($paidloan)) {
				$data['Attendance']['sss_loan'] = $number->currency($paidloan['Paidloan']['sss_loan'], '');
				$data['Attendance']['pagibig_loan'] = $number->currency($paidloan['Paidloan']['pagibig_loan'], '');
				$data['Attendance']['calamity_loan'] = $number->currency($paidloan['Paidloan']['calamity_loan'], '');
				$data['Attendance']['advance'] = $number->currency($paidloan['Paidloan']['advance'], '');
			} else {
				$data['Attendance']['sss_loan'] = 0;
				$data['Attendance']['pagibig_loan'] = 0;
				$data['Attendance']['calamity_loan'] = 0;
				$data['Attendance']['advance'] = 0;
			}
		


		} else {
			$data['Attendance']['totalBasicPay'] = 0;
			$data['Attendance']['totalWorkHours'] = 0;
			$data['Attendance']['totalOtHours'] = 0;
			$data['Attendance']['totalOtPay'] = 0;
			$data['Attendance']['totalNightPay'] = 0;
			$data['Attendance']['totalCola'] = 0;
			$data['Attendance']['totalSea'] = 0;
			$data['Attendance']['grossPay'] = 0;
			$data['Attendance']['totalSss'] = 0;
			$data['Attendance']['totalPhilhealth'] = 0;
			$data['Attendance']['totalPagibig'] = 0;
			$data['Attendance']['netPay'] = 0;
			$data['Attendance']['totalLegalPay'] = 0;
			$data['Attendance']['totalOmitHours'] = 0;
			$data['Attendance']['totalLegalHours'] = 0;
			$data['Attendance']['totalNightHours'] = 0;
			$data['Attendance']['sss_loan'] = 0;
			$data['Attendance']['pagibig_loan'] = 0;
			$data['Attendance']['calamity_loan'] = 0;
			$data['Attendance']['advance'] = 0;
		}

		return $data;

	}

	public function getTotalday($day,$week)
	{
		$this->virtualFields['grandTotal'] = 'SUM(Attendance.grandtotal)';
		$this->virtualFields['totalEmployee'] = 'COUNT(Attendance.id)';
		$this->virtualFields['totalAbsent'] = 'SUM(Attendance.is_absent)';
		$this->virtualFields['totalBasicPay'] = 'SUM(Attendance.total)';
		$this->virtualFields['totalWorkHours'] = 'SUM(Attendance.hours_work)';
		$this->virtualFields['totalOtHours'] = 'SUM(Attendance.ot_work)';
		$this->virtualFields['totalOtPay'] = 'SUM(Attendance.ot_pay)';
		$this->virtualFields['totalOmitPay'] = 'SUM(Attendance.omit_pay)';
		$this->virtualFields['totalNightPay'] = 'SUM(Attendance.night_pay)';
		$this->virtualFields['totalCola'] = 'SUM(Attendance.cola)';
		$this->virtualFields['totalSea'] = 'SUM(Attendance.sea)';
		$this->virtualFields['totalSss'] = 'SUM(Attendance.sss)';
		$this->virtualFields['totalPhilhealth'] = 'SUM(Attendance.philhealth)';
		$this->virtualFields['totalPagibig'] = 'SUM(Attendance.pagibig)';
		$this->virtualFields['totalLegalPay'] = 'SUM(Attendance.legal_pay)';
		$this->virtualFields['totalOmitHours'] = 'SUM(Attendance.omit_hours)';
		$this->virtualFields['totalLegalHours'] = 'SUM(Attendance.legal_hours)';
		$this->virtualFields['totalNightHours'] = 'SUM(Attendance.night_hours)';
		$data = $this->find('first',array('fields' => array('Attendance.grandTotal','Attendance.totalOmitPay','Attendance.totalLegalPay','Attendance.totalOmitHours','Attendance.totalLegalHours','Attendance.totalNightHours','Attendance.totalBasicPay','Attendance.totalWorkHours','Attendance.totalOtHours','Attendance.totalOtPay','Attendance.totalNightPay','Attendance.totalCola','Attendance.totalSea','Attendance.totalSss','Attendance.totalPhilhealth','Attendance.totalPagibig','Attendance.totalEmployee','Attendance.totalAbsent'), 
			'conditions' => array(
				'OR' => array('Attendance.omit_realdate' => $day,'Attendance.date' => $day),
				'Attendance.week' => $week, 
			)));

		$this->virtualFields['totalEmployee'] = 'Attendance.id';
		$totalAttendee = $this->find('count',array('fields' => array('Attendance.totalEmployee'),
			'conditions' => array(
				//'OR' => array('Attendance.omit_realdate' => $day,'Attendance.date' => $day),
				'Attendance.date' => $day,
				'Attendance.week' => $week, 
			),
			'group' => 'Attendance.user_id'
			));

		if(!empty($totalAttendee)) {
			$data['Attendance']['totalEmployee'] =  $totalAttendee;
		} else {
			$data['Attendance']['totalEmployee'] =  0;
		}

		if(!empty($data['Attendance']['totalEmployee'])) {
			$data['Attendance']['grossPay'] = $data['Attendance']['grandTotal'] + $data['Attendance']['totalOtPay'] + $data['Attendance']['totalNightPay'] + $data['Attendance']['totalSea'] + $data['Attendance']['totalCola'];

			$totalDeductions = $data['Attendance']['totalPagibig'] + $data['Attendance']['totalPhilhealth'] + $data['Attendance']['totalSss'];
			$data['Attendance']['netPay'] = $data['Attendance']['grossPay'] - $totalDeductions;

			App::uses('NumberHelper', 'View/Helper');
			$number = new NumberHelper(new View());

			$data['Attendance']['totalBasicPay'] = $number->currency($data['Attendance']['totalBasicPay'], '');
			$data['Attendance']['totalOmitPay'] = $number->currency($data['Attendance']['totalOmitPay'], '');
			$data['Attendance']['totalOtPay'] = $number->currency($data['Attendance']['totalOtPay'], '');
			$data['Attendance']['totalNightPay'] = $number->currency($data['Attendance']['totalNightPay'], '');
			$data['Attendance']['totalCola'] = $number->currency($data['Attendance']['totalCola'], '');
			$data['Attendance']['totalSea'] = $number->currency($data['Attendance']['totalSea'], '');
			$data['Attendance']['grossPay'] = $number->currency($data['Attendance']['grossPay'], '');
			$data['Attendance']['totalSss'] = $number->currency($data['Attendance']['totalSss'], '');
			$data['Attendance']['totalPhilhealth'] = $number->currency($data['Attendance']['totalPhilhealth'], '');
			$data['Attendance']['totalPagibig'] = $number->currency($data['Attendance']['totalPagibig'], '');
			$data['Attendance']['netPay'] = $number->currency($data['Attendance']['netPay'], '');
			$data['Attendance']['totalLegalPay'] = $number->currency($data['Attendance']['totalLegalPay'], '');

		} else {
			$data['Attendance']['totalBasicPay'] = 0;
			$data['Attendance']['totalWorkHours'] = 0;
			$data['Attendance']['totalOtHours'] = 0;
			$data['Attendance']['totalOtPay'] = 0;
			$data['Attendance']['totalNightPay'] = 0;
			$data['Attendance']['totalCola'] = 0;
			$data['Attendance']['totalSea'] = 0;
			$data['Attendance']['totalAbsent'] = 0;
			$data['Attendance']['totalEmployee'] = 0;
			$data['Attendance']['grossPay'] = 0;
			$data['Attendance']['totalSss'] = 0;
			$data['Attendance']['totalPhilhealth'] = 0;
			$data['Attendance']['totalPagibig'] = 0;
			$data['Attendance']['netPay'] = 0;
			$data['Attendance']['totalLegalPay'] = 0;
			$data['Attendance']['totalOmitHours'] = 0;
			$data['Attendance']['totalLegalHours'] = 0;
			$data['Attendance']['totalNightHours'] = 0;
		}

		return $data;

	}

	public function getTotalweek($week)
	{
		$this->virtualFields['grandTotal'] = 'SUM(Attendance.grandtotal)';
		$this->virtualFields['totalEmployee'] = 'COUNT(Attendance.id)';
		$this->virtualFields['totalAbsent'] = 'SUM(Attendance.is_absent)';
		$this->virtualFields['totalBasicPay'] = 'SUM(Attendance.total)';
		$this->virtualFields['totalWorkHours'] = 'SUM(Attendance.hours_work)';
		$this->virtualFields['totalOtHours'] = 'SUM(Attendance.ot_work)';
		$this->virtualFields['totalOtPay'] = 'SUM(Attendance.ot_pay)';
		$this->virtualFields['totalOmitPay'] = 'SUM(Attendance.omit_pay)';
		$this->virtualFields['totalNightPay'] = 'SUM(Attendance.night_pay)';
		$this->virtualFields['totalCola'] = 'SUM(Attendance.cola)';
		$this->virtualFields['totalSea'] = 'SUM(Attendance.sea)';
		$this->virtualFields['totalSss'] = 'SUM(Attendance.sss)';
		$this->virtualFields['totalPhilhealth'] = 'SUM(Attendance.philhealth)';
		$this->virtualFields['totalPagibig'] = 'SUM(Attendance.pagibig)';
		$this->virtualFields['totalLegalPay'] = 'SUM(Attendance.legal_pay)';
		$this->virtualFields['totalOmitHours'] = 'SUM(Attendance.omit_hours)';
		$this->virtualFields['totalLegalHours'] = 'SUM(Attendance.legal_hours)';
		$this->virtualFields['totalNightHours'] = 'SUM(Attendance.night_hours)';
		
		$data = $this->find('first',array('fields' => array('Attendance.grandTotal','Attendance.totalOmitPay','Attendance.totalLegalPay','Attendance.totalOmitHours','Attendance.totalLegalHours','Attendance.totalNightHours','Attendance.totalBasicPay','Attendance.totalWorkHours','Attendance.totalOtHours','Attendance.totalOtPay','Attendance.totalNightPay','Attendance.totalCola','Attendance.totalSea','Attendance.totalSss','Attendance.totalPhilhealth','Attendance.totalPagibig','Attendance.totalEmployee','Attendance.totalAbsent'), 
				'conditions' => array(
				'Attendance.week' => $week, 
			)));

		$totalAttendee = $this->find('count',array('fields' => array('Attendance.user_id'),
				'conditions' => array(
				'Attendance.week' => $week, 
			),
			'group' => 'Attendance.user_id'
		));

		if(!empty($totalAttendee)) {
			$data['Attendance']['totalEmployee'] =  $totalAttendee;
		} else {
			$data['Attendance']['totalEmployee'] = 0;
		}

		if(!empty($data['Attendance']['totalEmployee'])) {
			$data['Attendance']['grossPay'] = $data['Attendance']['grandTotal'] + $data['Attendance']['totalOtPay'] + $data['Attendance']['totalNightPay'] + $data['Attendance']['totalSea'] + $data['Attendance']['totalCola'];

			$totalDeductions = $data['Attendance']['totalPagibig'] + $data['Attendance']['totalPhilhealth'] + $data['Attendance']['totalSss'];
			$data['Attendance']['netPay'] = $data['Attendance']['grossPay'] - $totalDeductions;
			
			App::uses('NumberHelper', 'View/Helper');
			$number = new NumberHelper(new View());
			
			$data['Attendance']['totalBasicPay'] = $number->currency($data['Attendance']['totalBasicPay'], '');
			$data['Attendance']['totalOtPay'] = $number->currency($data['Attendance']['totalOtPay'], '');
			$data['Attendance']['totalNightPay'] = $number->currency($data['Attendance']['totalNightPay'], '');
			$data['Attendance']['totalCola'] = $number->currency($data['Attendance']['totalCola'], '');
			$data['Attendance']['totalSea'] = $number->currency($data['Attendance']['totalSea'], '');
			$data['Attendance']['grossPay'] = $number->currency($data['Attendance']['grossPay'], '');
			$data['Attendance']['totalSss'] = $number->currency($data['Attendance']['totalSss'], '');
			$data['Attendance']['totalPhilhealth'] = $number->currency($data['Attendance']['totalPhilhealth'], '');
			$data['Attendance']['totalPagibig'] = $number->currency($data['Attendance']['totalPagibig'], '');
			$data['Attendance']['netPay'] = $number->currency($data['Attendance']['netPay'], '');
			$data['Attendance']['totalLegalPay'] = $number->currency($data['Attendance']['totalLegalPay'], '');
			$data['Attendance']['totalOmitPay'] = $number->currency($data['Attendance']['totalOmitPay'], '');

		} else {
			$data['Attendance']['totalBasicPay'] = 0;
			$data['Attendance']['totalWorkHours'] = 0;
			$data['Attendance']['totalOtHours'] = 0;
			$data['Attendance']['totalOtPay'] = 0;
			$data['Attendance']['totalNightPay'] = 0;
			$data['Attendance']['totalCola'] = 0;
			$data['Attendance']['totalSea'] = 0;
			$data['Attendance']['grossPay'] = 0;
			$data['Attendance']['totalSss'] = 0;
			$data['Attendance']['totalPhilhealth'] = 0;
			$data['Attendance']['totalPagibig'] = 0;
			$data['Attendance']['netPay'] = 0;
			$data['Attendance']['totalAbsent'] = 0;
			$data['Attendance']['totalEmployee'] = 0;
			$data['Attendance']['totalLegalPay'] = 0;
			$data['Attendance']['totalOmitHours'] = 0;
			$data['Attendance']['totalLegalHours'] = 0;
			$data['Attendance']['totalNightHours'] = 0;
		}

		return $data;

	}

	public function getWeeklyAttendance($week,$dept,$year)
	{
		$this->bind('User');
		$users = $this->find('all',array(
		'fields' => array('Attendance.user_id'),
		'conditions' => array(
			'Attendance.week' => $week,
			'User.department_id' => $dept,
			'Attendance.year' => $year,
		),
		'group' => 'Attendance.user_id',
		'order' => 'User.lname'
		));

		$data = array();
		foreach ($users as $key => $value) {
			$tempDay = $this->find('all',array(
			'fields' => array('Attendance.hours_work','Attendance.night_hours','Attendance.legal_hours','Attendance.date','Attendance.omit_realdate'),
			'conditions' => array(
				'Attendance.user_id' => $value['Attendance']['user_id'],
				'Attendance.week' => $week,
				'Attendance.year' => $year,
			),
			'order' => 'Attendance.date'
			));

			$temp = $this->getTotal($value['Attendance']['user_id'],'',$week,$year);

			$this->bind(array('User'));
			$tempUser = $this->User->findById($value['Attendance']['user_id']);
			$data[$key]['User'] = $tempUser['User'];
			$data[$key]['Attendance'] = $temp['Attendance'];
			$data[$key]['Days'] = $tempDay;
		}
		return $data;
	}

	public function getMonthlyAttendance($month,$year='')
	{

		if($year == '') {
			$year = date('Y');
		}

		$this->bind('User');
		$users = $this->find('all',array(
		'fields' => array('Attendance.user_id'),
		'conditions' => array(
			'Attendance.month' => $month,
			'Attendance.year' => $year,
		),
		'group' => 'Attendance.user_id',
		'order' => 'User.lname'
		));

		$data = array();
		foreach ($users as $key => $value) {

			$temp = $this->getTotalMonth($value['Attendance']['user_id'],'',$month,$year);

			$this->bind(array('User'));
			$tempUser = $this->User->findById($value['Attendance']['user_id']);
			$data[$key]['User'] = $tempUser['User'];
			$data[$key]['Attendance'] = $temp['Attendance'];
		}
		return $data;
	}

	public function getOmit($week)
	{
		$this->bind(array('User'));
		$data = $this->find('all',array('conditions' => array('Attendance.week' => $week, 'Attendance.is_omit' => 1)));

		foreach ($data as $key => &$value) {
			if($value['Attendance']['is_reliever'] == 1) {
				$this->bind(array('Role'));
				$value['r'] = $this->Role->findById($value['Attendance']['reliever_position']);
			} else {
				$this->bind(array('Role'));
				$value['r'] = $this->Role->findById($value['Attendance']['position']);
			}
		}
		return $data;
	}

	public function checkForHolidays($data,$type,$isId = false)
	{
		if($type == 'week') {
			foreach ($data as $key => &$value) {
				$holiday = ClassRegistry::init('Holiday')->find('first',array('conditions' => array('Holiday.date' => strtotime($value))));
				if(!empty($holiday)) {
					if($holiday['Holiday']['is_legal'] == 1) {
						$value = $value." | Legal Holiday";
					} else {
						$value = $value." | Holiday";

					}
				}
			}
		} else {
			$temp = ClassRegistry::init('Holiday')->find('first',array('conditions' => array('Holiday.date' => $data)));
			if(!empty($temp)) {
				if($temp['Holiday']['is_legal'] == 1) {
					$temp = date("D m d, Y",$data)." | Legal Holiday";
				} else {
					$temp = date("D m d, Y",$data)." | Holiday";

				}
				$data = $temp;
				if($isId) {
					$data = true;
				}
			} else {
				if($isId) {
					$data = false;
				}
			}
		}
		return $data;
	}

	public function procChecker($data='')
	{
		// this is to remove unwanted field if the checker doesn't exist
		if(!isset($data['Attendance']['is_omit'])) {
			if(isset($data['Attendance']['omitDate'])) {
				unset($data['Attendance']['omitDate']);
			}
		}

		// this is to remove unwanted field if the checker doesn't exist
		if(!isset($data['Attendance']['is_reliever'])) {
			if(isset($data['Attendance']['reliever_position'])) {
				unset($data['Attendance']['reliever_position']);
			}
		}

		// this is to remove unwanted field if the checker doesn't exist
		if(!isset($data['Attendance']['is_ot'])) {
			$data['Attendance']['ot_work'] = 0;
			$data['Attendance']['ot_pay'] = 0;
		}

		return $data;

	}

	public function absentChecker($data)
	{
		if(isset($data['Attendance']['is_absent'])) {
			$data['Attendance']['hours_work'] = 0;
			$data['Attendance']['is_absent'] = 1;
		}

		return $data;
	}

	public function relieverChecker($data)
	{

		if(isset($data['Attendance']['is_reliever'])) {
				$data['Attendance']['is_reliever'] = 1;
				$newPosition = ClassRegistry::init('Role')->findById($data['Attendance']['reliever_position']);
				$data['Attendance']['per_hour'] = $newPosition['Role']['per_hour'];
		}

		return $data;
	}

	public function restdayChecker($data)
	{
		if(isset($data['Attendance']['is_restday'])) {

			if(isset($data['Attendance']['is_omit'])) {
				$user = $this->bind(array('User'));

			} else {
				$data['Attendance']['is_restday'] = 1;
				$data['Attendance']['per_hour'] = $data['Attendance']['per_hour'] * 1.30;
			}

		}

		return $data;
	}

	public function otChecker($data)
	{
		if(isset($data['Attendance']['is_ot'])) {
			$data['Attendance']['is_ot'] = 1;
			$otHours = $data['Attendance']['ot_work'];
			$basicPay = $data['Attendance']['per_hour'];
			$data['Attendance']['ot_pay'] = (($basicPay * 1.25) / 8) * $otHours;
		} else {
			$data['Attendance']['ot_pay'] = 0;
		}

		return $data;
	}

	public function nightChecker($data)
	{
		if(isset($data['Attendance']['is_night'])) {
			$data['Attendance']['is_night'] = 1;
			$data['Attendance']['night_pay'] = $data['Attendance']['total'] * .10;
		} else {
			 $data['Attendance']['night_pay'] = 0;
		}

		return $data;
	}

	public function getAttendanceTotal($data)
	{
		$data['Attendance']['total'] = ($data['Attendance']['per_hour'] / 8) * $data['Attendance']['hours_work'];

		return $data;
	}

	public function getInsentive($data,$holiday)
	{

		$hrsWork = $data['Attendance']['hours_work'];

		if($hrsWork >= 8) {
			$cola = 12.50;
			$sea = 13;
		} else {
			$cola = (12.50 / 8) * $hrsWork;
			$sea = (13 / 8) * $hrsWork;
		}

		if(!empty($holiday)) {
			if($holiday['Holiday']['is_legal'] == 1) {
				$cola = $cola * 2;
				$sea = $sea * 2;
			}
		}

		$data['Attendance']['cola'] = $cola;
		$data['Attendance']['sea'] = $sea;

		return $data;

	}


	public function getGrossPay($data)
	{
		$data['Attendance']['grosspay'] = $data['Attendance']['cola'] + $data['Attendance']['sea'] + $data['Attendance']['ot_pay'] + $data['Attendance']['total'] + $data['Attendance']['night_pay'];
		return $data;
	}

	public function getDeductions($data)
	{

		$gross = $data['Attendance']['grosspay'];

		$sss = ClassRegistry::init('Ss')->query("SELECT `Ss`.`id`, `Ss`.`from`, `Ss`.`to`, `Ss`.`premium`, `Ss`.`er`, `Ss`.`ee`, `Ss`.`ecc`, `Ss`.`total`, `Ss`.`date_added` FROM `narradb`.`sses` AS `Ss`   WHERE `SS`.`premium` != '' AND `Ss`.`from` <= ".$gross." AND `Ss`.`to` >= ".$gross."");
		
		$data['Attendance']['sss']  = $sss[0]['Ss']['premium'];

		$ph = ClassRegistry::init('Ss')->query("SELECT `Philhealth`.`id`, `Philhealth`.`from`, `Philhealth`.`to`, `Philhealth`.`premium`, `Philhealth`.`er`, `Philhealth`.`ee`, `Philhealth`.`ecc`, `Philhealth`.`total`, `Philhealth`.`date_added` FROM `narradb`.`philhealths` AS `Philhealth`   WHERE `Philhealth`.`premium` != '' AND `Philhealth`.`from` <= ".$gross." AND `Philhealth`.`to` >= ".$gross."");

		$data['Attendance']['philhealth'] = $ph[0]['Philhealth']['premium'];

		$data['Attendance']['pagibig'] = $data['Attendance']['per_hour'] * 0.02;

		return $data;

	}

	public function holidayChecker($data,$holiday)
	{

		if(!isset($data['Attendance']['is_lastpayroll'])) {
			if(!empty($holiday)) {
				$data['Attendance']['is_holiday'] = 1;
				if($holiday['Holiday']['is_legal'] == 1) {
					$data['Attendance']['per_hour'] = $data['Attendance']['per_hour'] * 2;
					$data['Attendance']['is_legal'] = 1;
				} else {
					$data['Attendance']['per_hour'] = $data['Attendance']['per_hour'] * 1.30;
				}

			}
		}

		return $data;
	}

	public function lastpayrollChecker($data)
	{
		if(isset($data['Attendance']['is_lastpayroll'])) {
			$data['Attendance']['is_lastpayroll'] = 1;
			$data['Attendance']['per_hour'] = $data['Attendance']['per_hour'] * 2;
		}
		return $data;
	}

	public function omitCheck($data)
	{
		
		if(isset($data['Attendance']['is_omit'])) {
			$omitdate = $data['Attendance']['omitDate'];

			unset($data['Attendance']['omitDate']);

			$data['Attendance']['omit_realdate'] = $data['Attendance']['date'];

			$omitdate = explode("-", $omitdate);
			$omitdate = $omitdate[2]."-".$omitdate[0]."-".$omitdate[1];
			$data['Attendance']['date'] = strtotime($omitdate);
		}
		
		return $data;

	}

	public function finalCheck($data) {


		if(isset($data['Attendance']['is_lastpayroll'])) {
			if(!isset($data['Attendance']['is_omit'])) {
				$data['Attendance']['legal_hours'] = $data['Attendance']['hours_work'];
				$data['Attendance']['grandtotal'] = $data['Attendance']['legal_pay'] = $data['Attendance']['total'];
			}
		} 

		if(isset($data['Attendance']['is_omit'])) {
			$data['Attendance']['omit_hours'] = $data['Attendance']['hours_work'];
			$data['Attendance']['grandtotal'] = $data['Attendance']['omit_pay'] = $data['Attendance']['total'];
		} else {
			$data['Attendance']['grandtotal'] = $data['Attendance']['total'];
		}

		if(isset($data['Attendance']['is_night'])) {
			if(!isset($data['Attendance']['is_omit'])) {
				$data['Attendance']['night_hours'] = $data['Attendance']['hours_work'];
				$data['Attendance']['hours_work'] = 0;
			}
		}

		if(isset($data['Attendance']['is_omit']) || isset($data['Attendance']['is_lastpayroll'])) {
			$data['Attendance']['hours_work'] = 0;
			$data['Attendance']['total'] = 0;
		}

		return $data;

	}

	public function loanChecker($data,$isLastWeek)
	{

		$loan = ClassRegistry::init('Loan')->find('first',array(
			'conditions' => array(
				'Loan.user_id' => $data['Attendance']['user_id']
			)
		));

		$paidloan = ClassRegistry::init('Paidloan')->find('first',array(
			'conditions' => array(
				'Paidloan.user_id' => $data['Attendance']['user_id'],
				'Paidloan.week' => $data['Attendance']['week']
			)
		));

		if(!empty($loan) && empty($paidloan)) {
			$prevTotal = $this->_getPervAttendanceTotal($data['Attendance']['user_id'],$data['Attendance']['week'],$data['Attendance']['year']);
			
			$netpay = $prevTotal['Attendance']['gross'] - $prevTotal['Attendance']['deduction'];

			if($isLastWeek) {
				$sssloan = $loan['Loan']['sss_loan'];
				$pagibigloan = $loan['Loan']['pagibig_loan'];
				$calamityloan = $loan['Loan']['calamity_loan'];
			} else {
				$sssloan = $loan['Loan']['totalsss_loan'] / 4;
				$pagibigloan = $loan['Loan']['totalpagibig_loan'] / 4;
				$calamityloan = $loan['Loan']['totalcalamity_loan'] / 4;
			}
			
			$totalLoan = $sssloan + $pagibigloan + $calamityloan;
		
			$totalNet = $netpay - $totalLoan;
			echo $totalNet;
			if($totalNet > 100) {

				$pl['Paidloan']['user_id'] = $data['Attendance']['user_id'];
				$pl['Paidloan']['week'] = $data['Attendance']['week'];
				$pl['Paidloan']['year'] = $data['Attendance']['year'];
				$pl['Paidloan']['sss_loan'] = $sssloan;
				$pl['Paidloan']['pagibig_loan'] = $pagibigloan;
				$pl['Paidloan']['calamity_loan'] = $calamityloan;

				if($isLastWeek) {
					$loan['Loan']['sss_loan'] = 0;
					$loan['Loan']['pagibig_loan'] = 0;
					$loan['Loan']['calamity_loan'] = 0;
					$loan['Loan']['totalsss_loan'] = 0;
					$loan['Loan']['totalpagibig_loan'] = 0;
					$loan['Loan']['totalcalamity_loan'] = 0;
				} else {
					$loan['Loan']['sss_loan'] -= $sssloan;
					$loan['Loan']['pagibig_loan'] -= $pagibigloan;
					$loan['Loan']['calamity_loan'] -= $calamityloan;
				}
				
				ClassRegistry::init('Loan')->save($loan);
				ClassRegistry::init('Paidloan')->create();
				ClassRegistry::init('Paidloan')->save($pl);

			}

		}

	}

	public function _getPervAttendanceTotal($userid,$week,$year)
	{
		$this->virtualFields['gross'] = 'SUM(Attendance.grandtotal) + SUM(Attendance.night_pay) + SUM(Attendance.ot_pay) + SUM(Attendance.cola) + SUM(Attendance.sea)';
		$this->virtualFields['deduction'] = 'SUM(Attendance.sss) + SUM(Attendance.philhealth) + SUM(Attendance.pagibig)';
		$data = $this->find('first',array('fields' => array('Attendance.deduction','Attendance.gross'), 
			'conditions' => array(
				'Attendance.user_id' => $userid,
				'Attendance.week' => $week,
				'Attendance.year' => $year
		)));

		return $data;

	}

}