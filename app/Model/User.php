<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property Group $Group
 * @property AccountType $AccountType
 */
class User extends AppModel {

  public $recursive = -1;

  public $actsAs = array('Containable');

  var $virtualFields = array(
      'fullname' => 'CONCAT(User.fname, " ", User.lname)'
  );

  public function bind($model = array('Group')) {
    $this->bindModel(array(
      'belongsTo' => array(
        'Role' => array(
          'className' => 'Role',
          'foriegnKey' => 'role_id',
        ),
        'Department' => array(
          'className' => 'Department',
          'foriegnKey' => 'department_id',
        )
      ),
      'hasMany' => array(
        'Attendance' => array(
          'className' => 'Attendance',
          'foriegnKey' => 'user_id',
        )
      )
    ),false);
    $this->contain($model);
  }

  public $validate = array(

    'email' => array(
      'email' => array(
        'rule' => array('email'),
      ),
      'unique' => array(
        'rule'    => 'isUnique',
        'message' => 'This Email has already been taken.'
      ),
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'You must enter a email.'
      )
    ),
    'fname' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'You must enter a first name.'
      )
    ),
    'lname' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'You must enter a last name.'
      )
    ),
    'repass' => array(
      'equaltofield' => array(
      'rule' => array('equaltofield','password'),
      'message' => 'Password does not match.',
      //'allowEmpty' => false,
      //'required' => false,
      //'last' => false, // Stop validation after this rule
      'on' => 'create', // Limit validation to 'create' or 'update' operations
      )
    ),
    'renewpass' => array(
      'equaltofield' => array(
      'rule' => array('equaltofieldnew','newpassword'),
      'message' => 'Password does not match.',
      'allowEmpty' => true,
      'required' => false,
      //'last' => false, // Stop validation after this rule
      )
    )
  );

  function equaltofield($check,$otherfield)
  {
      //get name of field
      $fname = '';
      foreach ($check as $key => $value){
          $fname = $key;
          break;
      }
      return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
  }

  function equaltofieldnew($check,$otherfield)
  {
      $fname = '';
      foreach ($check as $key => $value){
          $fname = $key;
          break;
      }
      if($this->data[$this->name][$otherfield] === $this->data[$this->name][$fname]) {
        $this->data[$this->name]['password'] = $this->data[$this->name][$fname];
        return true;
      } else {
        return false;
      }
  }

  public function passHasher($pass) {
    $passwordHasher = new SimplePasswordHasher();
    $pass = $passwordHasher->hash($pass);
    return $pass;
  }

  public function beforeSave($options = array()) {
      if (isset($this->data[$this->alias]['password'])) {
          $passwordHasher = new SimplePasswordHasher();
          $this->data[$this->alias]['password'] = $passwordHasher->hash(
              $this->data[$this->alias]['password']
          );
      }
      $this->data[$this->alias]['date_added'] = date("Y-m-d H:i:s");
      return true;
  }


  public function _search($keyword='')
  {

    $this->bind(array('Role'));

    $keyword = str_replace(' ', '', $keyword);

    $users = $this->find('all',array('conditions' => array('User.empid' => $keyword)));

    if(!empty($users)) {
      return $users;
    }

    $this->bind(array('Role'));

    $users = $this->find('all',array('conditions' => array(
          "OR" => array(
                  "User.fname LIKE" => "%".$keyword."%",
                  "User.lname LIKE" => "%".$keyword."%"
          )
        )
      )
    );

    if(!empty($users)) {
      return $users;
    }

    $this->bind(array('Role'));

    $users = $this->query("SELECT * FROM `narradb`.`users` AS `User` WHERE concat(`fname`,`lname`) = '".$keyword."' ");

    if(!empty($users)) {

      foreach ($users as $key => &$value) {
        if(!isset($value['Role'])) {
          $role = $this->Role->findById($value['User']['role_id']);
          array_push($value,$role);
          $value['Role'] = $value[0]['Role'];
          unset($value[0]);
        }
      }
      return $users;
    }

    return $users;

  }

  public function _searchAjax($keyword='')
  {

    $this->bind(array('Role','Department'));

    $keyword = str_replace(' ', '', $keyword);

    $users = $this->find('all',array('conditions' => array('User.empid' => $keyword)));

    if(!empty($users)) {
      return $users;
    }

    $this->bind(array('Role'));

    $users = $this->find('all',array('conditions' => array(
          "OR" => array(
                  "User.lname LIKE" => $keyword."%"
          )
        ),
      'order' => 'User.lname'
      )
    );

    return $users;

  }


}