<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property Group $Group
 * @property AccountType $AccountType
 */
class Department extends AppModel {

	public $recursive = -1;

	public $actsAs = array('Containable');

	public function bind($model = array('Group')) {
	  $this->bindModel(array(
	    'belongsTo' => array(
	      'User' => array(
	        'className' => 'User',
	        'foreignKey' => 'department_id',
	        'conditions' => false,
	        'dependent' => false,
	        'fields' => '',
	        'order' => ''
	      )
	    )
	  ),false);
	  $this->contain($model);
	}

}