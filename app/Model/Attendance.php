<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property Group $Group
 * @property AccountType $AccountType
 */
class Attendance extends AppModel {

	public $recursive = -1;

	public $actsAs = array('Containable');

	public function bind($model = array('Group')) {
		$this->bindModel(array(
			'belongsTo' => array(
				'User' => array(
					'className' => 'User',
					'foreignKey' => 'user_id',
					'conditions' => false,
					'dependent' => false,
					'fields' => '',
					'order' => '',
				),
				'Role' => array(
					'className' => 'Role',
					'foreignKey' => 'position',
					'conditions' => false,
					'dependent' => false,
					'fields' => '',
					'order' => '',
				),
			),
		), false);
		$this->contain($model);
	}

	public function getFields($userid = '', $day = '', $week = '', $month = '', $year = '', $type = '', $isAll = false, $getOmit = false, $getAll = false) {
		
		$this->virtualFields['grandTotal'] = 'SUM(Attendance.grandtotal)';

		$this->virtualFields['totalBasicPay'] = 'SUM(Attendance.total)';
		$this->virtualFields['totalWorkHours'] = 'SUM(Attendance.hours_work)';
		$this->virtualFields['totalRelieverPay'] = 'SUM(Attendance.reliever_pay)';

		$this->virtualFields['totalOtHours'] = 'SUM(Attendance.ot_work)';
		$this->virtualFields['totalOtPay'] = 'SUM(Attendance.ot_pay)';

		$this->virtualFields['totalRestdayHours'] = 'SUM(Attendance.restday_work)';
		$this->virtualFields['totalRestdayPay'] = 'SUM(Attendance.restday_pay)';

		$this->virtualFields['totalNightHours'] = 'SUM(Attendance.night_hours)';
		$this->virtualFields['totalNightPay'] = 'SUM(Attendance.night_pay)';

		$this->virtualFields['totalOmitHours'] = 'SUM(Attendance.omit_hours)';
		$this->virtualFields['totalOmitPay'] = 'SUM(Attendance.omit_pay)';

		$this->virtualFields['totalDeductHours'] = 'SUM(Attendance.deduct_hours)';
		$this->virtualFields['totalDeductPay'] = 'SUM(Attendance.deduct_pay)';
		$this->virtualFields['totalDeductBasics'] = 'SUM(Attendance.deduct_basic)';

		$this->virtualFields['totalLegalHours'] = 'SUM(Attendance.legal_hours)';
		$this->virtualFields['totalLegalPay'] = 'SUM(Attendance.legal_pay)';

		$this->virtualFields['totalRdlegalHours'] = 'SUM(Attendance.rdlegal_work)';
		$this->virtualFields['totalRdlegalPay'] = 'SUM(Attendance.rdlegal_pay)';

		$this->virtualFields['totalOtrdlegalHours'] = 'SUM(Attendance.otrdlegal_work)';
		$this->virtualFields['totalOtrdlegalPay'] = 'SUM(Attendance.otrdlegal_pay)';

		$this->virtualFields['totalLpHours'] = 'SUM(Attendance.lastpayroll_hours)';
		$this->virtualFields['totalLpPay'] = 'SUM(Attendance.lastpayroll_pay)';

		$this->virtualFields['totalOtLpHours'] = 'SUM(Attendance.ot_lp_work)';
		$this->virtualFields['totalOtLpPay'] = 'SUM(Attendance.ot_lp_pay)';

		$this->virtualFields['totalSpecialHours'] = 'SUM(Attendance.special_hours)';
		$this->virtualFields['totalSpecialPay'] = 'SUM(Attendance.special_pay)';
		
		$this->virtualFields['totalSss'] = 'SUM(Attendance.sss)';
		$this->virtualFields['totalPhilhealth'] = 'SUM(Attendance.philhealth)';
		$this->virtualFields['totalPagibig'] = 'SUM(Attendance.pagibig)';
		$this->virtualFields['totalDays'] = 'COUNT(Attendance.id)';

		$findType = 'first';

		if ($isAll) {
			$findType = 'all';
		}

		if ($year == '') {
			$year = date('Y');
		}

		$showExcess = false;
		if ($type == 'user' || $type == 'weekly') {
			if($getOmit) {
				$conditions = array(
					'user_id' => $userid,
					'week' => $week,
					'year' => $year,
					'is_omit' => 1,
				);
			} else {
				$conditions = array(
					'user_id' => $userid,
					'week' => $week,
					'year' => $year,
					'is_omit' => 0,
				);
			}

			if($getAll) {
				$conditions = array(
					'user_id' => $userid,
					'week' => $week,
					'year' => $year,
				);
			}

			$showExcess = true;
		} else if ($type == 'daily') {
			$conditions = array(
				'user_id' => $userid,
				'OR' => array('omit_realdate' => $day, 'date' => $day),
				'year' => $year,
			);
		} else if ($type == 'dailyall') {
			$conditions = array(
				'OR' => array('omit_realdate' => $day, 'date' => $day),
				'year' => $year,
			);
		} else if ($type == 'weekly') {
			$conditions = array(
				'user_id' => $userid,
				'week' => $week,
				'year' => $year,
			);
			$showExcess = true;
		} else if ($type == 'weeklyall') {
			$conditions = array(
				'week' => $week,
				'year' => $year,
			);
			$showExcess = true;
		} else if ($type == 'monthly') {
			$conditions = array(
				'user_id' => $userid,
				'month' => $month,
				'year' => $year,
			);
			$showExcess = true;
		} else if ($type == 'monthlyall') {
			$conditions = array(
				'month' => $month,
				'year' => $year,
			);
			$showExcess = true;
		}

		if($userid == "")
			unset($conditions['user_id']);

		$data = $this->find($findType, array('fields' => array(
			'Attendance.grandTotal',
			'Attendance.totalOmitPay',
			'Attendance.totalSpecialHours',
			'Attendance.totalSpecialPay',
			'Attendance.totalOmitHours',
			'Attendance.totalLegalPay',
			'Attendance.totalLegalHours',
			'Attendance.totalRdlegalHours',
			'Attendance.totalRdlegalPay',
			'Attendance.totalOtrdlegalHours',
			'Attendance.totalOtrdlegalPay',
			'Attendance.totalLpHours',
			'Attendance.totalLpPay',
			'Attendance.totalOtLpPay',
			'Attendance.totalOtLpHours',
			'Attendance.totalRestdayHours',
			'Attendance.totalRestdayPay',
			'Attendance.totalNightHours',
			'Attendance.totalBasicPay',
			'Attendance.totalWorkHours',
			'Attendance.totalRelieverPay',
			'Attendance.totalOtHours',
			'Attendance.totalOtPay',
			'Attendance.totalDeductHours',
			'Attendance.totalDeductPay',
			'Attendance.totalDeductBasics',
			'Attendance.totalNightPay',
			'Attendance.totalSss',
			'Attendance.totalPhilhealth',
			'Attendance.totalPagibig',
			'Attendance.totalDays',
		),
			'conditions' => $conditions,
		));

		if ($showExcess) {

			if(isset($conditions['is_omit']))
				unset($conditions['is_omit']);

			ClassRegistry::init('OtExccess')->virtualFields['totalOtExccess'] = 'SUM(OtExccess.ot_hours)';
			ClassRegistry::init('OtExccess')->virtualFields['totalOtPayExccess'] = 'SUM(OtExccess.ot_pay)';
			$exccess = ClassRegistry::init('OtExccess')->find($findType, array(
				'fields' => array(
					'OtExccess.totalOtExccess',
					'OtExccess.totalOtPayExccess',
				),
				'conditions' => $conditions,
			));
			if(isset($exccess['OtExccess'])) {
				$data['Attendance']['totalOtExccess'] = $exccess['OtExccess']['totalOtExccess'];
				$data['Attendance']['totalOtPayExccess'] = $exccess['OtExccess']['totalOtPayExccess'];
			} else {
				$data['Attendance']['totalOtExccess'] = 0;
				$data['Attendance']['totalOtPayExccess'] = 0;
			}
		}

		if ($week != '') {
			$this->virtualFields['totalEmployee'] = 'Attendance.id';
			$totalAttendee = $this->find('count', array('fields' => array('Attendance.totalEmployee'),
				'conditions' => array(
					'Attendance.week' => $week,
					'Attendance.year' => $year,
				),
				'group' => 'Attendance.user_id',
			));

			if (!empty($totalAttendee)) {
				$data['Attendance']['totalEmployee'] = $totalAttendee;
			} else {
				$data['Attendance']['totalEmployee'] = 0;
			}
		}
		return $data;
	}

	public function computeGross($data) {
		$data['Attendance']['grossPay'] = $data['Attendance']['grandTotal'] + $data['Attendance']['totalSpecialPay'] + $data['Attendance']['totalOtLpPay'] + $data['Attendance']['totalLpPay'] + $data['Attendance']['totalLegalPay']  + $data['Attendance']['totalOtPay'] + $data['Attendance']['totalNightPay']+ $data['Attendance']['totalRelieverPay'];
		return $data;
	}

	public function computeNet($data, $userid = '', $week = '', $year = '', $type = 'weekly',$grand='') {
			if ($year == '') {
				$year = date('Y');
			}

			if ($userid != '') {

				$data['Attendance']['grossPay'] -= $data['Attendance']['totalDeductPay'];

				if ($type == 'monthly') {
					ClassRegistry::init('Deduction')->virtualFields['totalSss'] = 'SUM(Deduction.sss)';
					ClassRegistry::init('Deduction')->virtualFields['totalPhilhealth'] = 'SUM(Deduction.philhealth)';
					ClassRegistry::init('Deduction')->virtualFields['totalPagibig'] = 'SUM(Deduction.pagibig)';
					$deduction = ClassRegistry::init('Deduction')->find('all', array('conditions' => array('user_id' => $userid, 'month' => $week, 'year' => $year)));
					$data['Attendance']['grandSss'] = $deduction['Deduction']['sss'] = $deduction[0]['Deduction']['totalSss'];
					$data['Attendance']['grandPhilhealth'] = $deduction['Deduction']['philhealth'] = $deduction[0]['Deduction']['totalPhilhealth'];
					$data['Attendance']['grandPagibig'] = $deduction['Deduction']['pagibig'] = $deduction[0]['Deduction']['totalPagibig'];
				} else {
					$deduction = ClassRegistry::init('Deduction')->find('first', array('conditions' => array('user_id' => $userid, 'week' => $week, 'year' => $year)));
					$data['Attendance']['grandSss'] = $deduction;
					$data['Attendance']['grandPhilhealth'] = $deduction;
				}

				if ($week != '') {
					$paidloan = ClassRegistry::init('Paidloan')->find('first', array(
						'conditions' => array(
							'Paidloan.user_id' => $userid,
							'Paidloan.week' => $week,
							'Paidloan.year' => $year,
						),
					));
				}

				if (!isset($paidloan['Paidloan'])) {
					$paidloan['Paidloan']['sss_loan'] = 0;
					$paidloan['Paidloan']['pagibig_loan'] = 0;
					$paidloan['Paidloan']['calamity_loan'] = 0;
					$paidloan['Paidloan']['advance'] = 0;
				}

				if (empty($deduction)) {
					$deduction['Deduction']['sss'] = 0;
					$deduction['Deduction']['philhealth'] = 0;
					$deduction['Deduction']['pagibig'] = 0;
				}

				$data['Attendance']['totalSss'] = $deduction['Deduction']['sss'];
				$data['Attendance']['totalPhilhealth'] = $deduction['Deduction']['philhealth'];
				$data['Attendance']['totalPagibig'] = $deduction['Deduction']['pagibig'];

				$data['Attendance']['totalDeductions'] = $totalDeductions = $paidloan['Paidloan']['sss_loan'] + $paidloan['Paidloan']['pagibig_loan'] + $paidloan['Paidloan']['calamity_loan'] + $data['Attendance']['totalPagibig'] + $data['Attendance']['totalPhilhealth'] + $data['Attendance']['totalSss'];
			} else {

				if ($type == 'monthly') {
					$tmpDeduction = ClassRegistry::init('Deduction')->find('all', array(
						'conditions' => array('month' => $week, 'year' => $year,
						)));
				} else {
					$deduction = ClassRegistry::init('Deduction')->find('first', array(
						'fields' => array('SUM(sss) as sss', 'SUM(philhealth) as philhealth', 'SUM(pagibig) as pagibig'),
						'conditions' => array('week' => $week, 'year' => $year,
						)));
					$deduction['Deduction'] = $deduction[0];
					unset($deduction[0]);
				}

				$data['Attendance']['grandDedudction'] = $deduction;
				$data['Attendance']['totalSss'] = $deduction['Deduction']['sss'];
				$data['Attendance']['totalPhilhealth'] = $deduction['Deduction']['philhealth'];
				$data['Attendance']['totalPagibig'] = $deduction['Deduction']['pagibig'];

				$data['Attendance']['totalDeductions'] = $totalDeductions = $data['Attendance']['totalPagibig'] + $data['Attendance']['totalPhilhealth'] + $data['Attendance']['totalSss'];

				if($grand=="grand") {
					$paidloan = ClassRegistry::init('Paidloan')->find('first', array(
						'fields' => array('SUM(sss_loan) as sss_loan', 'SUM(calamity_loan) as calamity_loan', 'SUM(pagibig_loan) as pagibig_loan', 'SUM(advance) as advance'),
						'conditions' => array(
							'Paidloan.week' => $week,
							'Paidloan.year' => $year,
						),
					));
					$data['Attendance']['sss_loan'] = $paidloan[0]['sss_loan'];
					$data['Attendance']['calamity_loan'] = $paidloan[0]['calamity_loan'];
					$data['Attendance']['pagibig_loan'] = $paidloan[0]['pagibig_loan'];
					$data['Attendance']['advance'] = $paidloan[0]['advance'];
					$data['Attendance']['totalDeductions'] = $totalDeductions = $totalDeductions + $data['Attendance']['sss_loan'] + $data['Attendance']['pagibig_loan'] + $data['Attendance']['calamity_loan'] + $data['Attendance']['advance'];
				}

			}

			$data['Attendance']['netPay'] = $data['Attendance']['grossPay'] - $totalDeductions;
			App::uses('NumberHelper', 'View/Helper');
			$number = new NumberHelper(new View());

			$data['Attendance']['totalBasicPay'] = $number->currency($data['Attendance']['totalBasicPay'] + $data['Attendance']['totalOmitPay'], '');
			$data['Attendance']['totalOtPay'] = $number->currency($data['Attendance']['totalOtPay'], '');
			$data['Attendance']['totalRestdayPay'] = $number->currency($data['Attendance']['totalRestdayPay'], '');
			$data['Attendance']['totalRdlegalPay'] = $number->currency($data['Attendance']['totalRdlegalPay'], '');
			$data['Attendance']['totalOtrdlegalPay'] = $number->currency($data['Attendance']['totalOtrdlegalPay'], '');
			$data['Attendance']['totalOmitPay'] = $number->currency($data['Attendance']['totalOmitPay'], '');
			$data['Attendance']['totalNightPay'] = $number->currency($data['Attendance']['totalNightPay'], '');
			$data['Attendance']['grossPay'] = $number->currency($data['Attendance']['grossPay'], '');
			$data['Attendance']['totalSss'] = $number->currency($data['Attendance']['totalSss'], '');
			$data['Attendance']['totalPhilhealth'] = $number->currency($data['Attendance']['totalPhilhealth'], '');
			$data['Attendance']['totalPagibig'] = $number->currency($data['Attendance']['totalPagibig'], '');
			$data['Attendance']['totalDeductions'] = $number->currency($data['Attendance']['totalDeductions'], '');
			$data['Attendance']['netPay'] = $number->currency($data['Attendance']['netPay'], '');
			$data['Attendance']['totalLegalPay'] = $number->currency($data['Attendance']['totalLegalPay'], '');
			$data['Attendance']['totalLpPay'] = $number->currency($data['Attendance']['totalLpPay'], '');

			if ($userid != '') {
				if (!empty($paidloan)) {
					$data['Attendance']['sss_loan'] = $number->currency($paidloan['Paidloan']['sss_loan'], '');
					$data['Attendance']['pagibig_loan'] = $number->currency($paidloan['Paidloan']['pagibig_loan'], '');
					$data['Attendance']['calamity_loan'] = $number->currency($paidloan['Paidloan']['calamity_loan'], '');
					$data['Attendance']['advance'] = $number->currency($paidloan['Paidloan']['advance'], '');
				} else {
					$data['Attendance']['sss_loan'] = 0;
					$data['Attendance']['pagibig_loan'] = 0;
					$data['Attendance']['calamity_loan'] = 0;
					$data['Attendance']['advance'] = 0;
				}
			}
		return $data;
	}

	public function getTotal($userid="", $day, $week, $year,$getOmit = false,$getAll = false) {
		$data = $this->getFields($userid, '', $week, '', '', 'weekly', false,$getOmit,$getAll);
		
		$data = $this->computeGross($data);
		$data = $this->computeNet($data, $userid, $week, $year,'weekly','grand');
		return $data;
	}

	public function getTotalMonth($userid, $day, $month, $year) {

		$data = $this->getFields($userid, '', '', $month, '', 'monthly', false);
		$data = $this->computeGross($data);
		$data = $this->computeNet($data);
		return $data;

	}

	public function getTotalday($day, $week) {

		$data = $this->getFields('', $day, '', '', '', 'dailyall', false);
		$data = $this->computeGross($data);
		$data = $this->computeNet($data);
		return $data;

	}

	public function getTotalweek($week, $year = '') {

		if ($year == '') {
			$year = date('Y');
		}

		$data = $this->getFields('', '', $week, '', '', 'weeklyall', false);
		$data = $this->computeGross($data);
		$data = $this->computeNet($data, '', $week, $year);
		return $data;

	}

	public function getWeeklyAttendancePosition($week, $dept, $year,$incLoan = false,$isOmit = false) {
		$this->bind('User');

		if($isOmit) {
			$conditions = array(
				'Attendance.week' => $week,
				'User.department_id' => $dept,
				'Attendance.year' => $year,
				'Attendance.is_omit' => 1,
			);
		} else {
			$conditions = array(
				'Attendance.week' => $week,
				'User.department_id' => $dept,
				'Attendance.year' => $year,
				'Attendance.is_omit' => 0,
			);
		}

		$users = $this->find('list', array(
			'fields' => array('User.role_id'),
			'conditions' => $conditions,
			'group' => 'User.role_id',
		));
		$data = array();
		foreach ($users as $key => $value) {
			$tmp = ClassRegistry::init('Role')->findById($value);
			array_push($data, $tmp);
		}

		return $data;
	}

	public function getWeeklyAttendanceTotal($week, $dept, $year,$incLoan = false,$isOmit = false,$role='') {
		if($isOmit) {
			$temp = $this->getTotal('', '', $week, $year,true);
		} else {
			$temp = $this->getTotal('', '', $week, $year);
		}
		return $temp;
	}

	public function getWeeklyAttendance($week, $dept, $year,$incLoan = false,$isOmit = false,$role='') {
		$this->bind('User');

		if($isOmit) {
			$conditions = array(
				'Attendance.week' => $week,
				'User.department_id' => $dept,
				'Attendance.year' => $year,
				'Attendance.is_omit' => 1,
			);
		} else {
			$conditions = array(
				'Attendance.week' => $week,
				'User.department_id' => $dept,
				'Attendance.year' => $year,
				'Attendance.is_omit' => 0,
			);
		}
		if($role != '') {
			$conditions['Attendance.position'] = $role;
		}
		$users = $this->find('all', array(
			'fields' => array('Attendance.user_id'),
			'conditions' => $conditions,
			'group' => 'Attendance.user_id',
			'order' => 'User.lname',
		));
		$data = array();
		foreach ($users as $key => $value) {

			if($isOmit) {
				$dateCondition = array(
					'Attendance.user_id' => $value['Attendance']['user_id'],
					'Attendance.week' => $week,
					'Attendance.year' => $year,
					'Attendance.is_omit' => 1,
				);
			} else {
				$dateCondition = array(
					'Attendance.user_id' => $value['Attendance']['user_id'],
					'Attendance.week' => $week,
					'Attendance.year' => $year,
				);
			}

			$tempDay = $this->find('all', array(
				'fields' => array('Attendance.hours_work', 'Attendance.night_hours','Attendance.is_night', 'Attendance.legal_hours', 'Attendance.ot_work', 'Attendance.special_hours', 'Attendance.lastpayroll_hours', 'Attendance.date', 'Attendance.omit_realdate'),
				'conditions' => $dateCondition,
				'order' => 'Attendance.date',
			));


			if($isOmit) {
				$temp = $this->getTotal($value['Attendance']['user_id'], '', $week, $year,true);
			} else {
				$temp = $this->getTotal($value['Attendance']['user_id'], '', $week, $year);
			}

			$this->bind(array('User'));
			$tempUser = $this->User->findById($value['Attendance']['user_id']);
			$data[$key]['User'] = $tempUser['User'];
			$data[$key]['Attendance'] = $temp['Attendance'];
			$data[$key]['Days'] = $tempDay;
			if($incLoan) {
				$tmpLoan = ClassRegistry::init('Loan')->findByUserId($tempUser['User']['id']);
				if(!empty($tmpLoan)) {
					$data[$key]['Loan'] = $tmpLoan['Loan'];
				} else {
					$data[$key]['Loan'] = array('sss_loan' => 0, 'pagibig_loan' => 0, 'calamity_loan' => 0,'advance' => 0);
				}
			}
		}
		return $data;
	}

	public function getUsers($week='',$year='')
	{
		$this->bind('User');
		$data = $this->find('all',array('fields' => 'User.*','conditions' => array('week' => $week, 'year' => $year),'group' => 'user_id'));
		return $data;
	}

	public function getMonthlyAttendance($month, $year = '') {

		if ($year == '') {
			$year = date('Y');
		}

		$this->bind('User');
		$users = $this->find('all', array(
			'fields' => array('Attendance.user_id'),
			'conditions' => array(
				'Attendance.month' => $month,
				'Attendance.year' => $year,
			),
			'group' => 'Attendance.user_id',
			'order' => 'User.lname',
		));

		$data = array();
		foreach ($users as $key => $value) {

			$temp = $this->getTotalMonth($value['Attendance']['user_id'], '', $month, $year);

			$this->bind(array('User'));
			$tempUser = $this->User->findById($value['Attendance']['user_id']);
			$data[$key]['User'] = $tempUser['User'];
			$data[$key]['Attendance'] = $temp['Attendance'];
		}
		return $data;
	}

	public function getSssTotal($data) {
		$total = array();
		$total['gross'] = 0;
		$total['msc'] = 0;
		$total['ee'] = 0;
		$total['er'] = 0;
		$total['ecc'] = 0;
		$total['total'] = 0;
		$total['grand'] = 0;
		if (!empty($data)) {

			foreach ($data as $key => $value) {
				$total['gross'] += $value['Deduction']['gross'];
				$total['msc'] += $value['Deduction']['sss_msc'];
				$total['ee'] += $value['Deduction']['sss_ee'];
				$total['er'] += $value['Deduction']['sss_er'];
				$total['ecc'] += $value['Deduction']['sss_ecc'];
				$total['total'] += $value['Deduction']['sss_total'];
			}
			$total['grand'] = $total['total'] + $total['ecc'];
		}
		return $total;
	}

	public function getPhilhealthTotal($data) {
		$total = array();
		$total['gross'] = 0;
		$total['msc'] = 0;
		$total['ee'] = 0;
		$total['er'] = 0;
		$total['ecc'] = 0;
		$total['total'] = 0;
		$total['grand'] = 0;
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$total['gross'] += $value['Deduction']['gross'];
				$total['msc'] += $value['Deduction']['phil_msc'];
				$total['ee'] += $value['Deduction']['phil_ee'];
				$total['er'] += $value['Deduction']['phil_er'];
				$total['total'] += $value['Deduction']['phil_total'];
			}
			$total['grand'] = $total['total'] + $total['ecc'];
		}
		return $total;
	}

	public function getSssReport($month, $year = '') {
		if ($year == '') {
			$year = date('Y');
		}

		$this->bind('User');
		$users = $this->find('all', array(
			'fields' => array('User.id','User.fname','User.lname','User.mname','User.sss','User.philhealth'),
			'conditions' => array(
				'Attendance.month' => $month,
				'Attendance.year' => $year,
			),
			'group' => 'Attendance.user_id',
			'order' => 'User.lname',
		));

		foreach ($users as $key => &$value) {
			$record = $this->getFields($value['User']['id'],'','',$month,$year,'monthly');
			$record = $this->computeGross($record);
			$record = $this->computeNet($record,$value['User']['id'],$month,$year,'monthly');
			//$sss = ClassRegistry::init('Ss')->query("SELECT `Ss`.`id`, `Ss`.`from`, `Ss`.`to`, `Ss`.`premium`, `Ss`.`er`, `Ss`.`ee`, `Ss`.`ecc`, `Ss`.`total`, `Ss`.`date_added` FROM `narradb`.`sses` AS `Ss`   WHERE `SS`.`premium` != '' AND `Ss`.`from` <= " . $gross . " AND `Ss`.`to` >= " . $gross . "");
			$value['Attendance'] = $record['Attendance'];
		}

		return $users;
	}

	public function getPhilhealthReport($month, $year = '') {
		if ($year == '') {
			$year = date('Y');
		}

		$this->bind('User');
		$users = $this->find('all', array(
			'fields' => array('Attendance.user_id'),
			'conditions' => array(
				'Attendance.month' => $month,
				'Attendance.year' => $year,
			),
			'group' => 'Attendance.user_id',
			'order' => 'User.lname',
		));

		$data = array();
		foreach ($users as $key => $value) {
			$temp = $this->getFields($value['Attendance']['user_id'], '', '', $month, $year, 'monthly');
			$temp = $this->computeGross($temp);
			$temp['Deduction'] = ClassRegistry::init('Mdeduction')->find('first', array(
				'conditions' => array('user_id' => $value['Attendance']['user_id'], 'month' => $month, 'year' => $year),
			));
			$temp['Deduction'] = $temp['Deduction']['Mdeduction'];
			$this->bind(array('User'));
			$tempUser = $this->User->findById($value['Attendance']['user_id']);
			$data[$key]['User'] = $tempUser['User'];
			$data[$key]['Attendance'] = $temp['Attendance'];
			$data[$key]['Deduction'] = $temp['Deduction'];
		}
		return $data;
	}

	public function getOmit($week) {
		$this->bind(array('User'));
		$data = $this->find('all', array('conditions' => array('Attendance.week' => $week, 'Attendance.is_omit' => 1)));

		foreach ($data as $key => &$value) {
			if ($value['Attendance']['is_reliever'] == 1) {
				$this->bind(array('Role'));
				$value['r'] = $this->Role->findById($value['Attendance']['reliever_position']);
			} else {
				$this->bind(array('Role'));
				$value['r'] = $this->Role->findById($value['Attendance']['position']);
			}
		}
		return $data;
	}

	public function checkForHolidays($data, $type, $isId = false) {
		if ($type == 'week') {
			foreach ($data as $key => &$value) {
				$holiday = ClassRegistry::init('Holiday')->find('first', array('conditions' => array('Holiday.date' => strtotime($value))));
				if (!empty($holiday)) {
					if ($holiday['Holiday']['is_legal'] == 1) {
						$value = $value . " | Legal Holiday";
					} else {
						$value = $value . " | Holiday";

					}
				}
			}
		} else {
			$temp = ClassRegistry::init('Holiday')->find('first', array('conditions' => array('Holiday.date' => $data)));
			if (!empty($temp)) {
				if ($temp['Holiday']['is_legal'] == 1) {
					$temp = date("D m d, Y", $data) . " | Legal Holiday";
				} else {
					$temp = date("D m d, Y", $data) . " | Holiday";

				}
				$data = $temp;
				if ($isId) {
					$data = true;
				}
			} else {
				if ($isId) {
					$data = false;
				}
			}
		}
		return $data;
	}

	public function procChecker($data = '') {
		// this is to remove unwanted field if the checker doesn't exist
		if (!isset($data['Attendance']['is_omit'])) {
			if (isset($data['Attendance']['omitDate'])) {
				unset($data['Attendance']['omitDate']);
			}
		}

		// this is to remove unwanted field if the checker doesn't exist
		if (!isset($data['Attendance']['is_reliever'])) {
			if (isset($data['Attendance']['reliever_position'])) {
				unset($data['Attendance']['reliever_position']);
			}
		}

		// this is to remove unwanted field if the checker doesn't exist
		if (!isset($data['Attendance']['is_ot'])) {
			$data['Attendance']['ot_work'] = 0;
			$data['Attendance']['ot_pay'] = 0;
		}

		return $data;

	}

	public function absentChecker($data) {
		if (isset($data['Attendance']['is_absent'])) {
			$data['Attendance']['hours_work'] = 0;
			$data['Attendance']['is_absent'] = 1;
		}

		return $data;
	}

	public function relieverChecker($data) {
		$data['Attendance']['reliever_pay'] = 0;
		if (isset($data['Attendance']['is_reliever'])) {
			
			$data['Attendance']['is_reliever'] = 1;
			$newPosition = ClassRegistry::init('Role')->findById($data['Attendance']['reliever_position']);

			if ($data['Attendance']['per_hour'] < $newPosition['Role']['per_hour']) {
				$data['Attendance']['reliever_pay'] = $newPosition['Role']['per_hour'] - $data['Attendance']['per_hour'];
			}
		} else {
			if(isset($data['Attendance']['reliever_position']))
				unset($data['Attendance']['reliever_position']);
		}

		return $data;
	}

	public function restdayChecker($data,$holiday) {
		if (isset($data['Attendance']['is_lastpayroll'])) {

			$isExcessExist = ClassRegistry::init('OtExccess')->findByDate($data['Attendance']['date']);

			if(!empty($isExcessExist)) {
				$temp['OtExccess']['id'] = $isExcessExist['OtExccess']['id'];
			}
			$temp['OtExccess']['user_id'] = $data['Attendance']['user_id'];
			$temp['OtExccess']['date'] = $data['Attendance']['date'];
			$temp['OtExccess']['week'] = $data['Attendance']['week'];
			$temp['OtExccess']['month'] = $data['Attendance']['month'];
			$temp['OtExccess']['year'] = $data['Attendance']['year'];
			$temp['OtExccess']['hours_work'] = $data['Attendance']['hours_work'];
			$temp['OtExccess']['per_hour'] = $data['Attendance']['per_hour'] * 1.30;
			if(empty($isExcessExist)) {
				ClassRegistry::init('OtExccess')->create();
			}
			ClassRegistry::init('OtExccess')->save($temp);
			return $data;
		}

		if (isset($data['Attendance']['is_restday'])) {
			if (isset($data['Attendance']['is_omit'])) {
				$user = $this->bind(array('User'));
			} else {
				$data['Attendance']['is_restday'] = 1;
				if (empty($holiday)) {
					$data['Attendance']['per_hour'] = $data['Attendance']['per_hour'] * 1.30;
				}
			}
		}

		return $data;
	}

	public function otChecker($data,$holiday) {
		if (isset($data['Attendance']['is_ot'])) {
			$data['Attendance']['is_ot'] = 1;
			$otHours = $data['Attendance']['ot_work'];
			$basicPay = $data['Attendance']['per_hour'];

			if (!empty($holiday) || $data['Attendance']['is_restday'] == 1) {
				if ($holiday['Holiday']['is_legal'] == 1) {
					if($data['Attendance']['is_restday'] == 1) {
						$percent = 3.38;
					}  else {
						$percent = 2.60;
					}
				} else {
					$percent = 1.30;
				}	
			} else {
				$percent = 1.25;
			}

			//2.6 legal ot
			//1.30 normal ot restday/special
			//1.25 normal ot

			if($otHours > 4) {

				if(!isset($data['Attendance']['is_lastpayroll'])) {
					$remOtHrs = $otHours - 4;
					$otHours = 4;
				} else {
					$remOtHrs = $otHours;
				}


				$isExcessExist = ClassRegistry::init('OtExccess')->findByDate($data['Attendance']['date']);
				if(!empty($isExcessExist)) {
					$temp['OtExccess']['id'] = $isExcessExist['OtExccess']['id'];
				}
				$temp['OtExccess']['user_id'] = $data['Attendance']['user_id'];
				$temp['OtExccess']['date'] = $data['Attendance']['date'];
				$temp['OtExccess']['week'] = $data['Attendance']['week'];
				$temp['OtExccess']['month'] = $data['Attendance']['month'];
				$temp['OtExccess']['year'] = $data['Attendance']['year'];
				$temp['OtExccess']['ot_hours'] = $remOtHrs;
				$temp['OtExccess']['ot_pay'] = (($basicPay * $percent) / 8) * $remOtHrs;
				if(empty($isExcessExist)) {
					ClassRegistry::init('OtExccess')->create();
				}
				ClassRegistry::init('OtExccess')->save($temp);
				$data['Attendance']['ot_work'] = 4;
			}

			$userData = $this->getFields($data['Attendance']['user_id'], '', $data['Attendance']['week'], '', $data['Attendance']['year'], 'weekly');
			$totOthrs = $userData['Attendance']['totalOtHours'] + $otHours;
 
			if ($totOthrs > 12) {
				$remOtHrs = $totOthrs - 12;
				$otHours = $otHours - (($userData['Attendance']['totalOtHours'] + $otHours) - 12);

				$isExcessExist = ClassRegistry::init('OtExccess')->findByDate($data['Attendance']['date']);
				if(!empty($isExcessExist)) {
					$temp['OtExccess']['id'] = $isExcessExist['OtExccess']['id'];
				}
				$temp['OtExccess']['user_id'] = $data['Attendance']['user_id'];
				$temp['OtExccess']['date'] = $data['Attendance']['date'];
				$temp['OtExccess']['week'] = $data['Attendance']['week'];
				$temp['OtExccess']['month'] = $data['Attendance']['month'];
				$temp['OtExccess']['year'] = $data['Attendance']['year'];
				$temp['OtExccess']['ot_hours'] = $remOtHrs;
				$temp['OtExccess']['ot_pay'] = (($basicPay * $percent) / 8) * $remOtHrs;
				if(empty($isExcessExist)) {
					ClassRegistry::init('OtExccess')->create();
				}
				ClassRegistry::init('OtExccess')->save($temp);

			}
			$data['Attendance']['ot_work'] = $otHours;
			$data['Attendance']['ot_pay'] = (($basicPay * $percent) / 8) * $otHours;
			if (isset($data['Attendance']['is_lastpayroll'])) {
				$data['Attendance']['ot_lp_work'] = $data['Attendance']['ot_work'];
				$data['Attendance']['ot_lp_pay'] = $data['Attendance']['ot_pay'];
				$data['Attendance']['ot_work'] = 0;
				$data['Attendance']['ot_pay'] = 0;
			}

		} else {
			$data['Attendance']['ot_pay'] = 0;
		}

		return $data;
	}

	public function nightChecker($data) {
		if (isset($data['Attendance']['is_night'])) {
			$data['Attendance']['is_night'] = 1;
			$data['Attendance']['night_pay'] = $data['Attendance']['total'] * .10;
		} else {
			$data['Attendance']['night_pay'] = 0;
		}

		return $data;
	}

	public function getAttendanceTotal($data) {
		$data['Attendance']['total'] = ($data['Attendance']['per_hour'] / 8) * $data['Attendance']['hours_work'];

		return $data;
	}

	public function getInsentive($data, $holiday) {

		$hrsWork = $data['Attendance']['hours_work'];

		if ($hrsWork >= 8) {
			$cola = 12.50;
			$sea = 13;
		} else {
			$cola = (12.50 / 8) * $hrsWork;
			$sea = (13.00 / 8) * $hrsWork;
		}
		
		if (!empty($holiday)) {
			if ($holiday['Holiday']['is_legal'] == 1) {
				if(!isset($data['Attendance']['is_lastpayroll'])) {
					$cola = $cola * 2;
					$sea = $sea * 2;
				} else {
					$isExcessExist = ClassRegistry::init('OtExccess')->findByDate($data['Attendance']['date']);
					if(!empty($isExcessExist)) {
						$temp['OtExccess']['id'] = $isExcessExist['OtExccess']['id'];
					}
					$temp['OtExccess']['user_id'] = $data['Attendance']['user_id'];
					$temp['OtExccess']['date'] = $data['Attendance']['date'];
					$temp['OtExccess']['week'] = $data['Attendance']['week'];
					$temp['OtExccess']['month'] = $data['Attendance']['month'];
					$temp['OtExccess']['year'] = $data['Attendance']['year'];
					$temp['OtExccess']['cola'] = $cola;
					$temp['OtExccess']['sea'] = $sea;
					if(empty($isExcessExist)) {
						ClassRegistry::init('OtExccess')->create();
					}
					ClassRegistry::init('OtExccess')->save($temp);
				}
			}
		}

		$data['Attendance']['cola'] = $cola;
		$data['Attendance']['sea'] = $sea;
		return $data;

	}

	public function getGrossPay($data) {
		if (!isset($data['Attendance']['omit_pay'])) {
			$data['Attendance']['omit_pay'] = 0;
		}
		
		if(!isset($data['Attendance']['otrdlegal_pay'])) {
			$data['Attendance']['otrdlegal_pay'] = 0;
		}

		if(!isset($data['Attendance']['rdlegal_pay'])) {
			$data['Attendance']['rdlegal_pay'] = 0;
		}

		if(!isset($data['Attendance']['restday_pay'])) {
			$data['Attendance']['restday_pay'] = 0;
		}

		$data['Attendance']['grosspay'] = $data['Attendance']['ot_pay'] + $data['Attendance']['total'] + $data['Attendance']['night_pay'] + $data['Attendance']['omit_pay'] + $data['Attendance']['otrdlegal_pay'] + $data['Attendance']['rdlegal_pay'] + $data['Attendance']['restday_pay'] + $data['Attendance']['reliever_pay'];
		return $data;
	}

	public function getDeductions($data) {

		$deduction = ClassRegistry::init('Deduction')->find('first', array(
			'conditions' => array(
				'user_id' => $data['Attendance']['user_id'],
				'week' => $data['Attendance']['week'],
				'month' => $data['Attendance']['month'],
				'year' => $data['Attendance']['year'],
			),
		));

		$userData = $this->getFields($data['Attendance']['user_id'], '', $data['Attendance']['week'], '', $data['Attendance']['year'], 'weekly');
		if (!isset($userData['Attendance']['grossPay'])) {
			$userData = $this->computeGross($userData);
		}

		$gross = $userData['Attendance']['grossPay'] - $userData['Attendance']['totalDeductPay'];
		$sss = ClassRegistry::init('Ss')->query("SELECT `Ss`.`id`, `Ss`.`from`, `Ss`.`to`, `Ss`.`premium`, `Ss`.`er`, `Ss`.`ee`, `Ss`.`ecc`, `Ss`.`total`, `Ss`.`date_added` FROM `narradb`.`sses` AS `Ss`   WHERE `SS`.`premium` != '' AND `Ss`.`from` <= " . $gross . " AND `Ss`.`to` >= " . $gross . "");
		$temp['Deduction']['sss'] = $sss[0]['Ss']['premium'];
		$ph = ClassRegistry::init('Philhealth')->query("SELECT `Philhealth`.`id`, `Philhealth`.`from`, `Philhealth`.`to`, `Philhealth`.`premium`, `Philhealth`.`er`, `Philhealth`.`ee`, `Philhealth`.`ecc`, `Philhealth`.`total`, `Philhealth`.`date_added` FROM `narradb`.`philhealths` AS `Philhealth`   WHERE `Philhealth`.`premium` != '' AND `Philhealth`.`from` <= " . $gross . " AND `Philhealth`.`to` >= " . $gross . "");

		$temp['Deduction']['philhealth'] = $ph[0]['Philhealth']['premium'];

		if (!isset($userData['Attendance']['totalDeductBasics'])) {
			$userData['Attendance']['totalDeductBasics'] = 0;
		}

		$userData['Attendance']['totalBasicPay'] -= $userData['Attendance']['totalDeductBasics'];

		$temp['Deduction']['pagibig'] = $userData['Attendance']['totalBasicPay'] * 0.02;

		$temp['Deduction']['week'] = $data['Attendance']['week'];
		$temp['Deduction']['month'] = $data['Attendance']['month'];
		$temp['Deduction']['year'] = $data['Attendance']['year'];
		$temp['Deduction']['user_id'] = $data['Attendance']['user_id'];

		if (empty($deduction)) {
			ClassRegistry::init('Deduction')->create();
		} else {
			$temp['Deduction']['id'] = $deduction['Deduction']['id'];
		}

		ClassRegistry::init('Deduction')->save($temp);

		return $data;

	}

	public function getMonthlyDeductions($data) {

		$deduction = ClassRegistry::init('Mdeduction')->find('first', array(
			'user_id' => $data['Attendance']['user_id'],
			'month' => $data['Attendance']['month'],
			'year' => $data['Attendance']['year'],
		));

		$userData = $this->getFields($data['Attendance']['user_id'], '', '', $data['Attendance']['month'], $data['Attendance']['year'], 'monthly');

		if (!isset($userData['Attendance']['grossPay'])) {
			$userData = $this->computeGross($userData);
		}

		$gross = $userData['Attendance']['grossPay'] - $userData['Attendance']['totalDeductPay'];

		$sss = ClassRegistry::init('Ss')->query("SELECT `Ss`.`id`, `Ss`.`from`, `Ss`.`to`, `Ss`.`premium`, `Ss`.`msc`, `Ss`.`er`, `Ss`.`ee`, `Ss`.`ecc`, `Ss`.`total`, `Ss`.`date_added` FROM `narradb`.`sses` AS `Ss`   WHERE `SS`.`premium` = '' AND `Ss`.`from` <= " . $gross . " AND `Ss`.`to` >= " . $gross . "");
		if (!empty($sss)) {
			$temp['Mdeduction']['sss_msc'] = $sss[0]['Ss']['msc'];
			$temp['Mdeduction']['sss_ee'] = $sss[0]['Ss']['ee'];
			$temp['Mdeduction']['sss_er'] = $sss[0]['Ss']['er'];
			$temp['Mdeduction']['sss_ecc'] = $sss[0]['Ss']['ecc'];
			$temp['Mdeduction']['sss_total'] = $sss[0]['Ss']['total'];
		}

		$ph = ClassRegistry::init('Philhealth')->query("SELECT `Philhealth`.`id`, `Philhealth`.`from`, `Philhealth`.`to`, `Philhealth`.`premium`, `Philhealth`.`msc`, `Philhealth`.`er`, `Philhealth`.`ee`, `Philhealth`.`ecc`, `Philhealth`.`total`, `Philhealth`.`date_added` FROM `narradb`.`philhealths` AS `Philhealth`   WHERE `Philhealth`.`premium` = '' AND `Philhealth`.`from` <= " . $gross . " AND `Philhealth`.`to` >= " . $gross . "");

		if (!empty($ph)) {
			$temp['Mdeduction']['phil_msc'] = $ph[0]['Philhealth']['msc'];
			$temp['Mdeduction']['phil_ee'] = $ph[0]['Philhealth']['ee'];
			$temp['Mdeduction']['phil_er'] = $ph[0]['Philhealth']['er'];
			$temp['Mdeduction']['phil_total'] = $ph[0]['Philhealth']['total'];
		}

		if (!isset($userData['Attendance']['totalDeductBasics'])) {
			$userData['Attendance']['totalDeductBasics'] = 0;
		}

		$userData['Attendance']['totalBasicPay'] -= $userData['Attendance']['totalDeductBasics'];

		$temp['Mdeduction']['pagibig'] = $data['Attendance']['per_hour'] * 0.02;

		$temp['Mdeduction']['month'] = $data['Attendance']['month'];
		$temp['Mdeduction']['year'] = $data['Attendance']['year'];
		$temp['Mdeduction']['user_id'] = $data['Attendance']['user_id'];
		$temp['Mdeduction']['gross'] = $gross;

		if (empty($deduction)) {
			ClassRegistry::init('Mdeduction')->create();
		} else {
			$temp['Mdeduction']['id'] = $deduction['Mdeduction']['id'];
		}

		ClassRegistry::init('Mdeduction')->save($temp);

		return $data;

	}

	public function holidayChecker($data, $holiday) {

		if (!empty($holiday)) {

			if($data['Attendance']['hours_work'] < 8) {
				$data['Attendance']['per_hour'] = ($data['Attendance']['per_hour'] / 8) * $data['Attendance']['hours_work'];
			}

			$data['Attendance']['is_holiday'] = 1;
			if ($holiday['Holiday']['is_legal'] == 1) {
				if (!isset($data['Attendance']['is_lastpayroll'])) {
					if (isset($data['Attendance']['is_restday'])) {
						$data['Attendance']['legal_pay'] = $data['Attendance']['per_hour'] * 2.3;
					} else {
						$data['Attendance']['legal_pay'] = $data['Attendance']['per_hour'] * 2;
					}
				} else {
					$data['Attendance']['legal_pay'] = $data['Attendance']['per_hour'];
					$data['Attendance']['is_lastpayroll'] = 1;
				}
				$data['Attendance']['is_legal'] = 1;
			} else {
				if (isset($data['Attendance']['is_restday'])) {
					$data['Attendance']['legal_pay'] = $data['Attendance']['per_hour'] * 1.5;
				} else {
					$data['Attendance']['special_pay'] = $data['Attendance']['per_hour'] * 1.30;
				}
			}
		}

		return $data;
	}

	public function lastpayrollChecker($data) {
		if (isset($data['Attendance']['is_lastpayroll'])) {
			$data['Attendance']['is_lastpayroll'] = 1;
			$data['Attendance']['lastpayroll_pay'] = $data['Attendance']['per_hour'];
			$data['Attendance']['lastpayroll_hours'] = $data['Attendance']['hours_work'];
		}
		return $data;
	}

	public function omitCheck($data) {
		if (isset($data['Attendance']['is_omit'])) {
			$omitdate = $data['Attendance']['omitDate'];
			unset($data['Attendance']['omitDate']);
			$data['Attendance']['omit_realdate'] = $data['Attendance']['date'];
			$omitdate = explode("-", $omitdate);
			$omitdate = $omitdate[2] . "-" . $omitdate[0] . "-" . $omitdate[1];
			$data['Attendance']['date'] = strtotime($omitdate);
		}
		return $data;
	}

	public function finalCheck($data) {

		if (isset($data['Attendance']['is_holiday'])) {
			if (!isset($data['Attendance']['is_restday'])) {
				if (isset($data['Attendance']['is_legal'])) {
					$data['Attendance']['legal_hours'] = $data['Attendance']['hours_work'];
					$data['Attendance']['grandtotal'] = $data['Attendance']['legal_pay'];
				} else {
					$data['Attendance']['special_hours'] = $data['Attendance']['hours_work'];
					$data['Attendance']['grandtotal'] = $data['Attendance']['special_pay'];
				}
			} else {
				if (isset($data['Attendance']['is_legal'])) {
					$data['Attendance']['grandtotal'] = $data['Attendance']['legal_pay'];
					$data['Attendance']['legal_pay'] = 0;
					$data['Attendance']['restday_pay'] = 0;

					if (isset($data['Attendance']['is_ot'])) {
						$data['Attendance']['otrdlegal_work'] = $data['Attendance']['ot_work'];
						$data['Attendance']['otrdlegal_pay'] = $data['Attendance']['ot_pay'];
						$data['Attendance']['ot_work'] = 0;
						$data['Attendance']['ot_pay'] = 0;
					}
				} else {
					$data['Attendance']['special_hours'] = $data['Attendance']['hours_work'];
					$data['Attendance']['grandtotal'] = $data['Attendance']['special_pay'];
				}
			}
			$data['Attendance']['hours_work'] = 0;
			$data['Attendance']['total'] = 0;
		}

		if (isset($data['Attendance']['is_restday'])) {
			if (!isset($data['Attendance']['is_holiday'])) {
				$data['Attendance']['restday_work'] = $data['Attendance']['hours_work'];
				$data['Attendance']['grandtotal'] = $data['Attendance']['restday_pay'] = $data['Attendance']['total'];
				$data['Attendance']['hours_work'] = 0;
				$data['Attendance']['total'] = 0;
			}
			
		}

		if (isset($data['Attendance']['is_night'])) {
			if (!isset($data['Attendance']['is_omit'])) {
				$data['Attendance']['night_hours'] = $data['Attendance']['hours_work'];
			}
		}

		if (isset($data['Attendance']['is_deduct'])) {
			$data['Attendance']['hours_work'] = 0;
			$data['Attendance']['total'] = 0;
		}

		if (isset($data['Attendance']['is_lastpayroll'])) {
			$data['Attendance']['hours_work'] = 0;
			$data['Attendance']['total'] = 0;
			$data['Attendance']['restday_work'] = 0;
			$data['Attendance']['restday_pay'] = 0;
			$data['Attendance']['legal_work'] = 0;
			$data['Attendance']['legal_pay'] = 0;
			$data['Attendance']['grandtotal'] = $data['Attendance']['lastpayroll_pay'];
		}

		if (isset($data['Attendance']['is_omit'])) {
			if (isset($data['Attendance']['is_deduct'])) {
				if (!isset($data['Attendance']['ot_hours'])) {
					$data['Attendance']['ot_hours'] = 0;
				}
				$data['Attendance']['deduct_hours'] = $data['Attendance']['hours_work'] + $data['Attendance']['ot_hours'];
				$data['Attendance']['grandtotal'] = 0;
				$data['Attendance']['deduct_basic'] = $data['Attendance']['total'];
				$data['Attendance']['deduct_pay'] = $data['Attendance']['grosspay'];
				$data['Attendance']['is_deduct'] = 1;
			} else {
				$data['Attendance']['omit_hours'] = $data['Attendance']['hours_work'];
				$data['Attendance']['grandtotal'] = $data['Attendance']['omit_pay'] = $data['Attendance']['total'];
			}
			$data['Attendance']['total'] = 0;
		} else {
			$data['Attendance']['grandtotal'] = $data['Attendance']['total'];
		}


		return $data;

	}

	public function loanChecker($data, $isLastWeek) {

		$loan = ClassRegistry::init('Loan')->find('first', array(
			'conditions' => array(
				'Loan.user_id' => $data['Attendance']['user_id'],
			),
		));

		$paidloan = ClassRegistry::init('Paidloan')->find('first', array(
			'conditions' => array(
				'Paidloan.user_id' => $data['Attendance']['user_id'],
				'Paidloan.week' => $data['Attendance']['week'],
			),
		));

		if (!empty($loan) && empty($paidloan)) {
			$prevTotal = $this->_getPervAttendanceTotal($data['Attendance']['user_id'], $data['Attendance']['week'], $data['Attendance']['year']);
			$netpay = $prevTotal['Attendance']['gross'] - $prevTotal['Attendance']['deduction'];

			if ($isLastWeek) {
				$sssloan = $loan['Loan']['sss_loan'];
				$pagibigloan = $loan['Loan']['pagibig_loan'];
				$calamityloan = $loan['Loan']['calamity_loan'];
			} else {
				$sssloan = ($loan['Loan']['totalsss_loan'] / 24) / 4;
				$pagibigloan = ($loan['Loan']['totalpagibig_loan'] / 24) / 4;
				$calamityloan = ($loan['Loan']['totalcalamity_loan'] / 24) / 4;
			}

			$totalLoan = $sssloan + $pagibigloan + $calamityloan;

			$totalNet = $netpay - $totalLoan;

			$ceil = ClassRegistry::init('Ceiling')->findById(1);
			if ($totalNet > $ceil['Ceiling']['ciel']) {

				$pl['Paidloan']['user_id'] = $data['Attendance']['user_id'];
				$pl['Paidloan']['week'] = $data['Attendance']['week'];
				$pl['Paidloan']['year'] = $data['Attendance']['year'];
				$pl['Paidloan']['sss_loan'] = $sssloan;
				$pl['Paidloan']['pagibig_loan'] = $pagibigloan;
				$pl['Paidloan']['calamity_loan'] = $calamityloan;

				if ($isLastWeek) {
					$loan['Loan']['sss_loan'] = 0;
					$loan['Loan']['pagibig_loan'] = 0;
					$loan['Loan']['calamity_loan'] = 0;
					$loan['Loan']['totalsss_loan'] = 0;
					$loan['Loan']['totalpagibig_loan'] = 0;
					$loan['Loan']['totalcalamity_loan'] = 0;
				} else {
					$loan['Loan']['sss_loan'] -= $sssloan;
					$loan['Loan']['pagibig_loan'] -= $pagibigloan;
					$loan['Loan']['calamity_loan'] -= $calamityloan;
				}
				ClassRegistry::init('Loan')->save($loan);
				ClassRegistry::init('Paidloan')->create();
				ClassRegistry::init('Paidloan')->save($pl);
			}
		}
	}

	public function _getPervAttendanceTotal($userid, $week, $year) {
		$this->virtualFields['gross'] = 'SUM(Attendance.grandtotal) + SUM(Attendance.night_pay) + SUM(Attendance.ot_pay)';
		$this->virtualFields['deduction'] = 'SUM(Attendance.sss) + SUM(Attendance.philhealth) + SUM(Attendance.pagibig)';
		$data = $this->find('first', array('fields' => array('Attendance.deduction', 'Attendance.gross'),
			'conditions' => array(
				'Attendance.user_id' => $userid,
				'Attendance.week' => $week,
				'Attendance.year' => $year,
			)));

		return $data;

	}

}