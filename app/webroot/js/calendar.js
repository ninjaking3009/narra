/* Calendar */

$(document).ready(function() {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultDate: '2015-01-21',
                defaultView: 'month',
                editable: true,
                events: [
                    {
                        title: 'Mrs. Bailey',
                        start: '2015-01-21'
                    }
                ]
            });

});