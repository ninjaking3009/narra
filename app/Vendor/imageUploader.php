<?php



/**

 * Image resize while uploading

 * @author Resalat Haque

 * @link http://www.w3bees.com/2013/03/resize-image-while-upload-using-php.html

 */



/**

 * Image resize

 * @param int $width

 * @param int $height

 */

class imageUploader

{

    

    function resize($data,$isType) {

        pr($data);

        $img_tmp = $data['tmp_name'];

        $name = $data['name'];

        $type = $data['type'];        

        list($w, $h) = getimagesize($img_tmp);
     
        /* calculate new image size with ratio */

        $width = $w;

        $height = $h;

        $ratio = max($width / $w, $height / $h);

        $h = ceil($height / $ratio);

        $x = ($w - $width / $ratio);

        $w = ceil($width / $ratio);



        $imgString = file_get_contents($img_tmp);

        $image = imagecreatefromstring($imgString);

        $tmp = imagecreatetruecolor($width, $height);

        imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);



        $date = new DateTime();

        $timestamp =  $date->getTimestamp();



        $filename = $timestamp . "_" . $name;



        if($isType == 'system') {

        	$path = WWW_ROOT . '/img/uploads/system/' . $filename;

        } else {

        	$path = WWW_ROOT . '/img/uploads/profile/' . $filename;

        }

        

        switch ($type) {

            case 'image/jpeg':

                imagejpeg($tmp, $path, 100);

                break;



            case 'image/png':

                imagepng($tmp, $path, 0);

                break;



            case 'image/gif':

                imagegif($tmp, $path);

                break;



            default:

                exit;

                break;

        }

        

        /* cleanup memory */

        imagedestroy($image);

        imagedestroy($tmp);



        return $filename;

    }

}

