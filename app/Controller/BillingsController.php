<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BillingsController extends AppController {

	var $uses = array('Attendance');

	public function index($week)
	{
		$this->layout = false;
		$this->autoRender = false;
		echo "Unable to load data. Missing dependencies, please install mx50010 plugin";
	}

	public function generate($date,$department)
	{
		// create new empty worksheet and set default font
		$this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 10);

		$border = array(
			'borders' => array(
				'outline' => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK,
					'color' => array('rgb' => '000000'),
				),
			)
		);

		$table = array(
		    array('label' => __('Names'), 'filter' => true,'border' => $border),
		    array('label' => __('June 07 to June 13, 2015'), 'filter' => true),
		    array('label' => __('TOTAL BASIC HRS.')),
		    array('label' => __('TOTAL OT')),
		    array('label' => __('OT PAY')),
		    array('label' => __('REST DAY')),
		    array('label' => __('GROSS PAY')),
		);
		$this->PhpExcel->addTableRow(array());
		$this->PhpExcel->addTableHeader($table, array('name' => 'Calibri'));
		$this->PhpExcel->setCellBorder('A2','G2',$border);
		$this->PhpExcel->mergeCell('A2','A6');

		$this->PhpExcel->output();
		exit();
		// add data
		foreach ($data as $d) {
		    $this->PhpExcel->addTableRow(array(
		        $d['User']['name'],
		        $d['Type']['name'],
		        $d['User']['date'],
		        $d['User']['description'],
		        $d['User']['modified']
		    ));
		}

		// close table and output
		$this->PhpExcel->addTableFooter()->output();
	}

}