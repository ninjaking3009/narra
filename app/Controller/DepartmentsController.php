<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DepartmentsController extends AppController {

	public function index()
	{
		$departments = $this->Department->find('all');
		$this->set(compact('departments'));
	}

	public function edit($id='')
	{
		if($this->request->is('post')) {
			if($this->Department->save($this->request->data)) {
				$this->Session->setFlash(__('Department updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Department unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		} else {
			$this->request->data = $this->Department->findById($id);
		}

	}

	public function add($id='')
	{

		if($this->request->is('post')) {
			if($this->Department->save($this->request->data)) {
				$this->Session->setFlash(__('Department added successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Department unable to add at this moment. Please contact system admin'), 'error_flash');
			}
		}

	}

	public function status($id='')
	{

		$data = $this->Department->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('Department unable to modify at this moment. Please contact system admin'), 'error_flash');
		}

		if($data['Department']['status'] == 1) {
			$data['Department']['status'] = 0;
		} else {
			$data['Department']['status'] = 1;
		}

		$this->Department->save($data);
		$this->Session->setFlash(__('Department updated successfully.'), 'success_flash');
		$this->redirect(array('action' => 'index'));
	}

}