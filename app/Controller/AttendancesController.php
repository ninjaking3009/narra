<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AttendancesController extends AppController {

	var $uses = array('Attendance','OtExccess','Role');

	public function index() {

	}

	public function edit($id = '') {
		if ($this->request->is('post')) {
			if ($this->Book->save($this->request->data)) {
				$this->Session->setFlash(__('Book updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Book unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		} else {
			$year = $this->Global->getYear();
			$level = $this->Global->getLevel();
			$this->request->data = $this->Book->findById($id);
			$this->set(compact('level', 'year'));
		}

	}

	public function weeklytotalbox()
	{


		if($this->request->is('Post')) {
			$this->layout = false;
			$data = $this->request->data;
			
			$tmpweek = $this->Attendance->find('all',array('fields' => array('week'),'conditions' => array('Attendance.year' => $data['year'],'Attendance.month' => $data['month']), 'order' => 'week DESC','group' => 'week'));
			
			$week = array();
			foreach ($tmpweek as $key => $value) {
				$week[$value['Attendance']['week']] = $this->Global->getWeekRange($value['Attendance']['week'],$data['year'], true);
			}

			echo json_encode($week);

			exit();
		}

		$tmpyear = $this->Attendance->find('all',array('fields' => array('year'),'order' => 'year DESC','group' => 'year'));
		
		$year = array();
		foreach ($tmpyear as $key => $value) {
			$year[$value['Attendance']['year']] = $value['Attendance']['year'];
		}

		$month = $this->Global->getMonth();

		$this->set(compact('year','month'));
	}

	public function perweeklytotal()
	{
		$this->layout = false;

		if($this->request->is('Post')) {
			$data = $this->request->data;
			$users = $this->Attendance->getUsers($data['week'],$data['year']);
		}

		$attendance = array();
		foreach ($users as $key => $value) {
			$attendance[$key]['User'] = $value['User'];
			$attendance[$key]['totals'] = $this->Attendance->getTotal($value['User']['id'], '', $data['week'], $data['year'],false,false);
			$attendance[$key]['omits'] = $this->Attendance->getTotal($value['User']['id'], '', $data['week'], $data['year'],true);
		}

		$this->set(compact('attendance'));

	}

	public function range() {
		$weeks = $this->Global->getWeeks(date('Y'));
		$this->set(compact('weeks'));

	}

	public function wrange() {
		$weeks = $this->Global->getWeeks(date('Y'));
		$this->set(compact('weeks'));

	}

	public function mrange() {
		$weeks = $this->Global->getWeeks(date('Y'));
		$this->set(compact('weeks'));

	}

	public function select($week) {
		$weeks = $this->Global->getWeekRange($week);

		$days = array();
		foreach ($weeks['days'] as $key => $value) {
			$days[strtotime($value)] = date('D M d, Y', strtotime($value));
		}

		$days = $this->Attendance->checkForHolidays($days, 'week');

		$omit = $this->Attendance->getOmit($week);

		$this->set(compact('days', 'week', 'omit'));
	}

	public function daystotal() {
		$this->layout = false;
		$day = date('D M d, Y', $this->request->data['date']);
		$totals = $this->Attendance->getTotalday($this->request->data['date'], $this->request->data['week']);
		$this->set(compact('totals', 'day'));
	}

	public function weekstotal($isExcess='') {
		$this->layout = false;
		if($isExcess=='') {
			$totals = $this->Attendance->getTotalweek($this->request->data['week']);
		} else {
			$totals = $this->OtExccess->find('all');
		}
		$weekrange = $this->Global->getWeekRange($this->request->data['week']);
		$this->set(compact('totals', 'weekrange'));
	}

	public function regenerate($week, $department) {

	}

	public function gensig($weeks,$dept,$isExcess='')
	{
		$this->layout = false;
		$this->autoRender = false;

		$days = $this->Global->getWeekRangeReport($weeks);
		$attendances = $this->Attendance->getWeeklyAttendance($weeks, $dept, date('Y'));
		App::import('Vendor', 'PHPExcel_IOFactory', array('file' => 'PHPExcel'.DS.'IOFactory.php'));

		$objPHPExcel = PHPExcel_IOFactory::load("signature.xlsx");

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1','Payroll '.$days['start']." to ".$days['end']);

		$c = 5;
		foreach ($attendances as $k => $v) {
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$c,ucfirst($v['User']['lname'].", ".$v['User']['fname']));
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$c,$v['Attendance']['totalWorkHours']);
			$c++;
		}

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$days['start']." to ".$days['end']."_".strtotime(date('Y-m-d H:m:s')).'.xlsx"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setPreCalculateFormulas(true);
		ob_end_clean();
		$objWriter->save('php://output');
		exit();

	}

	public function generate($weeks,$dept,$isExcess='')
	{
		$this->layout = false;
		$this->autoRender = false;

		$attpos = $this->Attendance->getWeeklyAttendancePosition($weeks, $dept, date('Y'));
		$days = $this->Global->getWeekRangeReport($weeks);
		if($isExcess != "7th") {
			$grand = $this->Attendance->getWeeklyAttendanceTotal($weeks, $dept, date('Y'));
		} else {
			$grand = $this->OtExccess->getExcessTotal($weeks, date('Y'),$dept);
		}

		App::import('Vendor', 'PHPExcel_IOFactory', array('file' => 'PHPExcel'.DS.'IOFactory.php'));
		$objPHPExcel = PHPExcel_IOFactory::load("payroll_template.xlsx");

		$daysPosition = array(
			0 => array(
					0 => 'B6',
					1 => 'C6',
				),
			1 => array(
					0 => 'D6',
					1 => 'E6',
				),
			2 => array(
					0 => 'F6',
					1 => 'G6',
				),
			3 => array(
					0 => 'H6',
					1 => 'I6',
				),
			4 => array(
					0 => 'J6',
					1 => 'K6',
				),
			5 => array(
					0 => 'L6',
					1 => 'M6',
				),
			6 => array(
					0 => 'N6',
					1 => 'O6',
				),
		);

		if($isExcess == '7th') {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B2:O2")->setCellValue('B2','EXCCESS OT');
		} else if($isExcess == 'omit') {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B2:O2")->setCellValue('B2','OMIT');
		} else {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B2:O2")->setCellValue('B2','WEEKLY PAYROLL');
		}

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1',$days['start']." to ".$days['end']);

		$dc = 0;
		foreach ($days['days'] as $key => $value) {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells($daysPosition[$dc][0].":".$daysPosition[$dc][1])->setCellValue($daysPosition[$dc][0],$value);
			$dc++;
		}

		$c=8;
		unset($key);
		unset($value);

		$styleArray = array(
		    'font'  => array(
		        'bold'  => true,
		    ));
		

		foreach ($attpos as $x => $y) {
			if($isExcess == '7th') {
				$attendances = $this->OtExccess->getExccess($weeks, $dept, date('Y'),false,false,$y['Role']['id']);
			} else {
				$attendances = $this->Attendance->getWeeklyAttendance($weeks, $dept, date('Y'),false,false,$y['Role']['id']);
			}
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$c,ucfirst($y['Role']['role']));
			$objPHPExcel->getActiveSheet()->getStyle('A'.$c)->applyFromArray($styleArray);
			$c+=1;
			foreach ($attendances as $k => $v) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$c,ucfirst($v['User']['lname'].", ".$v['User']['fname']));

				//setting day hours
				$d = 'B';
				$hr = 0;
				$day = 1;
				foreach ($days['days'] as $key => $value) {
					foreach ($v['Days'] as $k1 => $v1) {
						if($v1['Attendance']['date'] == $key) {
							if($v1['Attendance']['is_night'] != 1) {
								if(!empty($v1['Attendance']['hours_work'])) {
								    $hr = $v1['Attendance']['hours_work'];
								} else if(!empty($v1['Attendance']['lastpayroll_hours'])) {
								    $hr = $v1['Attendance']['lastpayroll_hours'];
								} else if(!empty($v1['Attendance']['special_hours'])) {
								    $hr = $v1['Attendance']['special_hours'];
								} else {
							    	$hr = $v1['Attendance']['legal_hours'];
								}
								if($hr == "") {
									$hr = 0;
								}
								$objPHPExcel->setActiveSheetIndex(0)->setCellValue($d.$c,$hr);
								$d++;
								$objPHPExcel->setActiveSheetIndex(0)->setCellValue($d.$c,0);
								$d++;
							} else {
								if(!empty($v1['Attendance']['night_hours'])) {
								    $hr = $v1['Attendance']['night_hours'];
								} else {
							    	$hr = 0;
								}

								if($hr == "")
									$hr = 0;

								$objPHPExcel->setActiveSheetIndex(0)->setCellValue($d.$c,0);
								$d++;
								$objPHPExcel->setActiveSheetIndex(0)->setCellValue($d.$c,$hr);
								$d++;
							}
							continue(2);
						}
					}
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($d.$c,0);
					$d++;
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($d.$c,0);
					$d++;
				}
				//end of setting day hours
				if($isExcess == 'omit') {
				
				} else if($isExcess == '7th') {
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("P".$c,$v['Attendance']['totalWorkHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Q".$c,$v['Attendance']['totalBasicPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("R".$c,$v['Attendance']['totalOtHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("S".$c,$v['Attendance']['totalOtPay']);
					/*$objPHPExcel->setActiveSheetIndex(0)->setCellValue("T".$c,$v['Attendance']['totalCola']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("U".$c,$v['Attendance']['totalSea']);*/
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("T".$c,$v['Attendance']['totalNightPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("U".$c,$v['Attendance']['totalSpecialHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("V".$c,$v['Attendance']['totalSpecialPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("W".$c,$v['Attendance']['totalLegalHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("X".$c,$v['Attendance']['totalLegalPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Y".$c,$v['Attendance']['totalOmitPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Z".$c,$v['Attendance']['grossPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AA".$c,$v['Attendance']['grandSss']['Deduction']['sss']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AB".$c,$v['Attendance']['grandSss']['Deduction']['philhealth']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AC".$c,$v['Attendance']['grandSss']['Deduction']['pagibig']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AD".$c,$v['Attendance']['sss_loan']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AE".$c,$v['Attendance']['pagibig_loan']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AF".$c,$v['Attendance']['calamity_loan']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AG".$c,$v['Attendance']['netPay']);
				} else {
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("P".$c,$v['Attendance']['totalWorkHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Q".$c,$v['Attendance']['totalBasicPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("R".$c,$v['Attendance']['totalOtHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("S".$c,$v['Attendance']['totalOtPay']);
					/*$objPHPExcel->setActiveSheetIndex(0)->setCellValue("T".$c,$v['Attendance']['totalCola']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("U".$c,$v['Attendance']['totalSea']);*/
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("T".$c,$v['Attendance']['totalNightPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("U".$c,$v['Attendance']['totalSpecialHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("V".$c,$v['Attendance']['totalSpecialPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("W".$c,$v['Attendance']['totalLegalHours']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("X".$c,$v['Attendance']['totalLegalPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Y".$c,$v['Attendance']['totalOmitPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Z".$c,$v['Attendance']['grossPay']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AA".$c,$v['Attendance']['grandSss']['Deduction']['sss']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AB".$c,$v['Attendance']['grandSss']['Deduction']['philhealth']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AC".$c,$v['Attendance']['grandSss']['Deduction']['pagibig']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AD".$c,$v['Attendance']['sss_loan']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AE".$c,$v['Attendance']['pagibig_loan']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AF".$c,$v['Attendance']['calamity_loan']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AG".$c,$v['Attendance']['netPay']);
				}
				$c++;
			}
			$c++;
 		}

 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("P".$c,$grand['Attendance']['totalWorkHours']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Q".$c,$grand['Attendance']['totalBasicPay']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("R".$c,$grand['Attendance']['totalOtHours']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("S".$c,$grand['Attendance']['totalOtPay']);
 		/*$objPHPExcel->setActiveSheetIndex(0)->setCellValue("T".$c,$grand['Attendance']['totalCola']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("U".$c,$grand['Attendance']['totalSea']);*/
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("T".$c,$grand['Attendance']['totalNightPay']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("U".$c,$grand['Attendance']['totalSpecialHours']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("V".$c,$grand['Attendance']['totalSpecialPay']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("W".$c,$grand['Attendance']['totalLegalHours']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("X".$c,$grand['Attendance']['totalLegalPay']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Y".$c,$grand['Attendance']['totalOmitPay']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("Z".$c,$grand['Attendance']['grossPay']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AA".$c,$grand['Attendance']['grandDedudction']['Deduction']['sss']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AB".$c,$grand['Attendance']['grandDedudction']['Deduction']['philhealth']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AC".$c,$grand['Attendance']['grandDedudction']['Deduction']['pagibig']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AD".$c,$grand['Attendance']['sss_loan']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AE".$c,$grand['Attendance']['pagibig_loan']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AF".$c,$grand['Attendance']['calamity_loan']);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("AG".$c,$grand['Attendance']['netPay']);
 		$objPHPExcel->getActiveSheet()->getStyle('P'.$c.':AG'.$c)->applyFromArray($styleArray);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$days['start']." to ".$days['end']."_".strtotime(date('Y-m-d H:m:s')).'.xlsx"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setPreCalculateFormulas(true);
		ob_end_clean();
		$objWriter->save('php://output');
		exit();

	}

	public function printreport($week, $department) {
		$this->layout = 'print';
		$weeks = $week;
		$dept = $department;
		$days = $this->Global->getWeekRangeReport($weeks);
		$attendances = $this->Attendance->getWeeklyAttendance($weeks, $dept, date('Y'),true);
		$this->set(compact('attendances', 'days'));

	}

	public function import($id="")
	{
		if ($this->request->is('post')) {
			$file = $this->request->data;

			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file['Import']['file']['tmp_name']);
			$highestRow = $spreadsheet->getActiveSheet()->getHighestRow();

			$cellValue = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(1,2)->getValue();
			for ($i=2; $i < $highestRow; $i++) { 
				$name = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(1,$i)->getValue();
				$unformatDate = $spreadsheet->getActiveSheet()->getCellByColumnAndRow(2,$i)->getValue();
				$date = trim(preg_replace("/\([^)]+\)/","",$unformatDate));
				echo $name."<br>";
				echo $date."<br>";
			}
		}
		exit();
	}

	public function proccess($value = '') {
		$this->layout = false;
		$this->autoRender = false;

		$isLastWeek = false;

		$data = $this->request->data;

		if (isset($this->request->data['Attendance']['is_omit'])) {
			$conditions = array(
				'user_id' => $data['Attendance']['user_id'],
				'omit_realdate' => $data['Attendance']['date'],
				'is_omit' => '1',
			);
		} else {
			$conditions = array(
				'user_id' => $data['Attendance']['user_id'],
				'date' => $data['Attendance']['date'],
			);
		}

		$isExist = $this->Attendance->find('first',
			array(
				'conditions' => $conditions,
			)
		);

		if (!empty($isExist)) {
			$this->Attendance->delete($isExist['Attendance']['id']);
			$this->OtExccess->deleteAll(array('date' => $data['Attendance']['date'], 'user_id' => $data['Attendance']['user_id']));
		}


		// this is check if omit
		$data = $this->Attendance->omitCheck($data);

		$holiday = ClassRegistry::init('Holiday')->find('first', array(
			'conditions' => array(
				'date' => $data['Attendance']['date'],
			),
		)
		);

		$data['Attendance']['year'] = date('Y');
		$data['Attendance']['date_added'] = date('Y-m-d h:i:s a');

		$data = $this->Attendance->procChecker($data);

		//basic pay
		$basicPay = $data['Attendance']['per_hour'];

		// this is checker if this day is for this month
		$data = $this->Global->monthChecker($data);

		// this is checker if this week is last week of the month
		$isLastWeek = $this->Global->lastMonthChecker($data);

		// this is checker for absent or not
		$data = $this->Attendance->absentChecker($data);

		// this is checker for restday or not
		$data = $this->Attendance->restdayChecker($data,$holiday);

		// this is checker for ot or not
		$data = $this->Attendance->otChecker($data,$holiday);

		// this is checker for ot or not
		$data = $this->Attendance->holidayChecker($data, $holiday);

		// this is checker for ot or not
		$data = $this->Attendance->lastpayrollChecker($data, $holiday);

		// this is get total
		$data = $this->Attendance->getAttendanceTotal($data);

		// this is checker for night or not
		$data = $this->Attendance->nightChecker($data);

		// this is get incentive
		//$data = $this->Attendance->getInsentive($data, $holiday);
		// final checker
		$data = $this->Attendance->finalCheck($data);

		// this is checker for reliever or not
		$data = $this->Attendance->relieverChecker($data);
		
		// this is get gross pay
		$data = $this->Attendance->getGrossPay($data);

		// loan checker
		$this->Attendance->save($data);

		// this is get get deductions
		$data = $this->Attendance->getDeductions($data);

		// this is get get deductions
		$data = $this->Attendance->getMonthlyDeductions($data);

		$data = $this->Attendance->loanChecker($data, $isLastWeek);
	}

	public function emp() {
		$this->layout = false;
		$id = $this->request->data['id'];
		$day = $this->request->data['date'];
		$week = $this->request->data['week'];
		/*$id = 18;
		$day = 1515279600;
		$week = 02;*/
		$this->Attendance->bind(array('User'));
		$this->Attendance->User->bind(array('Role'));
		$emp = $this->Attendance->User->findById($id);

		$isHoliday = $this->Attendance->checkForHolidays($day, 'day', true);

		$dayTemp = date('D', $day);

		if ($emp['User']['rest_day'] == $dayTemp) {
			$restday = '<span style="color: red">(Rest Day)</span>';
		} else {
			$restday = '';
		}

		$totals = $this->Attendance->getTotal($id, $day, $week, date('Y'));
		$totalomit = $this->Attendance->getTotal($id, $day, $week, date('Y'),true);

		$range = $this->Global->getWeekRange($week);

		$days = array();
		foreach ($range['days'] as $key => $value) {
			$days[strtotime($value)] = date('D M d, Y', strtotime($value));
		}

		$this->Attendance->bind(array('User'));
		$this->Attendance->User->bind(array('Role'));
		$rolelist = $this->Attendance->User->Role->find('list', array('fields' => array('Role.id', 'Role.role'), 'order' => 'Role.role',
		)
		);

		$this->request->data = $this->Attendance->find('first', array('conditions' => array('Attendance.date' => $day, 'Attendance.user_id' => $id)));

		$omit = $this->Attendance->find('first', array('conditions' => array('Attendance.omit_realdate' => $day, 'Attendance.user_id' => $id, 'is_omit' => 1)));

		if (!empty($omit['Attendance']['date'])) {
			$omit['Attendance']['date'] = date('m-d-Y', $omit['Attendance']['date']);
		}

		$this->set(compact('emp', 'omit', 'day', 'days', 'week', 'restday', 'range', 'rolelist', 'totals', 'totalomit', 'isHoliday'));
	}

	public function add() {
		$weeks = $this->Global->getWeeks(date('Y'));
		$this->set(compact('weeks'));
	}

	public function ajaxadd($day) {
		$this->layout = false;
		$this->Attendance->bind(array('User'));
		$payrolled = $this->Attendance->find('all', array(
			'fields' => array('User.*'),
			'conditions' => array(
				'OR' => array('Attendance.date' => $day, 'Attendance.omit_realdate' => $day),
			),
			'group' => 'Attendance.user_id',
			'order' => 'User.lname',
		));
		$this->set(compact('payrolled', 'day'));
	}

	public function ajaxupdatedpayrolled($day) {
		$this->layout = false;
		$this->Attendance->bind(array('User'));
		$payrolled = $this->Attendance->find('all', array(
			'fields' => array('User.*'),
			'conditions' => array(
				'OR' => array('Attendance.date' => $day, 'Attendance.omit_realdate' => $day),
			),
			'group' => 'Attendance.user_id',
			'order' => 'User.lname',
		));
		$this->set(compact('payrolled', 'day'));
	}

	public function ajaxdate($week) {

		$this->layout = false;

		$weeks = $this->Global->getWeekRange($week);
		$days = array();
		foreach ($weeks['days'] as $key => $value) {
			$days[strtotime($value)] = date('D M d, Y', strtotime($value));
		}

		$days = $this->Attendance->checkForHolidays($days, 'week');
		$this->set(compact('days', 'week'));
	}

	public function recomputededuction($id='')
	{
		$this->autoRender = $this->layout = false;
		ClassRegistry::init('Deduction')->deleteAll(array('user_id' => $id));
		$ded = ClassRegistry::init('Deduction')->find('all',array('conditions' => array('user_id' => $id)));
		$data = $this->Attendance->find('all',array('conditions' => array('user_id' => $id)));
		foreach ($data as $key => $value) {
			$tmp = $this->Attendance->getDeductions($value);
		}
		$ded = ClassRegistry::init('Deduction')->find('first',array('conditions' => array('user_id' => $id)));
		exit();
	}

	public function testing($id="")
	{
		$this->autoRender = $this->layout = false;
		$ded = ClassRegistry::init('Deduction')->find('all',array('conditions' => array('user_id' => $id)));
		//$ded = ClassRegistry::init('Deduction')->deleteAll(array('user_id' => $id));
		pr($ded);
		exit();
	}

	public function ommit($week = '') {
		$this->set(compact('week'));
	}
	public function reportw($isExcess = '') {
		$weeks = $this->Global->getWeeks(date('Y'));
		$deptlist = ClassRegistry::init('Department')->find('list', array(
			'fields' => array('id', 'name'),
			'conditions' => array(
				'status' => 1,
			),
			'order' => 'name',
		)
		);
		$title = '';
		if($isExcess == '7th')
			$title = 'OT Excess';
		elseif ($isExcess == 'omit')
			$title = 'Omits';

		$this->set(compact('weeks', 'deptlist','isExcess','title'));
	}

	public function weekreport($isExcess='') {
		$this->layout = false;
		$weeks = $this->request->data['week'];
		$dept = $this->request->data['dept'];
		$days = $this->Global->getWeekRangeReport($weeks);
		$attpos = $this->Attendance->getWeeklyAttendancePosition($weeks, $dept, date('Y'));
		if($isExcess != "7th") {
			$grand = $this->Attendance->getWeeklyAttendanceTotal($weeks, $dept, date('Y'));
		} else {
			$grand = $this->OtExccess->getExcessTotal($weeks, date('Y'),$dept);
		}
		foreach ($attpos as $key => &$value) {
			if($isExcess=='7th') {
				$value['Attendances'] = $this->OtExccess->getExccess($weeks,$dept, date('Y'),false,false,$value['Role']['id']);
			} else if($isExcess=='omit') {
				$value['Attendances'] = $this->Attendance->getWeeklyAttendance($weeks, $dept, date('Y'),false,true,$value['Role']['id']);
			} else {
				$value['Attendances'] = $this->Attendance->getWeeklyAttendance($weeks, $dept, date('Y'),false,false,$value['Role']['id']);
			}
		}
		$this->set(compact('attpos', 'days','grand'));
	}

	public function reportm() {
		$weeks = $this->Global->getMonth(date('Y'));
		$this->set(compact('weeks'));
	}

	public function monthreport() {
		$this->layout = false;
		$weeks = $this->request->data['week'];
		$attendances = $this->Attendance->getMonthlyAttendance($weeks, date('Y'));
		$this->set(compact('attendances'));
	}

	public function sss() {
		$month = $this->Global->getMonth();
		$this->set(compact('month'));
	}

	public function sssreport($value = '') {
		$this->layout = 'report';
		$month = $this->Global->getMonth($value);
		$sss = array();
		$sss = $this->Attendance->getSssReport($value);
		$this->set(compact('sss','month'));
	}

	public function ssstotal($value = '') {
		$this->layout = false;
		$month = $this->request->data['month'];
		$sss = array();
		$sss = $this->Attendance->getSssReport($month);
		$this->set(compact('sss','month'));
	}

	public function philhealth() {
		$month = $this->Global->getMonth();
		$this->set(compact('month'));
	}

	public function philhealthreport($value = '') {
		$this->layout = 'report';
		$month = $this->Global->getMonth($value);
		$sss = array();
		$sss = $this->Attendance->getSssReport($value);
		$this->set(compact('sss','month'));
	}

	public function philhealthtotal($value = '') {
		$this->layout = false;
		$month = $this->request->data['month'];
		$sss = array();
		$sss = $this->Attendance->getSssReport($month);
		$this->set(compact('sss','month'));
	}

	public function delete($id) {

		$this->layout = false;
		$this->autoRender = false;

		$data = $this->Attendance->findById($id);

		$this->Attendance->delete($id);

		// this is get get deductions
		$data = $this->Attendance->getDeductions($data);

		// this is get get deductions
		$data = $this->Attendance->getMonthlyDeductions($data);

	}

	public function deleteomit($week, $id) {
		$data = $this->Attendance->findById($id);

		if (empty($data)) {
			$this->Session->setFlash(__('Attendance unable to remove. Miscellaneous does not exist in the system'), 'error_flash');
			$this->redirect(array('action' => 'index'));
		}

		if ($this->Attendance->delete($id)) {
			$this->Session->setFlash(__('Attendance remove successfully.'), 'success_flash');
		} else {
			$this->Session->setFlash(__('Unable to remove Attendance at the moment.'), 'error_flash');
		}

		$this->redirect(array('action' => 'select', $week));
	}

}