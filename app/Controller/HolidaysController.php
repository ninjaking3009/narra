<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HolidaysController extends AppController {

	public function index()
	{
		$holidays = $this->Holiday->find('all');
		$this->set(compact('holidays'));
	}

	public function edit($id='')
	{
		if($this->request->is('post')) {

			$omitdate = $this->request->data['Holiday']['date'];
			$omitdate = explode("-", $omitdate);
			$omitdate = $omitdate[2]."-".$omitdate[0]."-".$omitdate[1];
			$this->request->data['Holiday']['date'] = strtotime($omitdate);
			
			if($this->Holiday->save($this->request->data)) {
				$this->Session->setFlash(__('Holiday updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Holiday unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		} else {
			$this->request->data = $this->Holiday->findById($id);
			$this->request->data['Holiday']['date'] = date('m-d-Y',$this->request->data['Holiday']['date']);
		}

	}

	public function add($id='')
	{

		if($this->request->is('post')) {

			$omitdate = $this->request->data['Holiday']['date'];
			$omitdate = explode("-", $omitdate);
			$omitdate = $omitdate[2]."-".$omitdate[0]."-".$omitdate[1];
			$this->request->data['Holiday']['date'] = strtotime($omitdate);
			$this->request->data['Holiday']['date_added'] = $this->Global->date();

			if($this->Holiday->save($this->request->data)) {
				$this->Session->setFlash(__('Holiday added successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Holiday unable to add at this moment. Please contact system admin'), 'error_flash');
			}
		}

	}

	public function delete($id='')
	{

		$data = $this->Holiday->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('Holiday unable to remove at this moment. Please contact system admin'), 'error_flash');
			$this->redirect(array('action' => 'index'));
		}

		$this->Holiday->delete($data['Holiday']['id']);
		$this->Session->setFlash(__('Holiday removed successfully.'), 'success_flash');
		$this->redirect(array('action' => 'index'));

	}

}