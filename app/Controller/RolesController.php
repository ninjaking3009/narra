<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class RolesController extends AppController {

	public function index()
	{
		$roles = $this->Role->find('all');
		$this->set(compact('roles'));
	}

	public function edit($id='')
	{
		if($this->request->is('post')) {
			if($this->Role->save($this->request->data)) {
				$this->Session->setFlash(__('Role updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Role unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		} else {
			$this->request->data = $this->Role->findById($id);
		}

	}

	public function add($id='')
	{

		if($this->request->is('post')) {
			if($this->Role->save($this->request->data)) {
				$this->Session->setFlash(__('Role added successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Role unable to add at this moment. Please contact system admin'), 'error_flash');
			}
		}

	}

	public function status($id='')
	{

		$data = $this->Role->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('Role unable to modify at this moment. Please contact system admin'), 'error_flash');
		}

		if($data['Role']['status'] == 1) {
			$data['Role']['status'] = 0;
		} else {
			$data['Role']['status'] = 1;
		}

		$this->Role->save($data);
		$this->Session->setFlash(__('Role updated successfully.'), 'success_flash');
		$this->redirect(array('action' => 'index'));
	}

}