<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

	var $uses = array('User','Loan');

	public function beforeFilter() {
	    $this->Auth->allow(array('login', 'register', 'forgot','redirectUser','_checkIfLogin'));
	    parent::beforeFilter();
	}

	private function _checkIfLogin() {
	    if ($this->Session->check('UserDataHolder')) {
	        $this->redirectUser();
	    }
	}

	private function redirectUser() {
		$userinfo = $this->Session->read('UserDataHolder');
		if($userinfo['User']['role_id'] == 1) {
			$this->redirect(array('controller' => 'admins', 'action' => 'dashboard'));
		}
	}

	public function dashboard()
	{

	}

	public function ajax()
	{
		$this->layout = false;
		$users = $this->User->_searchAjax($this->request->data['q']);
		$this->set(compact('users'));
	}

	public function lists($type='')
	{
		$this->User->bind(array('Role','Department'));

		$users = $this->User->find('all',array('order' => 'lname'));

		$this->set(compact('users'));
	}

	public function edit($id='')
	{

		if($this->request->is('post')) {

			$this->request->data['User']['modified_by'] = $this->Global->gli('id');
			$this->request->data['User']['date_modified'] = date('Y-m-d h:i:s a');

			if($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('User updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'lists'));
			} else {
				$this->Session->setFlash(__('User unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		}

		if($id == '') {
			$id = $this->Session->read('UserDataHolder');
			$id = $id['User']['id'];
		}

		$this->request->data = $this->User->findById($id);
		$this->User->bind(array('Role'));
		$rolelist = $this->User->Role->find('list',array('conditions' => array('Role.status' => 1,'NOT' => array('Role.id' => 7)),
					'fields' => array('Role.id','Role.role')
				,'order' => 'Role.role')
		);
		$deptlist = $this->User->Department->find('list',array(
					'fields' => array('Department.id','Department.name'),
					'conditions' => array('Department.status' => 1)
				,'order' => 'Department.name')
		);
		$this->set(compact('rolelist','deptlist'));

	}

	public function status($id='')
	{

		$data = $this->User->find('all',array('fields' => array('User.id','User.status'),'conditions' => array('User.id' => $id)));

		if(empty($data)) {
			$this->Session->setFlash(__('User unable to modify at this moment. Please contact system admin'), 'error_flash');
			$this->redirect(array('action' => 'lists'));
		}

		if($data['User']['status'] == 0) {
			$data['User']['status'] = 1;
		} else {
			$data['User']['status'] = 0;
		}

		$this->User->save($data);
		$this->Session->setFlash(__('User updated successfully.'), 'success_flash');
		$this->redirect(array('action' => 'lists'));
	}

	public function record($id='',$year='')
	{
		
		$info = $this->User->findById($id);

		$this->set(compact('info','id'));

	}

	public function report($id)
	{
		$this->layout = 'report';

		$year = date('Y');

		$month = $this->Global->getMonth();
		
		$record = array();
		$info = $this->User->findById($id);
		$this->User->bind(array('Attendance'));
		$years = $this->User->Attendance->find('all',array('fields' => 'year','conditions' => array('Attendance.user_id' => $id),'group' => 'Attendance.year'));
		foreach ($years as $key2 => $value2) {
			$record[$key2]['year'] = $value2['Attendance']['year'];
			$year = $value2['Attendance']['year'];
			foreach ($month as $key => $value) {
				$record[$key2]['months'][$key]['month'] = $value;
				$this->User->bind(array('Attendance'));
				$temp = $this->User->Attendance->find('all',array('conditions' => array('Attendance.user_id' => $id,'Attendance.month' => $key,'Attendance.year' => $year),'group' => 'Attendance.week'));
				foreach ($temp as $key1 => $value1) {
					$week['range'] = $this->Global->getWeekRange($value1['Attendance']['week'],$value1['Attendance']['year']);
					$record[$key2]['months'][$key]['Attendance'][$key1]['range'] = date('m-d-Y',strtotime($week['range']['start']))." - ".date('m-d-Y',strtotime($week['range']['end']));

					$temp1 = $this->User->Attendance->getTotal($id,'',$value1['Attendance']['week'],$value2['Attendance']['year'],false,true);
					$record[$key2]['months'][$key]['Attendance'][$key1]['Attendance'] = $temp1['Attendance'];
				}
			}
		}
		$this->set(compact('record','id','info'));
	}

	public function schedule($id)
	{
		$this->layout = false;

		$year = date('Y');

		$month = $this->Global->getMonth();
		
		$record = array();

		$this->User->bind(array('Attendance'));
		$years = $this->User->Attendance->find('all',array('fields' => 'year','conditions' => array('Attendance.user_id' => $id),'group' => 'Attendance.year'));
		foreach ($years as $key2 => $value2) {
			$record[$key2]['year'] = $value2['Attendance']['year'];
			$year = $value2['Attendance']['year'];
			foreach ($month as $key => $value) {
				$record[$key2]['months'][$key]['month'] = $value;
				$this->User->bind(array('Attendance'));
				$temp = $this->User->Attendance->find('all',array('conditions' => array('Attendance.user_id' => $id,'Attendance.month' => $key,'Attendance.year' => $year),'group' => 'Attendance.week'));
				foreach ($temp as $key1 => $value1) {
					$week['range'] = $this->Global->getWeekRange($value1['Attendance']['week'],$value1['Attendance']['year']);
					$record[$key2]['months'][$key]['Attendance'][$key1]['range'] = date('m-d-Y',strtotime($week['range']['start']))." - ".date('m-d-Y',strtotime($week['range']['end']));

					$temp1 = $this->User->Attendance->getTotal($id,'',$value1['Attendance']['week'],$value2['Attendance']['year'],false,true);
					$record[$key2]['months'][$key]['Attendance'][$key1]['Attendance'] = $temp1['Attendance'];
				}
			}
		}
		$this->set(compact('record','id'));
	}

	public function loan($id)
	{
		$this->layout = false;
		$loan = $this->Loan->find('all',array('conditions' => array('user_id' => $id),'order' => 'date_added DESC'));
		$this->set(compact('loan'));
	}

	public function delete($id)
	{
		$data = $this->User->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('User unable to remove. User does not exist in the system'), 'error_flash');
			$this->redirect(array('action' => 'lists'));
		}

		if($this->User->delete($id)) {
			$this->Session->setFlash(__('User remove successfully.'), 'success_flash');
		} else {
			$this->Session->setFlash(__('Unable to remove user at the moment.'), 'error_flash');
		}

		$this->redirect(array('action' => 'lists'));
	}

	public function login() {

		$this->redirect(array('controller' => 'staffs', 'action' => 'login'));

	    $this->_checkIfLogin();

	    $this->layout = 'login';


	    if ($this->request->is('post')) {

	        $userData = $this->User->find('first', 
	        		array('conditions' => 
	        			array(
	        				'email' => trim($this->request->data['login']['email']), 
	        				'password' => $this->User->passHasher($this->request->data['login']['password'])
	        			)
	        		));

	        if (!empty($userData)) {
	            if($userData['User']['status'] == 0) {
	              $this->Session->setFlash('Your account has been deactivated by admin.', 'error_flash');
	              $this->redirect(array('action' => 'login'));
	            }

	            if ($this->Auth->login($userData)) {
	                unset($userData['User']['password']);
	                $this->Session->write('UserDataHolder', $userData);
	                if($userData['User']['role_id'] == 1) {
		                $this->redirect(array('controller' => 'admins', 'action' => 'dashboard'));
	                }
	            }
	        } else {
	            $this->Session->setFlash('Sorry but your username or password is invalid.', 'error_flash');
	            $this->redirect('/');
	        }
	    }
	}

	public function loans($id)
	{

		if ($this->request->is('post')) {

			$this->request->data['Loan']['date_added'] = date('Y-m-d h:i:s a');

			if($this->request->data['Loan']['sss_loan'] == '') {
				$this->request->data['Loan']['sss_loan'] = 0;
			}
			if($this->request->data['Loan']['pagibig_loan'] == '') {
				$this->request->data['Loan']['pagibig_loan'] = 0;
			}
			if($this->request->data['Loan']['calamity_loan'] == '') {
				$this->request->data['Loan']['calamity_loan'] = 0;
			}

			$this->request->data['Loan']['totalsss_loan'] = $this->request->data['Loan']['sss_loan'];
			$this->request->data['Loan']['totalpagibig_loan'] = $this->request->data['Loan']['pagibig_loan'];
			$this->request->data['Loan']['totalcalamity_loan'] = $this->request->data['Loan']['calamity_loan'];

			$this->Loan->create();
			if ($this->Loan->save($this->request->data)) {
			    $this->Session->setFlash(__('The loan successfully added to the system.'), 'success_flash');
				$this->redirect(array('action' => 'loans',$id));
			}
		}

		$loan = $this->request->data = $this->Loan->findByUserId($id);
		$user = $this->User->findById($id);
		$this->set(compact('user','loan'));
	}

	public function register() {

	    if ($this->request->is('post')) {

	    	$this->request->data['User']['date'] = date('Y-m-d h:i:s a');

	    	$this->User->create();
	    	if ($this->User->save($this->request->data)) {
	    	    $this->Session->setFlash(__('The user successfully added to the system.'), 'success_flash');
	    		$this->redirect(array('action' => 'register'));
	    	} else {
	    		$this->User->bind(array('Role','Department'));
	    		$rolelist = $this->User->Role->find('list',array(
	    					'fields' => array('Role.id','Role.role'),
	    					'conditions' => array('Role.status' => 1),
	    				'order' => 'Role.role')
	    		);
	    		$deptlist = $this->Department->Role->find('list',array(
	    					'fields' => array('Department.id','Department.name'),
	    					'conditions' => array('Department.status' => 1),
	    				'order' => 'Department.name')
	    		);
	    		$this->set(compact('rolelist','deptlist'));
	    	}
	    }

	    $this->User->bind(array('Role','Department'));
	    $rolelist = $this->User->Role->find('list',array(
	    			'fields' => array('Role.id','Role.role'),
	    			'conditions' => array('Role.status' => 1)
	    		,'order' => 'Role.role')
	    );
	    $deptlist = $this->User->Department->find('list',array(
	    			'fields' => array('Department.id','Department.name'),
	    			'conditions' => array('Department.status' => 1)
	    		,'order' => 'Department.name')
	    );
	    $this->set(compact('rolelist','deptlist'));

	}

	public function logout()
	{
		$this->autoRender = false;
		if ($this->Session->delete('UserDataHolder')) {
		    $this->redirect($this->Auth->logout());
		}
	}

}
