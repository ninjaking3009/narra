<?php

App::uses('Component', 'Controller');

class GlobalComponent extends Component {

  public function startup(Controller $controller) {
      $this->Controller = $controller;
  }

  public function gli($type='',$id ='') {
    
    if($id == '') {
      $userinfo = CakeSession::read('UserDataHolder');
    } else {
      $userinfo = ClassRegistry::init('User')->findById($id);
    }

    if($type == '') {
      return $userinfo;
    } else {
      return $userinfo['User'][$type];
    }

  }

  public function date()
  {
    return date('Y-m-d h:i:s a');
  }

  public function generateRandomPassword($length = 8) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, strlen($characters) - 1)];
      }
      return $randomString;
  }

  public function getSystemInfo() {

      $data = ClassRegistry::init('System')->find('first');
      return $data;

  }

  public function getEmpInfo($id,$info = '') {

      $userinfo = ClassRegistry::init('User')->findById($id);

      if($info == '') {
        return $userinfo;
      } else {
        return $userinfo['User'][$info];
      }

      return $userinfo;

  }

  public function getPerHour($data)
  {
    return ($data / 8);
  }

  public function getSalaryTotal($cost,$hr)
  {
    return ($cost * $hr);
  }

  public function computeOt($hr,$basicPay)
  {
    return (($basicPay * 1.25) / 8) * $hr;
  }

  public function computeholiday($total)
  {
    return $total * 2;
  }

  public function computeNight($hr,$basicPay,$total)
  {
    return ($total * .10);
  }

  public function getIsoWeeksInYear($year) {
      $date = new DateTime;
      $date->setISODate($year, 53);
      return ($date->format("W") === "53" ? 53 : 52);
  }

  public function getWeeks($year)
  {
    $dateTemp = $year."-01-01";
    $endDate = $year."-12-31";

    $date = new DateTime($endDate);
    $lastWeekNumber = $this->getIsoWeeksInYear($year);
    $dateCont = array();

    for ($i=1; $i < $lastWeekNumber; $i++) {

      $currentDate = new DateTime($dateTemp);
      $weekNumber = $currentDate->format("W");

      $custom_date = strtotime( date('d-m-Y', strtotime($dateTemp)) ); 
      $week_start = date('D M d, Y', strtotime('this week last sunday', $custom_date));
      $week_end = date('D M d, Y', strtotime('this week next saturday', $custom_date));

      $dateCont[$weekNumber] = "Week ".$weekNumber." | ".$week_start." - ".$week_end;

      $dateTemp = date('Y-m-d', strtotime($dateTemp."+1 week"));

    }
    return $dateCont;
  }

  public function monthChecker($data,$year = '')
  {

    if($year == '') {
      $year = date('Y');
    }

    $week = $data['Attendance']['week'];

    $date =  strtotime(date('d-m-Y',strtotime($year.'W'.$week)));
    $start =  date('d-m-Y',strtotime($year.'W'.$week));
    $days['start'] = date('D M d, Y', strtotime('this week last sunday', $date));
    $days['end'] = date('D M d, Y', strtotime('this week next saturday', $date));

    $start_date = date('D Y-m-d',strtotime('this week last sunday', $date));

    $end_date = date ("Y-m-d", strtotime("+7 day", strtotime($start_date)));
      
    $monthTemp = array();

    while (strtotime($start_date) < strtotime($end_date)) {
      $days['days'][] = $start_date;
      $monthTemp[] = date ("m", strtotime($start_date));

      $start_date = date ("D Y-m-d", strtotime("+1 day", strtotime($start_date)));

    }

    $temp = $monthTemp = array_count_values($monthTemp);
    
    current($monthTemp);
    $startKey = key($monthTemp);

    end($monthTemp);
    $endKey = key($monthTemp);

    if($temp[$startKey] > $temp[$endKey]) {
      $data['Attendance']['month'] = $startKey;
    } else {
      $data['Attendance']['month'] = $endKey;
    }

    return $data;

  }

  public function lastMonthChecker($data)
  {
    $date = date('Y-m-d',$data['Attendance']['date']);
    return $this->weekCounter($date);
  }

  public function weekCounter($date)
  {
    
    $weekNum = $this->getWeekNumber($date);

    if($weekNum == 0) {
      return true;
    }

    $lastDayofMonth = date('Y-m-t',strtotime($date));
    $dayNumberOfLastDay = date('t',strtotime($date));
    $totalWeekNumber = $this->getWeekNumber($lastDayofMonth);

    if($totalWeekNumber == 5 && $weekNum == 4) {
      if($dayNumberOfLastDay >= 4) {
        return true;
      }
    }

    if($weekNum == $totalWeekNumber) {
      if($weekNum == 5) {
        $dayNumber = date('w',strtotime($date)) + 1;
        if($dayNumber >= 4) {
          return true;
        }
      }
    }

  }

  public function getWeekNumber($date)
  {
    $date_parts = explode('-', $date);
    $date_parts[2] = '01';
    $first_of_month = implode('-', $date_parts);
    $day_of_first = date('N', strtotime($first_of_month));
    $day_of_month = date('j', strtotime($date));
    return floor(($day_of_first + $day_of_month - 1) / 7);
  }

  public function getWeekRange($week,$year = '',$isRangeOnly = false) {

    if($year == '') {
      $year = date('Y');
    }

    $data = array();

    $date =  strtotime(date('d-m-Y',strtotime($year.'W'.$week)));
    $start =  date('d-m-Y',strtotime($year.'W'.$week));
    $data['start'] = date('D M d, Y', strtotime('this week last sunday', $date));
    $data['end'] = date('D M d, Y', strtotime('this week next saturday', $date));

    $start_date = date('Y-m-d',strtotime('this week last sunday', $date));

    $end_date = date ("Y-m-d", strtotime("+6 day", strtotime($start_date)));

    if($isRangeOnly) {
      return date ("D-d", strtotime($start_date))." - ".date ("D-d", strtotime($end_date));
    }
    while (strtotime($start_date) <= strtotime($end_date)) {
      $data['days'][] = $start_date;
      $start_date = date ("D Y-m-d", strtotime("+1 day", strtotime($start_date)));
    }

    return $data;


  }

  public function getWeekRangeReport($week,$year = '') {

    if($year == '') {
      $year = date('Y');
    }

    $data = array();

    $date =  strtotime(date('d-m-Y',strtotime($year.'W'.$week)));
    $start =  date('d-m-Y',strtotime($year.'W'.$week));
    $data['start'] = date('D M d, Y', strtotime('this week last sunday', $date));
    $data['end'] = date('D M d, Y', strtotime('this week next saturday', $date));

    $start_date = date('Y-m-d',strtotime('this week last sunday', $date));
    $start_date1 = date('D - d',strtotime('this week last sunday', $date));
    $end_date = date ("Y-m-d", strtotime("+7 day", strtotime($start_date)));
       
    while (strtotime($start_date) < strtotime($end_date)) {
      $data['days'][strtotime($start_date)] = $start_date1;
      $start_date = date ("D Y-m-d", strtotime("+1 day", strtotime($start_date)));
      $start_date1 = date ("D - d", strtotime($start_date));
    }

    return $data;


  }

  public function getMonth($id="")
  {
    $data = array(
      '01' => 'January',
      '02' => 'February',
      '03' => 'March',
      '04' => 'April',
      '05' => 'May',
      '06' => 'June',
      '07' => 'July',
      '08' => 'August',
      '09' => 'September',
      '10' => 'October',
      '11' => 'November',
      '12' => 'December'
    );
    if($id != "") {
      return $data[$id];
    } else {
      return $data;
    }
  }

  public function getMonthAndWeek($year='')
  {
    $month = $this->getMonth();
    pr($month);

    if($year == '') {
      $year = date('Y');
    }

    $tempDate = array();
    foreach ($month as $key => $value) {
        $ts  = strtotime("$year-$value-01");
        $ts  = strtotime("this week last sunday ".date('d-M-Y',strtotime("$year-$value-01")));
        $end = strtotime("$year-$value-01");
        $end = strtotime("this week last sunday ".date('t-M-Y',$end));
        $days = array();
        $days['Month'] = $value;
        while($ts <= $end) {
            $days[] = date('M-d-Y ',$ts)." - ".date('M-d-Y ',strtotime('+6 day ',$ts));
            $ts = strtotime('next sunday', $ts);
        }
        $tempDate[] = $days;
    }

    return $data;
  }

}