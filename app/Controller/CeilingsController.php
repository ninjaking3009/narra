<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CeilingsController extends AppController {

	public function edit($id='')
	{
		if($this->request->is('post')) {

			if($this->request->data['Ceiling']['ciel'] == '' || $this->request->data['Ceiling']['ciel'] < 0) {
				$this->request->data['Ceiling']['ciel'] = 0;
			}
			
			if($this->Ceiling->save($this->request->data)) {
				$this->Session->setFlash(__('Ceiling updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'edit',1));
			} else {
				$this->Session->setFlash(__('Ceiling unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		}

		$this->request->data = $this->Ceiling->findById($id);
	}

}