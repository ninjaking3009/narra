<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://Miscellaneous.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PhilhealthsController extends AppController {

	public function index()
	{
		$datas = $this->Philhealth->find('all', array('conditions' => array('premium' => '')));
		$this->set(compact('datas'));
	}

	public function indexw()
	{
		$datas = $this->Philhealth->find('all',array('conditions' => array('premium !=' => '')));
		$this->set(compact('datas'));
	}

	public function edit($id='')
	{
		if($this->request->is('post')) {
			if($this->Philhealth->save($this->request->data)) {
				$this->Session->setFlash(__('PhilHealth updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('PhilHealth unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		} else {
			$this->request->data = $this->Philhealth->findById($id);
		}
	}

	public function editw($id='')
	{
		if($this->request->is('post')) {
			if($this->Philhealth->save($this->request->data)) {
				$this->Session->setFlash(__('PhilHealth updated successfully.'), 'success_flash');
				$this->redirect(array('action' => 'indexw'));
			} else {
				$this->Session->setFlash(__('PhilHealth unable to edit at this moment. Please contact system admin'), 'error_flash');
			}
		} else {
			$this->request->data = $this->Philhealth->findById($id);
		}
	}

	public function add()
	{


		if($this->request->is('post')) {
			$this->request->data['Philhealth']['total'] = $this->request->data['Philhealth']['er'] + $this->request->data['Philhealth']['ee'];
			if($this->Philhealth->save($this->request->data)) {
				$this->Session->setFlash(__('PhilHealth added successfully.'), 'success_flash');
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('PhilHealth unable to add at this moment. Please contact system admin'), 'error_flash');
			}
		}

		$datas = $this->Philhealth->find('all', array('conditions' => array('premium' => '')));
		$this->set(compact('datas'));

	}

	public function addw()
	{


		if($this->request->is('post')) {
			if($this->Philhealth->save($this->request->data)) {
				$this->Session->setFlash(__('PhilHealth added successfully.'), 'success_flash');
				$this->redirect(array('action' => 'addw'));
			} else {
				$this->Session->setFlash(__('PhilHealth unable to add at this moment. Please contact system admin'), 'error_flash');
			}
		}

		$datas = $this->Philhealth->find('all',array('conditions' => array('premium !=' => '')));
		$this->set(compact('datas'));

	}

	public function delete($id)
	{
		$data = $this->Philhealth->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('PhilHealth unable to remove. Miscellaneous does not exist in the system'), 'error_flash');
			$this->redirect(array('action' => 'index'));
		}

		if($this->Philhealth->delete($id)) {
			$this->Session->setFlash(__('PhilHealth remove successfully.'), 'success_flash');
		} else {
			$this->Session->setFlash(__('Unable to remove Miscellaneous at the moment.'), 'error_flash');
		}

		$this->redirect(array('action' => 'index'));
	}

	public function deletew($id)
	{
		$data = $this->Philhealth->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('PhilHealth unable to remove. Miscellaneous does not exist in the system'), 'error_flash');
			$this->redirect(array('action' => 'indexw'));
		}

		if($this->Philhealth->delete($id)) {
			$this->Session->setFlash(__('PhilHealth remove successfully.'), 'success_flash');
		} else {
			$this->Session->setFlash(__('Unable to remove Miscellaneous at the moment.'), 'error_flash');
		}

		$this->redirect(array('action' => 'indexw'));
	}

	public function status($id='')
	{

		$data = $this->Year->findById($id);

		if(empty($data)) {
			$this->Session->setFlash(__('PhilHealth unable to modify at this moment. Please contact system admin'), 'error_flash');
		}

		if($data['Sss']['status'] == 1) {
			$data['Sss']['status'] = 0;
		} else {
			$data['Sss']['status'] = 1;
		}

		$this->Year->save($data);
		$this->Session->setFlash(__('PhilHealth updated successfully.'), 'success_flash');
		$this->redirect(array('action' => 'index'));
	}

}