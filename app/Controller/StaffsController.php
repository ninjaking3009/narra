<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class StaffsController extends AppController {

	public function beforeFilter() {
	    $this->Auth->allow(array('login', 'register', 'forgot','redirectUser','_checkIfLogin'));
	    parent::beforeFilter();
	}

	private function _checkIfLogin() {
	    if ($this->Session->check('UserDataHolder')) {
	        $this->redirectUser();
	    }
	}

	private function redirectUser() {
		$this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
	}

	public function login() {
	    $this->_checkIfLogin();

	    $this->layout = 'login';


	    if ($this->request->is('post')) {


	        $userData = $this->Staff->find('first', 
	        		array('conditions' => 
	        			array(
	        				'username' => trim($this->request->data['login']['username']), 
	        				'password' => $this->request->data['login']['password']
	        			)
	        		));

	        if (!empty($userData)) {
	            if($userData['Staff']['status'] == 0) {
	              $this->Session->setFlash('Your account has been deactivated by admin.', 'error_flash');
	              $this->redirect(array('action' => 'login'));
	            }

	            if ($this->Auth->login($userData)) {
	                unset($userData['Staff']['password']);
	                $this->Session->write('UserDataHolder', $userData);
	                $this->redirect(array('controller' => 'Users', 'action' => 'dashboard'));

	            }
	        } else {
	            $this->Session->setFlash('Sorry but your username or password is invalid.', 'error_flash');
	            $this->redirect('/');
	        }
	    }
	}

	public function logout()
	{
		$this->autoRender = false;
		if ($this->Session->delete('UserDataHolder')) {
		    $this->redirect($this->Auth->logout());
		}
	}

}
