<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <div class="sidebar-inner">

      <!-- Search form -->
      <!-- <div class="sidebar-widget">
        <form class="form-inline">
          <div class="input-append row-fluid">
            <input type="text" class="span8" placeholder="Search">
            <button type="submit" class="btn btn-info">Search</button>
          </div>
        </form>
      </div> -->

      <!--- Sidebar navigation -->
      <!-- If the main navigation has sub navigation, then add the class "has_submenu" to "li" of main navigation. -->
      <ul class="navi">

        <!-- Use the class nred, ngreen, nblue, nlightblue, nviolet or norange to add background color. You need to use this in <li> tag. -->

        <li class="nred"><a href="<?php echo $this->Html->url(array('controller'=>'admins','action'=>'dashboard')); ?>"><i class="icon-dashboard"></i> Dashboard</a></li>

        <!-- Menu with sub menu -->
        <li class="has_submenu nlightblue">
          <a href="#">
            <!-- Menu name with icon -->
            <i class="icon-user"></i> Users 
            <!-- Icon for dropdown -->
            <span class="pull-right"><i class="icon-angle-right"></i></span>
          </a>

          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'register')); ?>">Add User</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'lists')); ?>">All Users</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'lists','scheduler')); ?>">Schedulers</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'lists','adjuster')); ?>">Adjusters</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'lists','production')); ?>">Productions</a></li>
          </ul>
        </li>

        <li class="nviolet has_submenu"><a href="#"><i class="fa fa-asterisk"></i> Roles <span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <!-- <li><a href="<?php echo $this->Html->url(array('controller'=>'roles','action'=>'add')); ?>">Add</a></li> -->
            <li><a href="<?php echo $this->Html->url(array('controller'=>'roles','action'=>'index')); ?>">List</a></li>
          </ul>
        </li>

        <li class="ngreen"><a href="<?php echo $this->Html->url(array('controller'=>'systems')); ?>"><i class="fa fa-desktop"></i> System </a>
        </li>

        <li class="norange has_submenu"><a href="#"><i class="fa fa-cog"></i> Settings <span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'profile')); ?>">Profile</a></li>
          </ul>
        </li>

      </ul>

      <!-- Date -->

      <div class="sidebar-widget">
        <!-- <div id="todaydate"></div> -->
      </div>
    </div>
</div>
