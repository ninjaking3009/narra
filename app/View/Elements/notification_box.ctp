<!-- Notification box starts -->
   <div class="slide-box">

        <!-- Notification box head -->
        <div class="slide-box-head bred">
          <!-- Title -->
          <div class="pull-left">Notification Box</div>          
          <!-- Icon -->
          <div class="slide-icons pull-right">
            <a href="#" class="sminimize"><i class="icon-chevron-up"></i></a> 
          </div>
          <div class="clearfix"></div>
        </div>

        <div class="slide-content" style="display: none;">

          <!-- It is default bootstrap nav tabs. See official bootstrap doc for doubts -->
            <ul class="nav nav-tabs">
              <!-- Tab links -->
              <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-comments"></i></a></li>
            </ul>

            <!-- Tab content -->
            
            <div class="tab-content">

              <div class="tab-pane active" id="tab1">

                <h5>Have some content here.</h5>
                <p>Aliquam dui libero, pharetra nec venenatis in, scelerisque quis odio. In hac habitasse platea dictumst. Etiam porta placerat turpis, eget fermentum neque egestas at. Vestibulum ullamcorper, augue a sollicitudin vestibulum, orci purus semper felis, lobortis consequat nisi nunc eu enim. </p>           

              </div>           

            </div>

        </div>
      
    </div>