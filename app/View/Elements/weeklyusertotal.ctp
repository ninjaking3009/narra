<?php
    
    if(isset($attendance)) {
        $totals['Attendance'] = $attendance['totals']['Attendance'];
        $totalomit['Attendance'] = $attendance['omits']['Attendance'];
        $headText = ucfirst($attendance['User']['fname'])." ".ucfirst($attendance['User']['lname']);
    } else {
        $headText = "Total Summary from ".$range['start']." to ".$range['end'];
    }
?>
<div class="widget wblue">
        <!-- Widget title -->
        <div class="widget-head">
            <div class="pull-left"><?php echo $headText ?></div>
            <div class="clearfix"></div>
        </div>

        <div class="widget-content">
            <!-- Widget content -->
            <div class="padd">
                <!-- Contact box -->
                <div class="support-contact totalBox">
                    <div class="perTotalTitle">
                        Regular
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Basic Hours <strong>:</strong></div> <span class="totalHoursWorked"><?php echo $totals['Attendance']['totalWorkHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Basic Pay<strong>:</strong></div> <span class="totalBasicPay"><?php echo $totals['Attendance']['totalBasicPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Night Hours<strong>:</strong></div> <span class="totalNightHr"><?php echo $totals['Attendance']['totalNightHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Night Pay<strong>:</strong></div> <span class="totalNightPay"><?php echo $totals['Attendance']['totalNightPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total OT Hours<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total OT Pay<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            &nbsp;
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Reliever Pay<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalRelieverPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="perTotalTitle">
                        Holidays
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Legal Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totals['Attendance']['totalLegalHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Legal Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalLegalPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Last Payroll Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totals['Attendance']['totalLpHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Last Payroll Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalLpPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Special Hours<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalSpecialHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Special Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalSpecialPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="perTotalTitle">
                        Excess
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Restday Hours<strong>:</strong></div> <span class="totalRestday"><?php echo $totals['Attendance']['totalRestdayHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Restday Pay<strong>:</strong></div> <span class="totalRestday"><?php echo $totals['Attendance']['totalRestdayPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total OT Hours Exccess<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtExccess'] == '' ? 0 : $totals['Attendance']['totalOtExccess'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total OT Pay Exccess<strong>:</strong></div> <span class="totalOt"><?php echo $this->Number->currency($totals['Attendance']['totalOtPayExccess'], '') ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total RD Legal Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totals['Attendance']['totalRdlegalHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total RD Legal Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalRdlegalPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total OT RD Legal Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totals['Attendance']['totalOtrdlegalHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total OT RD Legal Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalOtrdlegalPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="perTotalTitle">
                        Adjustments
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Basic Hours <strong>:</strong></div> <span class="totalHoursWorked"><?php echo $totalomit['Attendance']['totalWorkHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Basic Pay<strong>:</strong></div> <span class="totalBasicPay"><?php echo $totalomit['Attendance']['totalBasicPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Night Hours<strong>:</strong></div> <span class="totalNightHr"><?php echo $totalomit['Attendance']['totalNightHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Night Pay<strong>:</strong></div> <span class="totalNightPay"><?php echo $totalomit['Attendance']['totalNightPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total OT Hours<strong>:</strong></div> <span class="totalOt"><?php echo $totalomit['Attendance']['totalOtHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total OT Pay<strong>:</strong></div> <span class="totalOt"><?php echo $totalomit['Attendance']['totalOtPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Legal Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totalomit['Attendance']['totalLegalHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Legal Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totalomit['Attendance']['totalLegalPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Last Payroll Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totalomit['Attendance']['totalLpHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Last Payroll Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totalomit['Attendance']['totalLpPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Total Special Hours<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totalomit['Attendance']['totalSpecialHours'] ?> hrs</span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Total Special Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totalomit['Attendance']['totalSpecialPay'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <!-- <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">COLA<strong>:</strong></div> <span class="totalCola"><?php echo $totalomit['Attendance']['totalCola'] ?></span>
                        </div>
                            <div class="fl fl-pay">
                                <div class="tb-lbl">SEA<strong>:</strong></div> <span class="totalSea"><?php echo $totalomit['Attendance']['totalSea'] ?></span>
                            </div>
                        <div class="clearfix"></div>
                    </div> -->
                    <!-- <div class="perTotalTitle">
                        Allowance
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">COLA<strong>:</strong></div> <span class="totalCola"><?php echo $totals['Attendance']['totalCola'] ?></span>
                        </div>
                            <div class="fl fl-pay">
                                <div class="tb-lbl">SEA<strong>:</strong></div> <span class="totalSea"><?php echo $totals['Attendance']['totalSea'] ?></span>
                            </div>
                        <div class="clearfix"></div>
                    </div> 
                    <hr> -->
                    <div class="perTotalBox">
                        <div class="fl">
                            <strong><div class="tb-lbl grossfield">Gross Pay:</div> <span class="totalGross bold"><?php echo $this->Number->currency(str_replace(',', '', $totals['Attendance']['grossPay']) + str_replace(',', '', $totalomit['Attendance']['grossPay']),''); ?></span></strong>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="perTotalTitle">
                        Adjustments Deductions
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">SSS Prem.<strong>:</strong></div> <span class="totalSss"><?php echo $totalomit['Attendance']['totalSss'] ?></span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">SSS Loan<strong>:</strong></div> <span class="totalSssLoan"><?php echo $totalomit['Attendance']['sss_loan'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">PhilHealth<strong>:</strong></div> <span class="totalPhilHealth"><?php echo $totalomit['Attendance']['totalPhilhealth'] ?></span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Pag-Ibig<strong>:</strong></div> <span class="totalPagIbig"><?php echo $totalomit['Attendance']['totalPagibig'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Pag-Ibig Loan<strong>:</strong></div> <span class="totalPagIbigLoan"><?php echo $totalomit['Attendance']['pagibig_loan'] ?></span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Calamity Loan<strong>:</strong></div> <span class="totalPagIbigLoan"><?php echo $totalomit['Attendance']['calamity_loan'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="perTotalTitle">
                        Deductions
                    </div>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">SSS Prem.<strong>:</strong></div> <span class="totalSss"><?php echo $totals['Attendance']['totalSss'] ?></span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">SSS Loan<strong>:</strong></div> <span class="totalSssLoan"><?php echo $totals['Attendance']['sss_loan'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">PhilHealth<strong>:</strong></div> <span class="totalPhilHealth"><?php echo $totals['Attendance']['totalPhilhealth'] ?></span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Pag-Ibig<strong>:</strong></div> <span class="totalPagIbig"><?php echo $totals['Attendance']['totalPagibig'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <div class="perTotalBox">
                        <div class="fl">
                            <div class="tb-lbl">Pag-Ibig Loan<strong>:</strong></div> <span class="totalPagIbigLoan"><?php echo $totals['Attendance']['pagibig_loan'] ?></span>
                        </div>
                        <div class="fl fl-pay">
                            <div class="tb-lbl">Calamity Loan<strong>:</strong></div> <span class="totalPagIbigLoan"><?php echo $totals['Attendance']['calamity_loan'] ?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr>
                    <strong><div class="tb-lbl totalnetfield">NET Pay:</div> <span class="totalNetPay bold"><?php echo $this->Number->currency(str_replace(',', '', $totals['Attendance']['netPay']) + str_replace(',', '', $totalomit['Attendance']['netPay']),''); ?></span></strong> 
                </div>
            </div>
        </div>

    </div>