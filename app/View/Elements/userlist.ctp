			<?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget wlightblue">

                    <div class="widget-head">
                        <div class="pull-left">User List</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content tblBorder">

                    <!-- <?php echo $this->form->create(null,array('url'=>array('controller' => 'search','action' => 'result'),'class'=>'form-horizontal')) ?>
                      <div class="input-append row-fluid">
                        <input type="text" class="span3" required="required" name="keyword" placeholder="Type in your keyword to search">
                        <button type="submit" class="btn btn-info">Search</button>
                      </div>
                    </form> -->

                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Last name</th>
                                    <th>First name</th>
                                    <th>Middle name</th>
                                    <th>SSS</th>
                                    <th>PhilHealth</th>
                                    <th>Pag-Ibig</th>
                                    <th>Position</th>
                                    <th>Department</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>



                                <?php foreach ($users as $key=> $value) { ?>
                                <tr>
                                    <td>
                                        <?php echo $value['User']['empid']; ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['User']['lname']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['User']['fname']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['User']['mname']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['User']['sss']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['User']['philhealth']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['User']['pagibig']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['Role']['role']); ?>
                                    </td>
                                    <td>
                                        <?php echo ucfirst($value['Department']['name']); ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-mini btn-success" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'record',$value['User']['id'])); ?>"><i class="fa fa-book"></i> </a>

                                        <a class="btn btn-mini btn-warning" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'edit',$value['User']['id'])); ?>"><i class="fa fa-pencil"></i> </a>

                                        <a class="btn btn-mini btn-primary" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'loans',$value['User']['id'])); ?>"><i class="fa fa-dollar"></i> </a>

                                        
                                        <!-- <a class="btn btn-mini <?php echo $value['User']['status'] == 0 ? 'btn-success' : 'btn-warning' ?>" title="<?php echo $value['User']['status'] == 0 ? 'Click to change user status to ACTIVE' : 'Click to change user status to INACTIVE' ?>" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'status',$value['User']['id'])); ?>">
                                                <i class="fa <?php echo $value['User']['status'] == 1 ? 'fa-eye-slash' : 'fa-eye' ?>"></i> 
                                        </a>
                                         -->
                                        <a class="btn btn-mini btn-danger" data-toggle="modal" href="#confirmModal<?php echo $value['User']['id'] ?>"><i class="icon-remove"></i></a>

                                        <div id="confirmModal<?php echo $value['User']['id'] ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Confirm Delete</h3>
                                          </div>
                                          <div class="modal-body">
                                            <p>You are about to completely remove <?php echo ucfirst($value['User']['fname'])." ".ucfirst($value['User']['lname']) ?>. Are you sure you want to continue?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'delete',$value['User']['id'])); ?>">Yes, Delete Permanently</a>
                                          </div>
                                        </div>

                                    </td>

                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>



                    </div>
                </div>
            </div>

        </div>