<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <div class="sidebar-inner">

      <div class="sidebar-widget">
        <?php echo $this->form->create(null,array('url'=>array('controller' => 'search','action' => 'result'),'class'=>'form-horizontal')) ?>
          <div class="input-append row-fluid">
            <input type="text" class="span8" name="keyword" placeholder="Search">
            <button type="submit" class="btn btn-info">Search</button>
          </div>
        </form>
      </div>

      <!--- Sidebar navigation -->
      <!-- If the main navigation has sub navigation, then add the class "has_submenu" to "li" of main navigation. -->
      <ul class="navi">

        <!-- Use the class nred, ngreen, nblue, nlightblue, nviolet or norange to add background color. You need to use this in <li> tag. -->

        <li class="nred"><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'dashboard')); ?>"><i class="icon-dashboard"></i> Dashboard</a></li>

        <!-- Menu with sub menu -->
        <li class="has_submenu nlightblue">
          <a href="#">
            <!-- Menu name with icon -->
            <i class="icon-user"></i> Employees 
            <!-- Icon for dropdown -->
            <span class="pull-right"><i class="icon-angle-right"></i></span>
          </a>

          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'register')); ?>">Add</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'lists')); ?>">List</a></li>
          </ul>
        </li>

        <li class="ngreen">
          <a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'add')); ?>">
            <!-- Menu name with icon -->
            <i class="icon-user"></i> Payroll 
            <!-- Icon for dropdown -->
          </a>
        </li>

        <li class="has_submenu ngreen">
          <a href="#">
            <!-- Menu name with icon -->
            <i class="icon-book"></i>Report
            <!-- Icon for dropdown -->
            <span class="pull-right"><i class="icon-angle-right"></i></span>
          </a>

          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'weeklytotalbox')); ?>">Payroll Summary</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'reportw')); ?>">Payroll Weekly</a></li>
            <!-- <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'reportw','omit')); ?>">Omit Payroll Weekly</a></li> -->
            <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'reportw','7th')); ?>">7th Payroll Weekly</a></li>
            <!-- <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'reportm')); ?>">Payroll Monthly</a></li> -->
            <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'sss')); ?>">SSS</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'attendances','action'=>'philhealth')); ?>">Philhealth</a></li>
          </ul>
        </li>

        <li class="nblue">
          <a href="<?php echo $this->Html->url(array('controller'=>'billings','action'=>'index')); ?>">
            <!-- Menu name with icon -->
            <i class="icon-user"></i> Billing 
            <!-- Icon for dropdown -->
          </a>
        </li>

        <li class="has_submenu norange">
          <a href="#">
            <!-- Menu name with icon -->
            <i class="fa fa-archive"></i> SSS 
            <!-- Icon for dropdown -->
            <span class="pull-right"><i class="icon-angle-right"></i></span>
          </a>

          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'sss','action'=>'index')); ?>">Monthly</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'sss','action'=>'indexw')); ?>">Weekly</a></li>
          </ul>
        </li>

        <li class="has_submenu ngreen">
          <a href="#">
            <!-- Menu name with icon -->
            <i class="fa fa-archive"></i> PhilHealth 
            <!-- Icon for dropdown -->
            <span class="pull-right"><i class="icon-angle-right"></i></span>
          </a>

          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'philhealths','action'=>'index')); ?>">Monthly</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'philhealths','action'=>'indexw')); ?>">Weekly</a></li>
          </ul>
        </li>

        <li class="nred has_submenu"><a href="#"><i class="fa fa-calendar"></i> Holidays <span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'holidays','action'=>'add')); ?>">Add</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'holidays','action'=>'index')); ?>">List</a></li>
          </ul>
        </li>

        <li class="nviolet has_submenu"><a href="#"><i class="fa fa-asterisk"></i> Positions <span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'roles','action'=>'add')); ?>">Add</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'roles','action'=>'index')); ?>">List</a></li>
          </ul>
        </li>

        <li class="nviolet has_submenu"><a href="#"><i class="fa fa-cubes"></i>Department <span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'departments','action'=>'add')); ?>">Add</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'departments','action'=>'index')); ?>">List</a></li>
          </ul>
        </li>
        
        <li class="ngreen">
          <a href="<?php echo $this->Html->url(array('controller'=>'ceilings','action'=>'edit',1)); ?>">
            <!-- Menu name with icon -->
            <i class="icon-user"></i> Ceiling 
            <!-- Icon for dropdown -->
          </a>
        </li>

      </ul>

      <!-- Date -->

      <div class="sidebar-widget">
        <div id="todaydate"></div>
      </div>
    </div>
</div>
