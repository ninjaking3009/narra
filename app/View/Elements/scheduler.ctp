<div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <div class="sidebar-inner">

      <div class="sidebar-widget">
        <?php echo $this->form->create(null,array('url'=>array('controller' => 'search','action' => 'result'),'class'=>'form-horizontal')) ?>
          <div class="input-append row-fluid">
            <input type="text" class="span8" required="required" name="keyword" placeholder="Search">
            <button type="submit" class="btn btn-info">Search</button>
          </div>
        </form>
      </div>

      <!--- Sidebar navigation -->
      <!-- If the main navigation has sub navigation, then add the class "has_submenu" to "li" of main navigation. -->
      <ul class="navi">

        <!-- Use the class nred, ngreen, nblue, nlightblue, nviolet or norange to add background color. You need to use this in <li> tag. -->

        <li class="nred"><a href="<?php echo $this->Html->url(array('controller'=>'schedulers','action'=>'dashboard')); ?>"><i class="icon-dashboard"></i> Dashboard</a></li>

        <li class="ngreen has_submenu"><a href="#"><i class="fa fa-group"></i> Clients<span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'clients','action' => 'register')); ?>">Register</a></li>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'users','action' => 'lists','client')); ?>">List</a></li>
          </ul>
        </li>

        <li class="nviolet"><a href="<?php echo $this->Html->url(array('controller'=>'adjusters','action'=>'lists')); ?>"><i class="fa fa-wrench"></i> Adjusters</a></li>

        <li class="nlightblue"><a href="<?php echo $this->Html->url(array('controller'=>'calendars','action'=>'index')); ?>"><i class="icon-calendar"></i> Schedules</a></li>

        <li class="norange has_submenu"><a href="#"><i class="fa fa-cog"></i> Settings <span class="pull-right"><i class="icon-angle-right"></i></span></a>
          <ul>
            <li><a href="<?php echo $this->Html->url(array('controller'=>'profile')); ?>">Profile</a></li>
          </ul>
        </li>

      </ul>

      <!-- Date -->

      <div class="sidebar-widget">
        <!-- <div id="todaydate"></div> -->
      </div>
    </div>
</div>
