<div class="admin-form">
  <div class="container-fluid">

    <div class="row-fluid">
      <div class="span12">
        <!-- Widget starts -->
            <div class="widget worange">
              <!-- Widget head -->
              <div class="widget-head">
                <i class="icon-lock"></i> Login 
              </div>

              <div class="widget-content">
                <div class="padd">
                  <!-- Login form -->
                  <?php echo $this->Session->flash(); ?>
                  <?php echo $this->Form->create('login',array('class' => 'form-horizontal')); ?>
                    <!-- Email -->
                    <div class="control-group">
                      <label class="control-label" for="inputEmail">Username</label>
                      <div class="controls">
                      <?php echo $this->Form->input('username',array('label'=>false,'placeholder' => 'Username')); ?>
                      </div>
                    </div>
                    <!-- Password -->
                    <div class="control-group">
                      <label class="control-label" for="inputPassword">Password</label>
                      <div class="controls">
                        <?php echo $this->Form->input('password',array('label'=>false,'placeholder' => 'Password')); ?>
                      </div>
                    </div>
                    <!-- Remember me checkbox and sign in button -->
                    <div class="control-group">
                      <div class="controls">
                        <!-- <label class="checkbox">
                          <input type="checkbox"> Remember me
                        </label>
                        <br> -->
                        <button type="submit" class="btn btn-danger">Sign in</button>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
                <div class="widget-foot">
                  Narra Payroll System ©<?php echo date('Y') ?>
                </div>
            </div>  
      </div>
    </div>
  </div> 
</div>