<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Holidays
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">

        <?php echo $this->Session->flash(); ?>

            <div class="span6">

                <div class="widget wred">

                    <div class="widget-head">
                        <div class="pull-left">Edit Position</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('edit',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">Holiday</label>
                                <div class="controls">
                                    <?php echo $this->Form->hidden('Holiday.id',array('label'=>false,'class' => 'input-large','type' => 'text', 'required' => 'required')); ?>
                                    <?php echo $this->Form->input('Holiday.name',array('label'=>false,'class' => 'input-large','type' => 'text', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">Date</label>
                                <div class="controls">
                                    <div class="input-append datetimepicker1">
                                        <?php echo $this->Form->input('Holiday.date',array('div' => false, 'label'=>false,'class' => 'date_schedule1','type' => 'text', 'data-format' => 'MM-dd-yyyy', 'required' => 'required')); ?>
                                      <span class="add-on">
                                        <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">
                                        </i>
                                      </span>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">Legal</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Holiday.is_legal',array('div'=>false,'label'=>false,'class' => 'input-large','type' => 'checkbox')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">Special</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Holiday.is_special',array('div'=>false,'label'=>false,'class' => 'input-large','type' => 'checkbox')); ?>
                                </div>
                            </div>

                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <?php echo $this->Form->button('Update Holiday', array('type' => 'submit','class' => 'btn btn-success')); ?>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
