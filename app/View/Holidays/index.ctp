<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Holidays
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
			<?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span6">
                <div class="widget wred">

                    <div class="widget-head">
                        <div class="pull-left">Holidays List</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Legal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>



                                <?php foreach ($holidays as $key=> $value) { ?>
                                <tr>
                                    <td>
                                        <?php echo ucfirst($value[ 'Holiday'][ 'name']); ?>
                                    </td>
                                    <td>
                                        <?php echo date('D M d, Y',$value[ 'Holiday'][ 'date']); ?>
                                    </td>
                                    <td>
                                        <?php echo ($value['Holiday']['is_legal'] == 1 ? 'Yes' : 'No') ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-mini btn-warning" href="<?php echo $this->Html->url(array('action' => 'edit',$value['Holiday']['id'])); ?>"><i class="icon-pencil"></i> </a>
                                        
                                        <a class="btn btn-mini btn-danger" data-toggle="modal" href="#confirmModal<?php echo $value['Holiday']['id'] ?>"><i class="icon-remove"></i></a>
                                        
                                        <div id="confirmModal<?php echo $value['Holiday']['id'] ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Confirm Delete</h3>
                                          </div>
                                          <div class="modal-body">
                                            <p>You are about to completely remove this. Are you sure you want to continue?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <a class="btn btn-danger" href="<?php echo $this->Html->url(array('action' => 'delete',$value['Holiday']['id'])); ?>">Yes, Delete Permanently</a>
                                          </div>
                                        </div>

                                    </td>

                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>



                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
