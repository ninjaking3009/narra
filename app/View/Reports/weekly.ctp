<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left"><a class="backBtn" href="<?php echo $this->Html->url(array('action' => 'select',$week)) ?>"><i class="fa fa-arrow-left"></i></a> Report for Week <?php echo $week ?>
  </h2>
    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">

            <div class="span8">
                
            </div>
        </div>
    </div>
</div>

<!-- Matter ends -->

<script type="text/javascript">
    jQuery(document).ready(function($){
        var id = $('#posSelect').val();
        $('#position'+id).show();

        $('#posSelect').on('change', function() {
          var id = this.value;
          $('.positionBoxWrap').hide();
          $('#position'+id).show();
        });

        $('.row-fluid').on('click','.makePayrollBtn',function(){
            var userid = $(this).data('id');
            $.ajax({
               url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
               data: {
                id:userid,
                date: '<?php echo $day; ?>',
                week: '<?php echo $week; ?>',
               },
               cache: false,
               success: function(data) {
                    $('.attendanceContainer').html('');
                    $('.attendanceContainer').html(data);
               },
               type: 'POST'
            });
        });

    });
</script>

<script type="text/javascript">
    $(function() {

        $('.attendanceContainer').on('click','.absentChk',function(){
            if($(this).is(':checked')) {
                $('.pField').attr('disabled','disabled');
                $('.hrsWorked').val('');
            } else {
                $('.pField').removeAttr('disabled');
            }
        });
        
        $('.attendanceContainer').on('click','.omitChk',function(){
            if($(this).is(':checked')) {
                $('.omitBox').show();
            } else {
                $('.omitBox').hide();
            }
        });
        
        $('.attendanceContainer').on('click','.relieverChk',function(){
            if($(this).is(':checked')) {
                $('.relieverBox').show();
            } else {
                $('.relieverBox').hide();
            }
        });

        $('.attendanceContainer').on('click','.otChk',function(){
            if($(this).is(':checked')) {
                $('.otBox').show();
            } else {
                $('.otBox').hide();
            }
        });

        $('.hrsWorked').on('input propertychange paste', function() {

        });

        $('.attendanceContainer').on('submit','#addEmpForm',function(event){

            $.post("<?php echo $this->Html->url(array('action' => 'proccess')) ?>",$("#addEmpForm").serialize(), function(data){
                window.location.href = $(location).attr('href');
            });

            event.preventDefault();
            return false;
        });

    });
</script>