<?php 
	if(empty($users)) {
		echo '<div class="ajaxEmpty">No record found.</div>';
	}
?>
<ul>
	<?php foreach ($users as $key => $value) {
		echo '<li data-id="'.$value["User"]["id"].'">'.ucfirst($value["User"]["lname"]).' '.ucfirst($value["User"]["fname"]).' '.ucfirst($value["User"]["mname"]).'</li>';
	} ?>
</ul>