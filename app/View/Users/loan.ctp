<div class="row-fluid">
    <div class="span12">
        <div class="widget wviolet">

            <div class="widget-head">
                <div class="pull-left">Loan Record</div>
                <div class="widget-icons pull-right">
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content tblBorder">

                <table class="table  table-bordered ">
                    <thead>
                        <tr>
                            <th>Date Deducted</th>
                            <th>SSS Loan</th>
                            <th>Calamity Loan</th>
                            <th>Pag-ibig Loan</th>
                            <th>Advance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($loan as $key => $value) { 
                        ?>
                        	<tr>
                        		<td><?php echo date('M d, Y',strtotime($value['Loan']['date_added'])) ?></td>
                        		<td><?php echo $this->Number->currency($value['Loan']['sss_loan'], '');  ?></td>
                        		<td><?php echo $this->Number->currency($value['Loan']['calamity_loan'], '');  ?></td>
                        		<td><?php echo $this->Number->currency($value['Loan']['pagibig_loan'], '');  ?></td>
                        		<td><?php echo $this->Number->currency($value['Loan']['advance'], '');  ?></td>
                        	</tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
