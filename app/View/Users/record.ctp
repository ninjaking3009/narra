<!-- Page heading -->
<div class="page-head">
  <!-- Page heading -->
  <h2 class="pull-left"><?php echo ucfirst($info['User']['lname']).", ".ucfirst($info['User']['fname'])." ".ucfirst($info['User']['mname']) ?>
    <!-- page meta -->
    <span class="page-meta">Employee's Earnings Record</span>
  </h2>

  <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
  <div class="container-fluid">
    	
    	<div class="rangeSelectBox">
    	    <div class="perSb dayRange"><span>Select Report</span>
		    	<select id="DateRange1" required="required" class="input-large">
			    	<option value="schedule">Attendance Record</option>
			    	<option value="loans">Loans Record</option>
		    	</select>
    		</div>
    	    <div class="clearfix"></div>
    	</div>

    	<div class="reportContainer">
    		
    	</div>

  </div>
</div>

<!-- Matter ends -->

<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function($){

		function getReport() {
			var report = $('#DateRange1').val();
			if(report == 'schedule') {
				link = '<?php echo $this->Html->url(array("action" => "schedule",$id)) ?>';
			} else if(report == 'loans') {
				link = '<?php echo $this->Html->url(array("action" => "loan",$id)) ?>';
			}

			$.ajax({
				url: link,
				type: 'POST',
			})
			.done(function(data) {
				$('.reportContainer').html(data);
			});
		}

		getReport();

		$('#DateRange1').change(function(){
			getReport();
		});

		
		

	});
</script>