<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Users
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">

        <?php echo $this->Session->flash(); ?>

        <div class="row-fluid">

            <div class="span12">

                <div class="widget wgreen">
                    <div class="widget-head">
                        <div class="pull-left">Register</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">
                        <div class="padd">

                            <!-- Profile form -->

                            <div class="form profile">

                                <?php echo $this->form->create('register',array('class'=>'form-horizontal')) ?>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Employee ID </label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.empid',array('label'=>false,'class' => 'input-large')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Last Name <span>*</span></label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.lname',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">First Name <span>*</span></label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.fname',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                    </div>
                                </div>


                                <div class="control-group">
                                    <label for="name1" class="control-label">Mid Name</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.mname',array('label'=>false,'class' => 'input-large'
                                        )); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Gender</label>
                                    <div class="controls">
                                        <?php $gender= array( 'male'=> 'Male','female' => 'Female'); echo $this->Form->input('User.gender',array('options' => $gender, 'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name1">Birthdate</label>
                                    <div class="controls">
                                        <div class="input-append datetimepicker1">
                                          <input type="text" class="date_schedule" class="date_schedule" name="data[User][birthday]" data-format="MM-dd-yyyy">
                                          <span class="add-on">
                                            <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">
                                            </i>
                                          </span>
                                        </div>
                                    </div>
                                </div>

                                

                                <div class="control-group">
                                    <label for="name1" class="control-label">Address</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.address',array('label'=>false,'class' => 'input-large')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Contact Number</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.contact',array('label'=>false,'class' => 'input-large')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">SSS</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.sss',array('label'=>false,'class' => 'input-large sss')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">PhilHealth</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.philhealth',array('label'=>false,'class' => 'input-large philhealth')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Pag-Ibig</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.pagibig',array('label'=>false,'class' => 'input-large')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">TIN</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.tin',array('label'=>false,'class' => 'input-large tin')); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Rest Day</label>
                                    <div class="controls">
                                        <?php 
                                            $restday = array('Mon' => 'Monday','Tue' => 'Tuesday','Wed' => 'Wednesday','Thu' => 'Thursday','Fri' => 'Friday','Sat' => 'Saturday','Sun' => 'Sunday');
                                        echo $this->Form->input('User.rest_day', array( 'label'=>false, 'options' => $restday )); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Position</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.role_id', array( 'label'=>false, 'options' => $rolelist )); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Department</label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.department_id', array( 'label'=>false, 'options' => $deptlist )); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name1">Date Hired <span>*</span></label>
                                    <div class="controls">
                                        <div class="input-append datetimepicker1">
                                          <input type="text" class="date_schedule" class="date_schedule" name="data[User][date_hired]" required="required" data-format="MM-dd-yyyy">
                                          <span class="add-on">
                                            <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">
                                            </i>
                                          </span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Buttons -->
                                <div class="form-actions">
                                    <!-- Buttons -->
                                    <?php echo $this->Form->button('Save', array('type' => 'submit','class' => 'btn btn-success')); ?>
                                    <button class="btn" type="reset">Reset</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
