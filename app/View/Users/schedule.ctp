    	<?php foreach ($record as $key2 => $value2) { ?>
		<div class="row-fluid">
		    <div class="span12">
		    	<a class="btn btn-info pull-right" href="<?php echo $this->Html->url(array('action' => 'report',$id,$value2['year'])) ?>" title=""><i class="fa fa-print"></i> Print Report</a>
		    	<div class="clearfix"></div>	
		        <div class="widget worange">

		            <div class="widget-head">
		                <div class="pull-left">Weekly Earnings Record - Year <?php echo $value2['year'] ?></div>
		                <div class="widget-icons pull-right">
		                </div>
		                <div class="clearfix"></div>
		            </div>

		            <div class="widget-content tblBorder">

		                <table class="table  table-bordered ">
		                    <thead>
		                        <tr>
		                            <th>Month</th>
		                            <th>Week Ending</th>
		                            <th>Basic Pay</th>
		                            <th>Gross Income</th>
		                            <th># of Days</th>
		                            <th>SSS</th>
		                            <th>Philhealth</th>
		                            <th>Pag-ibig</th>
		                        </tr>
		                    </thead>
		                    <tbody>


								<?php 
									$grandBasic = 0;
									$grandGross = 0;
									$grandSss = 0;
									$grandPhilhealth = 0;
									$grandPagibig = 0;
								?>
		                        <?php foreach ($value2['months'] as $key => $value) { 
		                        	$totalBasic = 0;
		                        	$totalGross = 0;
		                        	$totalDays = 0;
		                        	$totalSss = 0;
		                        	$totalPhilhealth = 0;
		                        	$totalPagibig = 0;
		                        ?>
		                        	<tr>
		                        		<td class="strong"><?php echo $value['month'] ?></td>
		                        		<td colspan="7"></td>
		                        	</tr>
									
		                        	<?php 
		                        		if(!isset($value['Attendance'])) {
		                        			continue;
		                        		}
		                        	foreach ($value['Attendance'] as $key1 => $value1) {
		                        		$grandBasic = $totalBasic += str_replace(',', '', $value1['Attendance']['totalBasicPay']);
		                        		$grandGross = $totalGross += str_replace(',', '', $value1['Attendance']['grossPay']);
		                        		$grandDays = $totalDays += str_replace(',', '', $value1['Attendance']['totalDays']);
		                        		if(isset($value1['Attendance']['grandSss'])) {
		                        			$grandSss = $totalSss += str_replace(',', '', $value1['Attendance']['grandSss']['Deduction']['sss']);
		                        			$grandPhilhealth = $totalPhilhealth += str_replace(',', '', $value1['Attendance']['grandSss']['Deduction']['philhealth']);
		                        			$grandPagibig = $totalPagibig += str_replace(',', '', $value1['Attendance']['grandSss']['Deduction']['pagibig']);
		                        		} else {
		                        			$grandSss = 0;
		                        			$grandPhilhealth = 0;
		                        			$grandPagibig = 0;
		                        		}
		                        		
		                        	?>
		                        		<tr>
		                        			<td></td>
		                        			<td><?php echo $value1['range'] ?></td>
		                        			<td><?php echo  $value1['Attendance']['totalBasicPay'] ?></td>
		                        			<td><?php echo  $value1['Attendance']['grossPay'] ?></td>
		                        			<td><?php echo  $value1['Attendance']['totalDays'] ?></td>
		                        			<?php if(isset($value1['Attendance']['grandSss'])): ?>
		                        			<td><?php echo   $this->Number->currency($value1['Attendance']['grandSss']['Deduction']['sss'],''); ?></td>
		                        			<td><?php echo   $this->Number->currency($value1['Attendance']['grandSss']['Deduction']['philhealth'],''); ?></td>
		                        			<td><?php echo   $this->Number->currency($value1['Attendance']['grandSss']['Deduction']['pagibig'],''); ?></td>
		                        			<?php else: ?>
		                        			<td>0</td>
		                        			<td>0</td>
		                        			<td>0</td>
		                        			<?php endif; ?>
		                        		</tr>
		                        	<?php } ?>
		                        	<tr>
		                        		<td colspan="2">Total For <?php echo $value['month'] ?></td>
		                        		
		                        		<td class="strong"><?php echo $this->Number->currency($totalBasic, '');  ?></td>
		                        		<td class="strong"><?php echo $this->Number->currency($totalGross, '');  ?></td>
		                        		<td><?php echo $totalDays ?></td>
		                        		<td class="strong"><?php echo $this->Number->currency($totalSss, '');  ?></td>
		                        		<td class="strong"><?php echo $this->Number->currency($totalPhilhealth, '');  ?></td>
		                        		<td class="strong"><?php echo $this->Number->currency($totalPagibig, '');  ?></td>
		                        	</tr>
		                        <?php } ?>
		                        
								<tr>
									<td colspan="2">Grand Total</td>
									<td class="strong"><?php echo $this->Number->currency($grandBasic, '');  ?></td>
									<td class="strong"><?php echo $this->Number->currency($grandGross, '');  ?></td>
									<td><?php echo $grandDays ?></td>
									<td class="strong"><?php echo $this->Number->currency($grandSss, '');  ?></td>
									<td class="strong"><?php echo $this->Number->currency($grandPhilhealth, '');  ?></td>
									<td class="strong"><?php echo $this->Number->currency($grandPagibig, '');  ?></td>
								</tr>
		                    </tbody>
		                </table>
		            </div>
		        </div>
		    </div>
		</div>
		<?php } ?>