<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Department
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
			<?php echo $this->Session->flash(); ?>
            <div class="span6">
                <div class="widget wviolet">

                    <div class="widget-head">
                        <div class="pull-left">Department Available</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th>Department</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($departments as $key=> $value) { ?>
                                <tr>
                                    <td>
                                        <?php echo $value[ 'Department'][ 'name']; ?>
                                    </td>
                                    <td>
                                        <?php if($value[ 'Department'][ 'status']==1 ) { echo '<span class="label label-success">Active</span>'; } else { echo '<span class="label label-important">Inactive</span>'; } ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-mini btn-warning" href="<?php echo $this->Html->url(array('action' => 'edit',$value['Department']['id'])); ?>"><i class="icon-pencil"></i> </a>
                                        <a class="btn btn-mini <?php echo $value['Department']['status'] == 0 ? 'btn-success' : 'btn-danger' ?>" href="<?php echo $this->Html->url(array('action' => 'status',$value['Department']['id'])); ?>"><i class="<?php echo $value['Department']['status'] == 1 ? 'icon-remove' : 'fa fa-check' ?>"></i> </a>
                                    </td>
                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>



                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
