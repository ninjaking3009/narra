<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Select Week to Generate Report
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">

        <?php echo $this->Session->flash(); ?>

            <div class="span6">

                <div class="widget wgreen">

                    <div class="widget-head">
                        <div class="pull-left">Select Week Range</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('register',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">Select Weeks</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Date.range',array('options' => $weeks,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <a href="<?php $this->Html->url(array('action' => 'select')) ?>" class="btn btn-success" id="procceedNext">Next</a>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

            <div class="span6 daysTotalWrap">
            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->

<script type="text/javascript">
    jQuery(document).ready(function($){

        var weeks = $('#DateRange').val();
        var linkmain = "<?php echo $this->Html->url(array('controller' => 'reports', 'action' => 'weekly')) ?>"+"/"+weeks;
        $('#procceedNext').attr("href",linkmain);

        $.ajax({
           url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
           data: {
            week: weeks,
           },
           success: function(data) {
                $('.daysTotalWrap').html('');
                $('.daysTotalWrap').html(data);
           },
           type: 'POST'
        });

        $('#DateRange').on('change', function() {
          var week1 = this.value;
          var link = "<?php echo $this->Html->url(array('controller' => 'reports', 'action' => 'weekly')) ?>"+"/"+week1;
          $('#procceedNext').attr('href',link)

          $.ajax({
             url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
             data: {
              week: week1,
             },
             success: function(data) {
                  $('.daysTotalWrap').html('');
                  $('.daysTotalWrap').html(data);
             },
             type: 'POST'
          });

        });
    });
</script>