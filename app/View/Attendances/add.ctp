<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Attendance
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="rangeSelectBox">
                <div class="perSb weekRange">
                    <span>Select Week</span>
                    <?php echo $this->Form->input('Date.range',array('options' => $weeks,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>
                <div class="perSb dayRange"></div>
                <div class="perSbBtn">
                    <a href="<?php echo $this->Html->url(['controller' => 'attendances', 'action' => 'import']) ?>" class="btn btn-success">Import Attendance</a>
                    <button class="btn btn-primary showWeekTotal">Week Total</button>
                </div>
                <!-- <div class="perSbBtn">
                    <button class="btn btn-primary showDayTotal">Day Total</button>
                </div> -->
                <div class="clearfix"></div>
                <div class="weeksTotalWrap"></div>
                <div class="daysTotalWrap"></div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4 empListBox"></div>

            <div class="span8">
                <div class="alert alert-success payrollAddNotif">Payroll Successfully Added.</div>
                <div class="attendanceContainer">
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Matter ends -->

<script type="text/javascript">
jQuery(document).ready(function($) {

    var weeks = $('#DateRange').val();
    var curWeek = $('#DateRange').val();
    var curDay = 0;



    $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
        data: {
            week: weeks,
        },
        success: function(data) {
            $('.weeksTotalWrap').html('');
            $('.weeksTotalWrap').html(data);
        },
        type: 'POST'
    });

    $('.dayRange').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');

    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "ajaxdate")); ?>' + '/' + weeks,
        success: function(data) {
            $('.dayRange').html('');
            $('.dayRange').html(data);


            var day = $('#DateRange1').val();
            curDay = day;
            $('.daysTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
            $.ajax({
                url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
                data: {
                    date: day,
                    week: weeks,
                },
                success: function(data) {
                    $('.daysTotalWrap').html('');
                    $('.daysTotalWrap').html(data);
                },
                type: 'POST'
            });

            $('.empListBox').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
            $.ajax({
                url: '<?php echo $this->html->url(array("action" => "ajaxadd")); ?>/'+day,
                success: function(data) {
                    $('.empListBox').html('');
                    $('.empListBox').html(data);
                },
            });


        },
    });

    
    $('#DateRange').on('change', function() {
        var week1 = this.value;
        curWeek = week1;

        $('.attendanceContainer').html('');

        $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
            data: {
                week: week1,
            },
            success: function(data) {
                $('.weeksTotalWrap').html('');
                $('.weeksTotalWrap').html(data);
            },
            type: 'POST'
        });

        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "ajaxdate")); ?>' + '/' + week1,
            success: function(data) {
                $('.dayRange').html('');
                $('.dayRange').html(data);

                var day = $('#DateRange1').val();
                curDay = day;
                
                $('.daysTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
                    data: {
                        date: day,
                        week: week1,
                    },
                    success: function(data) {
                        $('.daysTotalWrap').html('');
                        $('.daysTotalWrap').html(data);

                        $('.empListBox').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                        $.ajax({
                            url: '<?php echo $this->html->url(array("action" => "ajaxadd")); ?>/'+curDay,
                            success: function(data) {
                                $('.empListBox').html('');
                                $('.empListBox').html(data);
                            },
                        });

                    },
                    type: 'POST'
                });

            },
        });

    });

    $('.rangeSelectBox').on('change', '#DateRange1', function() {
        var week2 = this.value;
        curDay = week2;
        
        $('.daysTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
            data: {
                date: week2,
                week: curWeek,
            },
            success: function(data) {
                $('.daysTotalWrap').html('');
                $('.daysTotalWrap').html(data);
                $('.attendanceContainer').html('');

                $('.empListBox').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "ajaxadd")); ?>/'+curDay,
                    success: function(data) {
                        $('.empListBox').html('');
                        $('.empListBox').html(data);
                    },
                });

            },
            type: 'POST'
        });

    });

    $('.row-fluid').on('click', '.makePayrollBtn', function() {
        var userid = $(this).data('id');

        $('.attendanceContainer').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
            data: {
                id: userid,
                date: curDay,
                week: curWeek,
            },
            cache: false,
            success: function(data) {
                $('.attendanceContainer').html('');
                $('.attendanceContainer').html(data);
                $('.payrollAddNotif').slideUp('fast');
            },
            type: 'POST'
        });
    });


    $('.datetimepicker2').datetimepicker({
        pick12HourFormat: true,
        pickSeconds: false,
        pickDate: false
    });

    $('.datetimepicker1').datetimepicker({
        pickTime: false
    });

    $('.attendanceContainer').on('click', '.absentChk', function() {
        if ($(this).is(':checked')) {
            $('.pField').attr('disabled', 'disabled');
            $('.hrsWorked').val('');
        } else {
            $('.pField').removeAttr('disabled');
        }
    });

    $('.attendanceContainer').on('click', '.omitChk', function() {
        if ($(this).is(':checked')) {
            $('.omitBox').show();
        } else {
            $('.omitBox').hide();
        }
    });

    $('.attendanceContainer').on('click', '.relieverChk', function() {
        if ($(this).is(':checked')) {
            $('.relieverBox').show();
        } else {
            $('.relieverBox').hide();
        }
    });

    $('.attendanceContainer').on('click', '.otChk', function() {
        if ($(this).is(':checked')) {
            $('.otBox').show();
        } else {
            $('.otBox').hide();
        }
    });

    $('.attendanceContainer').on('click', '.relieverChk1', function() {
        if ($(this).is(':checked')) {
            $('.relieverBox1').show();
        } else {
            $('.relieverBox1').hide();
        }
    });

    $('.attendanceContainer').on('click', '.otChk1', function() {
        if ($(this).is(':checked')) {
            $('.otBox1').show();
        } else {
            $('.otBox1').hide();
        }
    });


    $('.attendanceContainer').on('click', '.deletePayroll', function(event) {
        var userid = $(this).data('user');
        var attId = $(this).data('id');

        console.log(userid);

        $('.attendanceContainer').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');

        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "delete")); ?>/'+attId,
            success: function(data) {

                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
                    data: {
                        id: userid,
                        date: curDay,
                        week: curWeek,
                    },
                    cache: false,
                    success: function(data) {
                        $('.attendanceContainer').html('');
                        $('.attendanceContainer').html(data);
                    },
                    type: 'POST'
                });

                $('#payrollList').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "ajaxupdatedpayrolled")); ?>/'+curDay,
                    success: function(data) {
                        $('#payrollList').html('');
                        $('#payrollList').html(data);
                    },
                });

            },
        });

    });

    $('.attendanceContainer').on('submit', '#addEmpForm', function(event) {

        var $this = $(this);

        $.post("<?php echo $this->Html->url(array('action' => 'proccess')) ?>", $("#addEmpForm").serialize(), function(data) {
            
            $('.payrollAddNotif').slideDown('fast');
            var userid = $this.children('#UserId').val();

                $('.attendanceContainer').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
                    data: {
                        id: userid,
                        date: curDay,
                        week: curWeek,
                    },
                    cache: false,
                    success: function(data) {

                        $('#payrollList').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                        
                        $.ajax({
                            url: '<?php echo $this->html->url(array("action" => "ajaxupdatedpayrolled")); ?>/'+curDay,
                            success: function(data) {
                                $('#payrollList').html('');
                                $('#payrollList').html(data);
                            },
                        });

                        $('.attendanceContainer').html('');
                        $('.attendanceContainer').html(data);
                    },
                    type: 'POST'
                });



                $('#q').val('').focus();
                
                $('#emp'+userid).removeClass('btn-success').addClass('btn-warning').html('<i class="fa fa-calendar"></i> Edit Payroll');

        });

        event.preventDefault();
        return false;
    });

    $('.attendanceContainer').on('submit', '#omitEmpForm', function(event) {

        var $this = $(this);

        $('.attendanceContainer').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');

        $.post("<?php echo $this->Html->url(array('action' => 'proccess')) ?>", $this.serialize(), function(data) {
            
            $('.payrollAddNotif').slideDown('fast');
            var userid = $this.children('#UserId').val();

            $.ajax({
                url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
                data: {
                    id: userid,
                    date: curDay,
                    week: curWeek,
                },
                cache: false,
                success: function(data) {

                    $('#payrollList').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                    $.ajax({
                        url: '<?php echo $this->html->url(array("action" => "ajaxupdatedpayrolled")); ?>/'+curDay,
                        success: function(data) {
                            $('#payrollList').html('');
                            $('#payrollList').html(data);
                        },
                    });

                    $('.attendanceContainer').html(data);
                },
                type: 'POST'
            });

            $('#emp'+userid).removeClass('btn-success').addClass('btn-warning').html('<i class="fa fa-calendar"></i> Edit Payroll');

        });

        event.preventDefault();
        return false;
    });

    $('.empListBox').on('click','#q',function(){
        $(this).select();
    });

    $('.empListBox').on('click','.ajaxBox ul li',function(){

        var curEmp = $(this);

        $('#q').val(curEmp.text());
        var empId = curEmp.data('id');

        $('.attendanceContainer').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');

        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
            data: {
                id: empId,
                date: curDay,
                week: curWeek,
            },
            cache: false,
            success: function(data) {
                $('.attendanceContainer').html('');
                $('.attendanceContainer').html(data);
                $('.payrollAddNotif').slideUp('fast');
                $('.ajaxBox').html('').hide();
                $('#AttendanceHoursWork').focus();
            },
            type: 'POST'
        });
    });

    $(document).mouseup(function (e)
    {
        var container = $(".ajaxBox");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });

    $('.empListBox').on('keyup','#q', function(event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);

        if (query == "" || query == " ") {
            $('.ajaxBox').slideUp('fast');
        }
        if (keycode == '13') {
            
            if(!$('.ajaxBox').is(':empty')) {
                var curEmp = $('.ajaxBox ul li.hov');

                if(curEmp.length) {
                    $('#q').val(curEmp.text());
                    var empId = curEmp.data('id');

                    $('.attendanceContainer').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');

                    $.ajax({
                        url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
                        data: {
                            id: empId,
                            date: curDay,
                            week: curWeek,
                        },
                        cache: false,
                        success: function(data) {
                            $('.ajaxBox').hide();
                            $('.attendanceContainer').html('');
                            $('.attendanceContainer').html(data);
                            $('.hrsWorkedreg').focus();
                            $('.payrollAddNotif').slideUp('fast');
                        },
                        type: 'POST'
                    });

                }

            }


            return false;
        }

        if (keycode != '38' && keycode != '40') {
                var query = $(this).val();

                    $('.ajaxBox').slideDown('fast');
                    $('.ajaxBox').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');

                    delay(function() {
                        $.ajax({
                            url: '<?php echo $this->html->url(array("controller" => "users", "action" => "ajax")); ?>',
                            data: {
                                q: query,
                            },
                            cache: false,
                            success: function(data) {
                                $('.ajaxBox').html('');
                                $('.ajaxBox').html(data);
                            },
                            type: 'POST'
                        });
                    }, 200);
        } else {

            if(!$('.ajaxBox').is(':empty')) {
                var curEmp = $('.ajaxBox ul li.hov');

                if(curEmp.length) {
                    
                    if(keycode == '38') {

                        if(!curEmp.is(':first-child')) {
                            curEmp.removeClass('hov').prev().addClass('hov');
                        }

                    } else if(keycode == '40') {

                        if(!curEmp.is(':last-child')) {
                            curEmp.removeClass('hov').next().addClass('hov');
                        }

                    }

                } else {
                    $('.ajaxBox ul li:first-child').addClass('hov');
                }

            }

        }
        
    });



    $('.perSbBtn button').click(function() {
        if ($(this).hasClass('btn-primary')) {
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-warning');

            if ($(this).hasClass('showWeekTotal')) {
                $(this).text('Hide Week Total');
                $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
                    data: {
                        week: curWeek,
                    },
                    success: function(data) {
                        $('.weeksTotalWrap').html('');
                        $('.weeksTotalWrap').html(data);
                        $('.weeksTotalWrap').slideDown('fast');
                        $('.daysTotalWrap').slideUp('fast');
                        $('.showDayTotal').text('Week Total').removeClass('btn-warning').addClass('btn-primary');
                    },
                    type: 'POST'
                });
            } else {
                $(this).text('Hide Day Total');
                
                $('.daysTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
                    data: {
                        date: curDay,
                        week: curWeek,
                    },
                    success: function(data) {
                        $('.daysTotalWrap').html('');
                        $('.daysTotalWrap').html(data);
                        $('.daysTotalWrap').slideDown('fast');
                        $('.weeksTotalWrap').slideUp('fast');
                        $('.showWeekTotal').text('Week Total').removeClass('btn-warning').addClass('btn-primary');
                    },
                    type: 'POST'
                });
            }

        } else {
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-primary');

            if ($(this).hasClass('showWeekTotal')) {
                $(this).text('Week Total');
                $('.weeksTotalWrap').slideUp('fast');
            } else {
                $(this).text('Day Total');
                $('.daysTotalWrap').slideUp('fast');
            }

        }
    });

    $('.attendanceContainer').on('click','#addEmpForm #AttendanceHoursWork', function(){
        
        
        if(timein != '' && timeout != '') {

            var timein = am_pm_to_hours($('.timein1').val());
            var timeout = am_pm_to_hours($('.timeout1').val());

            tempTimein = timein.split(':',1);
            tempTimeout = timeout.split(':',1);
            console.log(tempTimein)
            console.log(tempTimeout)
            var totTime = (tempTimeout - tempTimein);
            if(totTime < 0) {
                totTime = 0;
            }
            $(this).val(totTime);
        }

    });

});
</script>
