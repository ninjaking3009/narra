<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Roles Edit
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">

        <?php echo $this->Session->flash(); ?>

            <div class="span6">

                <div class="widget wred">

                    <div class="widget-head">
                        <div class="pull-left">Edit Book</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('edit',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">Book Name</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Book.name',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                    <?php echo $this->Form->hidden('Book.id',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="name1" class="control-label">Book Cost</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Book.cost',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="name1" class="control-label">School Year</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Book.year',array('value'=>$year['Year']['name'],'readonly'=>true,'type' => 'text','label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">Year Level</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Book.year_level',array('options' => $level, 'label'=>false,'class' => '', 'required' => 'required')); ?>
                                </div>
                            </div>
                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <?php echo $this->Form->button('Update Book', array('type' => 'submit','class' => 'btn btn-success')); ?>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
