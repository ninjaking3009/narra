<?php 
$counter = 0;
$counterPage = 0;
?>
<?php 
foreach ($attendances as $key => $value) {
$counter++;
$counterPage++;
if($counter==1) {
	echo '<div class="srow">';
}
?>
	<div class="perPayslip">
		<div class="Mainholder">
		<div class="payslipCounter">#<?php echo $key+1 ?></div>
			<div class="header">
				Narra Man Power Agency
				<span>Candelaria Quezon</span>
			</div>
			<div class="details">
				<div class="name">
					<span class="fl"><?php echo $value['User']['lname'].", ".$value['User']['fname'] ?></span>
					<span class="fr">Date: <?php echo date('M, d Y') ?></span>
					<div class="clearfix"></div>
				</div>
				<div class="dateToday">Payroll from <?php echo $days['start']." to ".$days['end'] ?></div>
			</div>

			<div class="dateBox">
				<table>
					<thead>
							<tr>
							<td></td>
						<?php foreach ($days['days'] as $key1 => $value1) { ?>
								<td><?php echo $value1 ?></td>
						<?php } ?>
							</tr>
					</thead>
					<tbody>
							<tr>
								<td>Basic</td>
							<?php foreach ($days['days'] as $k => $v) { ?>
							    
							        <?php
							            $hr = '';
							            $isNight = false;
							            foreach ($value['Days'] as $k1 => $v1) {
							                if($v1['Attendance']['date'] == $k) {
							                    if(!empty($v1['Attendance']['hours_work'])) {
							                        $hr = $v1['Attendance']['hours_work'];
							                    } elseif($v1['Attendance']['night_hours']) {
							                        $hr = $v1['Attendance']['night_hours'];
							                        $isNight = true;
							                    } else {
							                        $hr = $v1['Attendance']['legal_hours'];
							                    }
							                }
							            }
							            if($hr=='') {
							                $hr = 0;
							            }
							        ?>
							    <td class="<?php echo (!$isNight) ? "" : "nightShift" ?>" >
							    <?php echo $hr ?>
							    </td>
							<?php } ?>
							</tr>
							<tr>
							<td>OT</td>
							<?php 
							unset($k);
							unset($v);
							unset($k1);
							unset($v1);
							foreach ($days['days'] as $k => $v) { ?>
							        <?php
							            $hr = 0;
							            foreach ($value['Days'] as $k1 => $v1) {
							                if($v1['Attendance']['date'] == $k) {
							                    if($v1['Attendance']['ot_work'] != "") {
							                        $hr = $v1['Attendance']['ot_work'];
							                    }
							                }
							            }
							        ?>
							    <td>
							    <?php echo $hr ?>
							    </td>
							 <?php } ?>
							</tr>
					</tbody>
				</table>
			</div>
			<div class="detailsWrap">
				<div class="fl">
					<div class="detailsTitle">Total Hours</div>
					<ul>
						<li>
							<span>Basic Hours</span> <span><?php echo $value['Attendance']['totalWorkHours'] ?>hrs</span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Night Hours</span> <span><?php echo $value['Attendance']['totalNightHours'] ?>hrs</span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>OT Hours</span> <span><?php echo $value['Attendance']['totalOtHours'] ?>hrs</span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Legal Hol. Hrs.</span> <span><?php echo $value['Attendance']['totalLegalHours'] ?>hrs</span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Special Hol. hrs.</span> <span><?php echo $value['Attendance']['totalSpecialHours'] ?>hrs</span>
							<div class="clearfix"></div>
						</li>
						<!-- <li>
							<span>Cola</span> <span><?php echo $value['Attendance']['totalCola'] ?>hrs</span>
							<div class="clearfix"></div>
						</li> -->
					</ul>
				</div>
				<div class="fr">
					<div class="detailsTitle">Total Pay</div>
					<ul>
						<li>
							<span>Basic Pay</span> <span><?php echo $value['Attendance']['totalBasicPay'] ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Night Pay</span> <span><?php echo $value['Attendance']['totalNightPay'] ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>OT Pay</span> <span><?php echo $value['Attendance']['totalOtPay'] ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Legal Hol. Pay</span> <span><?php echo $value['Attendance']['totalLegalPay'] ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Special Hol. Pay</span> <span><?php echo $value['Attendance']['totalSpecialPay'] ?></span>
							<div class="clearfix"></div>
						</li>
						<!-- <li>
							<span>SEA</span> <span><?php echo $value['Attendance']['totalSea'] ?></span>
							<div class="clearfix"></div>
						</li> -->
						<li class="total">
							<span>Gross</span> <span><?php echo $value['Attendance']['grossPay'] ?></span>
							<div class="clearfix"></div>
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
				<div class="fl">
					<div class="detailsTitle">Deductions</div>
					<ul>
						<li>
							<span>SSS</span> <span><?php echo $value['Attendance']['grandSss']['Deduction']['sss'] ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Philhealth</span> <span><?php echo $value['Attendance']['grandSss']['Deduction']['philhealth'] ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>Pag-Ibig</span> <span><?php echo $value['Attendance']['grandSss']['Deduction']['pagibig'] ?></span>
							<div class="clearfix"></div>
						</li>
					</ul>
				</div>
				<div class="fr">
					<div class="detailsTitle">Loans/Running Bal.</div>
					<ul>
						<li>
							<span>SSS</span> <span><?php echo $this->Number->currency($value['Attendance']['sss_loan'],'')."(".$this->Number->currency($value['Loan']['sss_loan'],'').")" ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>HDMF MPL</span> <span><?php echo $this->Number->currency($value['Attendance']['pagibig_loan'],'')."(".$this->Number->currency($value['Loan']['pagibig_loan'],'').")" ?></span>
							<div class="clearfix"></div>
						</li>
						<li>
							<span>HDMF CAL</span> <span><?php echo $this->Number->currency($value['Attendance']['calamity_loan'],'')." (".$this->Number->currency($value['Loan']['calamity_loan'],'').")" ?></span>
							<div class="clearfix"></div>
						</li>
						<li class="total">
							<span>Net Pay</span> <span><?php echo $value['Attendance']['netPay'] ?></span>
							<div class="clearfix"></div>
						</li>

					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="advanceCont">
			<span>Advance:</span>
			<span><?php echo $this->Number->currency($value['Attendance']['advance'],'')." (".$this->Number->currency($value['Loan']['advance'],'').")" ?></span>
			<span>Net Pay</span>
			<span><?php echo $value['Attendance']['netPay'] ?></span>
		</div>

	</div>
<?php 
if($counter==3) {
	echo '<div class="clearfix"></div></div>';
	$counter = 0;
}
if($counterPage==9) {
	echo '<div class="pagebreak"></div>';
	$counter = 0;
}
} ?>
