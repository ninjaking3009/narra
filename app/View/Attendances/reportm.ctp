<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Attendance
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="rangeSelectBox">
                <div class="perSb weekRange">
                    <span>Select Month</span>
                    <?php echo $this->Form->input('Date.range',array('options' => $weeks,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>
                <div class="perSbBtn">
                    <button class="btn btn-primary showWeekTotal">Month Total</button>
                </div>
                <div class="clearfix"></div>
                <div class="weeksTotalWrap"></div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 totalWrap"></div>
        </div>
    </div>
</div>



<!-- Matter ends -->

<script type="text/javascript">
jQuery(document).ready(function($) {

    var weeks = $('#DateRange').val();
    var curWeek = $('#DateRange').val();

    $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "monthtotal")); ?>',
        data: {
            week: weeks,
        },
        success: function(data) {
            $('.weeksTotalWrap').html('');
            $('.weeksTotalWrap').html(data);
        },
        type: 'POST'
    });


    $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "monthreport")); ?>',
        data: {
            week: weeks,
        },
        success: function(data) {
            $('.totalWrap').html('');
            $('.totalWrap').html(data);
        },
        type: 'POST'
    });
    
    $('#DateRange').on('change', function() {
        var week1 = this.value;
        curWeek = week1;

        $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
            data: {
                week: week1,
            },
            success: function(data) {
                $('.weeksTotalWrap').html('');
                $('.weeksTotalWrap').html(data);
            },
            type: 'POST'
        });

        $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "monthreport")); ?>',
            data: {
                week: week1,
            },
            success: function(data) {
                $('.totalWrap').html('');
                $('.totalWrap').html(data);
            },
            type: 'POST'
        });

    });

    $('.perSbBtn button').click(function() {
        if ($(this).hasClass('btn-primary')) {
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-warning');

            if ($(this).hasClass('showWeekTotal')) {
                $(this).text('Hide Month Total');
                $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
                    data: {
                        week: curWeek,
                    },
                    success: function(data) {
                        $('.weeksTotalWrap').html('');
                        $('.weeksTotalWrap').html(data);
                        $('.weeksTotalWrap').slideDown('fast');
                        $('.daysTotalWrap').slideUp('fast');
                        $('.showDayTotal').text('Month Total').removeClass('btn-warning').addClass('btn-primary');
                    },
                    type: 'POST'
                });
            } else {
                $(this).text('Hide Day Total');
                
                $('.daysTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
                    data: {
                        date: curDay,
                        week: curWeek,
                    },
                    success: function(data) {
                        $('.daysTotalWrap').html('');
                        $('.daysTotalWrap').html(data);
                        $('.daysTotalWrap').slideDown('fast');
                        $('.weeksTotalWrap').slideUp('fast');
                        $('.showWeekTotal').text('Month Total').removeClass('btn-warning').addClass('btn-primary');
                    },
                    type: 'POST'
                });
            }

        } else {
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-primary');

            if ($(this).hasClass('showWeekTotal')) {
                $(this).text('Month Total');
                $('.weeksTotalWrap').slideUp('fast');
            } else {
                $(this).text('Day Total');
                $('.daysTotalWrap').slideUp('fast');
            }

        }
    });

});
</script>
