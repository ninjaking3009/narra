<div class="selectBox">
    <label for="">Search Employee</label>
    <?php echo $this->Form->input('q', array( 'div'=>false, 'label'=>false, 'placeholder' => 'search..' )); ?>
    <div class="ajaxBox"></div>
</div>

<div class="widget wgreen">

    <div class="widget-head">
        <div class="pull-left">Already Pay-rolled</div>
        <div class="clearfix"></div>
    </div>

    <div class="widget-content" id="payrollList">
        <table class="table  table-bordered ">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($payrolled as $key => $data) { ?>
                <tr>
                    <td>
                        <?php echo ucfirst($data['User']['lname']). ", ".ucfirst($data['User']['fname']). " ".ucfirst($data['User'][ 'mname']) ?>
                    </td>
                    <td>
                        <a class="btn btn-mini btn-warning makePayrollBtn" id="emp<?php echo $data['User']['id'] ?>" data-id="<?php echo $data['User']['id'] ?>" href="javascript:;"><i class="icon-calendar"></i> Edit Payroll</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!--
    <label for="posSelect">Select Position</label>
    <?php echo $this->Form->input('User.role_id', array( 'label'=>false, 'options' => $rolelist, 'id' => 'posSelect' )); ?>
</div>
<?php foreach ($positions as $key=> $value) { ?>
<div class="widget wgreen positionBoxWrap" id="position<?php echo $value['Role']['id'] ?>">

    <div class="widget-head">
        <div class="pull-left">Employee</div>
        <div class="clearfix"></div>
    </div>

    <div class="widget-content">
        <table class="table  table-bordered ">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($value[ 'Employee'] as $key=> $data) { ?>
                <tr>
                    <td>
                        <?php echo ucfirst($data[ 'User'][ 'lname']). ", ".ucfirst($data[ 'User'][ 'fname']). " ".ucfirst($data[ 'User'][ 'mname']) ?>
                    </td>
                    <td>
                        <?php if(empty($data['payroll'])) { ?>
                        <a class="btn btn-mini btn-success makePayrollBtn" id="emp<?php echo $data['User']['id'] ?>" data-id="<?php echo $data['User']['id'] ?>" href="javascript:;"><i class="icon-calendar"></i> Add Payroll</a>
                        <?php } else { ?>
                        <a class="btn btn-mini btn-warning makePayrollBtn" id="emp<?php echo $data['User']['id'] ?>" data-id="<?php echo $data['User']['id'] ?>" href="javascript:;"><i class="icon-calendar"></i> Edit Payroll</a>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php } ?>
-->
<script type="text/javascript">
jQuery(document).ready(function($) {
    var id = $('#posSelect').val();
    $('#position' + id).show();

    $('#posSelect').on('change', function() {
        var id = this.value;
        $('.positionBoxWrap').hide();
        $('#position' + id).show();
    });

});
</script>