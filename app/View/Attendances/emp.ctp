<div class="row-fluid omitWrapper" <?php if(!empty($omit['Attendance']['id'])) { echo 'style="display:block"'; } ?>>
    <div class="span12">
        <div class="widget worange">

            <div class="widget-head">
                <div class="pull-left">Omit Payroll for Employee <strong><?php echo ucfirst($emp['User']['lname']).", ".ucfirst($emp['User']['fname'])." ".ucfirst($emp['User']['mname']) ?></strong>
                </div>
                <div class="pull-right widget-icon">
                    <button class="btn hideOmitBox">Cancel</button>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">

                <div class="padd">
                    <?php echo $this->form->create('omit',array('class'=>'form-horizontal')) ?>
                    <?php
                        if($isHoliday) {
                            echo $this->Form->hidden('Attendance.is_holiday',array('value' => '1','div' => false,'label'=>false));
                        }
                    ?>

                    <input type="hidden" class="input-large" id="UserId" value="<?php echo $emp['User']['id'] ?>" name="data[Attendance][user_id]">
                    <input type="hidden" class="input-large" value="<?php echo $emp['Role']['per_hour'] ?>" name="data[Attendance][per_hour]">
                    <input type="hidden" class="input-large" value="<?php echo $emp['Role']['id'] ?>" name="data[Attendance][position]">
                    <input type="hidden" class="input-large" value="<?php echo $day ?>" name="data[Attendance][date]">
                    <input type="hidden" class="input-large" value="<?php echo date('m',$day) ?>" name="data[Attendance][month]">
                    <input type="hidden" class="input-large" value="<?php echo $week ?>" name="data[Attendance][week]">
                    <input type="hidden" class="input-large" value="1" name="data[Attendance][is_omit]">

                    <?php if(empty($restday)){ ?>
                    <?php if(!$isHoliday){ ?>

                    <?php } ?>
                    <?php } else { ?>
                        <input type="hidden" class="input-large" value="1" name="data[Attendance][is_restday]">
                    <?php } ?>

                    <div class="omitBox">
                        <div class="control-group">
                            <label class="control-label" for="name1">Omit Date</label>
                            <div class="controls">
                                <div class="input-append datetimepicker1">
                                  <input type="text" class="date_schedule1 " <?php if(!empty($omit['Attendance']['date'])) { echo 'value="'.$omit['Attendance']['date'].'"'; } ?> name="data[Attendance][omitDate]" data-format="MM-dd-yyyy">
                                  <span class="add-on">
                                    <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">
                                    </i>
                                  </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name1">Deduct</label>
                        <div class="controls">
                            <div class="input text">
                                <input type="checkbox" <?php if($omit['Attendance']['is_deduct'] == 1) echo 'checked' ?> class="pField lastpayrollChk" name="data[Attendance][is_deduct]">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name1">Last Payroll</label>
                        <div class="controls">
                            <div class="input text">
                                <input type="checkbox" <?php if($omit['Attendance']['is_lastpayroll'] == 1) echo 'checked' ?> class="pField lastpayrollChk" name="data[Attendance][is_lastpayroll]">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name1">Reliever</label>
                        <div class="controls">
                            <div class="input text">
                                <input type="checkbox" <?php if($omit['Attendance']['is_reliever'] == 1) echo 'checked' ?> class="pField relieverChk1" name="data[Attendance][is_reliever]">
                            </div>
                        </div>
                    </div>

                    <div class="relieverBox1" <?php if($omit['Attendance']['is_reliever']  == 1) echo 'style="display:block"' ?>>
                        <div class="control-group">
                            <label class="control-label" for="name1">Select Position</label>
                            <div class="controls">
                                <?php echo $this->Form->input('Attendance.reliever_position', array( 'label'=>false, 'options' => $rolelist, 'default' => $omit['Attendance']['reliever_position'] )); ?>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name1">Over Time</label>
                        <div class="controls">
                            <div class="input text">
                            <input type="checkbox" <?php if($omit['Attendance']['is_ot'] == 1) echo 'checked' ?> class="pField otChk1" name="data[Attendance][is_ot]">
                            </div>
                        </div>
                    </div>

                    <div class="otBox1" <?php if($omit['Attendance']['is_ot'] == 1) echo 'style="display:block"' ?>>
                        <div class="control-group">
                            <label class="control-label" for="name1">Over Time Hours</label>
                            <div class="controls">
                                <input type="text" class="input-large pField hrsWorked" <?php if(!empty($omit['Attendance']['id'])) echo 'value="'.$omit['Attendance']['ot_work'].'"' ?> name="data[Attendance][ot_work]">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="name1" class="control-label">Time In</label>
                        <div class="controls">
                            <div class="input-append datetimepicker2">
                                <input type="text" class="pField time_schedule" data-format="HH:mm PP" <?php if(!empty($omit['Attendance']['id'])) echo 'value="'.$omit['Attendance']['time_in'].'"' ?> name="data[Attendance][time_in]">
                                <span class="add-on">
                                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                              </i>
                                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="name1" class="control-label">Time Out</label>
                        <div class="controls">
                            <div class="input-append datetimepicker2">
                                <input type="text" class="pField time_schedule" data-format="HH:mm PP" <?php if(!empty($omit['Attendance']['id'])) echo 'value="'.$omit['Attendance']['time_out'].'"' ?> name="data[Attendance][time_out]">
                                <span class="add-on">
                                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                              </i>
                                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name1">Hours Worked</label>
                        <div class="controls">
                            <div class="input text">
                                <?php
                                    if($omit['Attendance']['is_deduct'] == 1) {
                                        $omit['Attendance']['omit_hours'] = $omit['Attendance']['deduct_hours'];
                                    }
                                    if($omit['Attendance']['is_holiday'] == 1) {
                                        $omit['Attendance']['omit_hours'] = $omit['Attendance']['legal_hours'];   
                                        if($omit['Attendance']['is_legal'] != 1) {
                                            $omit['Attendance']['omit_hours'] = $omit['Attendance']['special_hours'];   
                                        }
                                    }
                                ?>
                                <input type="text" class="input-large pField hrsWorked" <?php if(!empty($omit['Attendance']['id'])) echo 'value="'.$omit['Attendance']['omit_hours'].'"' ?> name="data[Attendance][hours_work]">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name1">Night Shift</label>
                        <div class="controls">
                            <div class="input text">
                                <input type="checkbox" <?php if($omit['Attendance']['is_night'] == 1) echo 'checked' ?> class="pField" name="data[Attendance][is_night]">
                            </div>
                        </div>
                    </div>

                    <!-- Buttons -->
                    <div class="form-actions">
                        <!-- Buttons -->
                        <?php echo $this->Form->button('Submit', array('type' => 'submit','class' => 'btn btn-success')); ?>

                        <?php if(!empty($omit['Attendance']['id'])) { ?>
                            <a href="javascript:;" data-user="<?php echo $emp['User']['id'] ?>" data-id="<?php echo $omit['Attendance']['id'] ?>" class="btn btn-danger deletePayroll">Delete</a>
                        <?php } ?>
                    </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="row-fluid regPayrollBox">
<div class="span12">

    <div class="widget wgreen">

        <div class="widget-head">
            <div class="pull-left">Payroll for Employee <strong><?php echo ucfirst($emp['User']['lname']).", ".ucfirst($emp['User']['fname'])." ".ucfirst($emp['User']['mname']) ?></strong> <?php echo $restday; ?>
            </div>
            <div class="widget-icon pull-right">
                <button class="btn showPayroll"><i class="fa fa-plus"></i> Add Omit</button>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="widget-content">

            <div class="padd">
                <?php echo $this->form->create('add',array('class'=>'form-horizontal')) ?>
                <?php
                    if($isHoliday) {
                        echo $this->Form->hidden('Attendance.is_holiday',array('value' => '1','div' => false,'label'=>false));
                    }
                ?>

                <input type="hidden" class="input-large" id="UserId" value="<?php echo $emp['User']['id'] ?>" name="data[Attendance][user_id]">
                <input type="hidden" class="input-large" value="<?php echo $emp['Role']['per_hour'] ?>" name="data[Attendance][per_hour]">
                <input type="hidden" class="input-large" value="<?php echo $emp['Role']['id'] ?>" name="data[Attendance][position]">
                <input type="hidden" class="input-large" value="<?php echo $day ?>" name="data[Attendance][date]">
                <input type="hidden" class="input-large" value="<?php echo date('m',$day) ?>" name="data[Attendance][month]">
                <input type="hidden" class="input-large" value="<?php echo $week ?>" name="data[Attendance][week]">

                <?php if(empty($restday)){ ?>
                <?php if(!$isHoliday){ ?>
                <div class="control-group">
                    <label class="control-label" for="name1">Absent</label>
                    <div class="controls">
                        <div class="input text">
                            <input type="checkbox" <?php if($this->request->data['Attendance']['is_absent'] == 1) echo 'checked' ?> class="absentChk" name="data[Attendance][is_absent]">
                        </div>
                    </div>
                </div>

                <?php } ?>
                <?php } else { ?>
                    <input type="hidden" class="input-large" value="1" name="data[Attendance][is_restday]">
                <?php } ?>

                <div class="control-group">
                    <label class="control-label" for="name1">Last Payroll</label>
                    <div class="controls">
                        <div class="input text">
                            <input type="checkbox" <?php if($this->request->data['Attendance']['is_lastpayroll'] == 1) echo 'checked' ?> class="pField lastpayrollChk" name="data[Attendance][is_lastpayroll]">
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Reliever</label>
                    <div class="controls">
                        <div class="input text">
                            <input type="checkbox" <?php if($this->request->data['Attendance']['is_reliever'] == 1) echo 'checked' ?> class="pField relieverChk" name="data[Attendance][is_reliever]">
                        </div>
                    </div>
                </div>

                <div class="relieverBox" <?php if(!empty($this->request->data['Attendance']['is_reliever'])) echo 'style="display:block"' ?>>
                    <div class="control-group">
                        <label class="control-label" for="name1">Select Position</label>
                        <div class="controls">
                            <?php echo $this->Form->input('Attendance.reliever_position', array( 'label'=>false, 'options' => $rolelist )); ?>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Over Time</label>
                    <div class="controls">
                        <div class="input text">
                        <input type="checkbox" <?php if($this->request->data['Attendance']['is_ot'] == 1) echo 'checked' ?> class="pField otChk" name="data[Attendance][is_ot]">
                        </div>
                    </div>
                </div>

                <?php
                    if($this->request->data['Attendance']['otrdlegal_work'] > 0) {
                        $this->request->data['Attendance']['ot_work'] = $this->request->data['Attendance']['otrdlegal_work'];
                    }
                ?> 

                <div class="otBox" <?php if(!empty($this->request->data['Attendance']['is_ot'])) echo 'style="display:block"' ?>>
                    <div class="control-group">
                        <label class="control-label" for="name1">Over Time Hours</label>
                        <div class="controls">
                            <?php echo $this->Form->input('Attendance.ot_work',array('label'=>false,'class' => 'pField input-large hrsWorked')); ?>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label for="name1" class="control-label">Time In</label>
                    <div class="controls">
                        <div class="input-append datetimepicker2">
                            <?php echo $this->Form->input('Attendance.time_in',array('label'=>false,'class' => 'time_schedule pField timein1', 'data-format' => 'HH:mm PP')); ?>
                            <span class="add-on">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label for="name1" class="control-label">Time Out</label>
                    <div class="controls">
                        <div class="input-append datetimepicker2">
                            <?php echo $this->Form->input('Attendance.time_out',array('label'=>false,'class' => 'time_schedule pField timeout1', 'data-format' => 'HH:mm PP')); ?>
                            <span class="add-on">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>
                        </div>
                    </div>
                </div>
                <?php
                    if($this->request->data['Attendance']['is_legal'] == '1') {
                        $this->request->data['Attendance']['hours_work'] = $this->request->data['Attendance']['legal_hours'];
                    }
                    if($this->request->data['Attendance']['is_lastpayroll'] == '1') {
                        $this->request->data['Attendance']['hours_work'] = $this->request->data['Attendance']['lastpayroll_hours'];
                    }
                    if($this->request->data['Attendance']['rdlegal_work'] > 0) {
                        $this->request->data['Attendance']['hours_work'] = $this->request->data['Attendance']['rdlegal_work'];
                    }
                ?> 
                <div class="control-group">
                    <label class="control-label" for="name1">Hours Worked</label>
                    <div class="controls">
                        <div class="input text">
                            <?php echo $this->Form->input('Attendance.hours_work',array('label'=>false,'class' => 'pField input-large hrsWorked hrsWorkedreg')); ?>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Night Shift</label>
                    <div class="controls">
                        <div class="input text">
                            <input type="checkbox" <?php if($this->request->data['Attendance']['is_night'] == 1) echo 'checked' ?> class="pField" name="data[Attendance][is_night]">
                        </div>
                    </div>
                </div>

                <!-- Buttons -->
                <div class="form-actions">
                    <!-- Buttons -->
                    <?php echo $this->Form->button('Submit', array('type' => 'submit','class' => 'btn btn-success')); ?>

                    <?php if(!empty($this->request->data['Attendance']['id'])) { ?>
                        <a href="javascript:;" data-user="<?php echo $emp['User']['id'] ?>" data-id="<?php echo $this->request->data['Attendance']['id'] ?>" class="btn btn-danger deletePayroll">Delete</a>
                    <?php } ?>
                </div>
                </form>
            </div>

        </div>

    </div>

</div>
</div>
<div class="row-fluid">
<div class="span12">
    <?php echo $this->element('weeklyusertotal'); ?>
</div>
</div>

<script type="text/javascript">
    $(function() {

        <?php if($this->request->data['Attendance']['is_absent'] == 1) { ?>
        $('.pField').attr('disabled','disabled');
        <?php } ?>

        $('.datetimepicker2').datetimepicker({
            pick12HourFormat: true,
            pickSeconds: false,
            pickDate: false
        });

        $('.datetimepicker1').datetimepicker({
            pickTime: false
        });

        $('.showPayroll').click(function(){
            $('.omitWrapper').slideDown('fast');
        });

        $('.hideOmitBox').click(function(){
            $('.omitWrapper').slideUp('fast');
        });

    });
</script>