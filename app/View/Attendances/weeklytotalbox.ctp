<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Weekly Summary Attendance Record
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="rangeSelectBox">
                <div class="perSb weekRange">
                    <span>Select Year</span>
                    <?php echo $this->Form->input('year',array('options' => $year,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>

                <div class="perSb weekRange">
                    <span>Select Month</span>
                    <?php echo $this->Form->input('month',array('options' => $month,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>

                <div class="perSb weekRange">
                    <span>Select Week</span>
                    <?php echo $this->Form->input('week',array('options' => array(),'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div id="totalWrap">
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {

    getRecord();


    function getRecord(change) {

        var year = $('#year').val();
        var month = $('#month').val();
        var week = $('#week').val();

        $('#totalWrap').html('<div class="loaderBox" style="display:block"><i class="fa fa-refresh fa-spin"></i></div>');

        if(!week || change) {
            $('#week').html('');
            $.ajax({
                url: '<?php echo $this->html->url(array("action" => "weeklytotalbox")); ?>',
                data: {
                    year : year, month : month
                },
                dataType: 'json',
                success: function(data) {

                    if(data.length == 0) {
                        $('#totalWrap').html('<div class="noData" style="display:block"><i class="fa fa-exclamation-circle"></i> No Record Found</div>');
                    }

                    var isFirst = true;
                    var firstWeek = '';
                    $.each(data, function(index, val) {
                        if(isFirst) {
                            $.ajax({
                                url: '<?php echo $this->html->url(array("action" => "perweeklytotal")); ?>',
                                data: {
                                    year : year, month : month, week : index
                                },
                                success: function(html) {
                                     $('#totalWrap').hide().html(html).slideDown('fast');
                                },
                                type: 'POST'
                            });
                            isFirst = false;
                        }
                        $('#week')
                        .append($("<option></option>")
                        .attr("value",index)
                        .text(val));
                    });
                },
                type: 'POST'
            });
        } else {
            $.ajax({
                url: '<?php echo $this->html->url(array("action" => "perweeklytotal")); ?>',
                data: {
                    year : year, month : month, week : week
                },
                success: function(data) {
                    $('#totalWrap').hide().html(data).slideDown('fast');
                },
                type: 'POST'
            });
        }
        
    }

    $('#year').change(function(){
        getRecord(true);
    });

    $('#month').change(function(){
        getRecord(true);
    });
    
    $('#week').change(function(){
        getRecord();
    });


});
</script>
