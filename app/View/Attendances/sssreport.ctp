<div class="reportMainTitle">
    SSS Monthly Report - <?php echo $month ?>
</div>

<div class="widget wgreen">

    <div class="widget-head">
        <div class="pull-left">SSS Report</div>
        <div class="clearfix"></div>
    </div>

    <div class="widget-content">

        <table class="table  table-bordered ">
            <thead>
                <tr>
                    <td>#</td>
                    <td>SSS</td>
                    <td>Names</td>
                    <td>Basic Pay</td>
                    <td>Gross</td>
                    <td>Net</td>
                    <td>SSS</td>
                </tr>
            </thead>
            <tbody>

                <?php 
                $counter = 0;
                $totalBasic = 0;
                $totalGross = 0;
                $totalNet = 0;
                $totalSss = 0;
                foreach ($sss as $key=> $value) { 
                    $counter++;
                    $totalBasic += str_replace(',','',$value['Attendance']['totalBasicPay']);
                    $totalGross += str_replace(',','',$value['Attendance']['grossPay']);
                    $totalNet += str_replace(',','',$value['Attendance']['netPay']);
                    $totalSss += str_replace(',','',$value['Attendance']['totalSss']);
                ?>
                <tr>
                    <td><?php echo $counter ?></td>
                    <td><?php echo $value['User']['sss'] ?></td>
                    <td><?php echo ucfirst($value['User']['lname']).", ".ucfirst($value['User']['fname'])." ".ucfirst($value['User']['mname']) ?></td>
                    <td><?php echo $value['Attendance']['totalBasicPay'] ?></td>
                    <td><?php echo $value['Attendance']['grossPay'] ?></td>
                    <td><?php echo $value['Attendance']['netPay'] ?></td>
                    <td><?php echo $value['Attendance']['totalSss'] ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3"></td>
                    <td><strong><?php echo $this->Number->currency($totalBasic, ''); ?></strong></td>
                    <td><strong><?php echo $this->Number->currency($totalGross, ''); ?></strong></td>
                    <td><strong><?php echo $this->Number->currency($totalNet, ''); ?></strong></td>
                    <td><strong><?php echo $this->Number->currency($totalSss, ''); ?></strong></td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
