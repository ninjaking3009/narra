<div class="btn btn-warning generateBtnSignature pull-right"><i class="fa fa-retweet"></i> Generate Signature</div> 
<div class="btn btn-success generateBtnBilling pull-right"><i class="fa fa-retweet"></i> Generate Report</div> 
<div class="btn btn-primary generateBtn pull-right"><i class="fa fa-retweet"></i> Generate Payslip</div> 
<div class="clearfix"></div>
<div class="widget wred">

    <div class="widget-head">
        <div class="pull-left">Attendance</div>
        <div class="clearfix"></div>
    </div>

    <div class="widget-content tblvs">
        <table class="table  table-bordered tblAttendance ">
            <thead>
                <tr>
                    <td class="tac vam" rowspan="3">Name</td>
                    <td class="tac" colspan="7"><?php echo $days['start']." to ".$days['end'] ?></td>
                    <td rowspan="2" class="vam">Total Basic Hrs.</td>
                    <td rowspan="2" class="vam">Total OT</td>
                    <td rowspan="2" class="vam">Total Basic Pay</td>
                    <td rowspan="2" class="vam">OT Pay</td>
                    <!-- <td rowspan="2" class="vam">COLA</td>
                    <td rowspan="2" class="vam">SEA</td> -->
                    <td rowspan="2" class="vam">Night Pay</td>
                    <td rowspan="2" class="vam">Special Hours</td>
                    <td rowspan="2" class="vam">Special Pay</td>
                    <td rowspan="2" class="vam">Legal Hours</td>
                    <td rowspan="2" class="vam">Legal Pay</td>
                    <td rowspan="2" class="vam">Omitted Hours</td>
                    <td rowspan="2" class="vam">Gross Pay</td>
                    <td rowspan="2" class="vam">SSS Prem.</td>
                    <td rowspan="2" class="vam">Philhealth</td>
                    <td rowspan="2" class="vam">HDMF</td>
                    <td rowspan="2" class="vam">SSS Loan</td>
                    <td rowspan="2" class="vam">P. Ibig MPL Loan</td>
                    <td rowspan="2" class="vam">P. Ibig Cal. Loan</td>
                    <td rowspan="2" class="vam">Net Pay</td>
                </tr>
                <tr class="borderLeftFirst">
                    <?php foreach ($days['days'] as $key=> $value) { ?>
                        <td class="TblDaysTD">
                            <?php echo $value ?>
                        </td>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                    unset($key); unset($value);
                foreach ($attpos as $k=> $v) { ?>
                <tr>
                    <td colspan="28"><strong><?php echo ucfirst($v['Role']['role']) ?></strong></td>
                </tr>
                <?php foreach ($v['Attendances'] as $key => $value): ?>
                    <tr>
                        <td><?php echo ucfirst($value['User']['lname']).", ".ucfirst($value['User']['fname']) ?></td>
                        <?php foreach ($days['days'] as $key1 => $value1) { ?>
                            <td class="hrsWorkTd">
                                <?php
                                    $hr = '';
                                    foreach ($value['Days'] as $key2 => $value2) {
                                        if(isset($value2['Attendance']['omit_realdate']) && $value2['Attendance']['omit_realdate'] != '' && $value2['Attendance']['omit_realdate'] == $key1) {
                                            $hr = $value2['Attendance']['hours_work'];
                                            $hr .= " <span class=\"omitDatetbl\">(".date('M-d-y',$value2['Attendance']['date']).")</span>";
                                        }
                                        if($value2['Attendance']['date'] == $key1) {
                                            if(!empty($value2['Attendance']['hours_work'])) {
                                                $hr = $value2['Attendance']['hours_work'];
                                            } elseif($value2['Attendance']['night_hours']) {
                                                $hr = $value2['Attendance']['night_hours'];
                                            } else {
                                                $hr = $value2['Attendance']['legal_hours'];
                                            }
                                        }
                                    }
                                    if($hr=='') {
                                        echo 0;
                                    } else {
                                        echo $hr;
                                    }
                                ?>
                            </td>
                        <?php } ?>
                        <td><?php echo $value['Attendance']['totalWorkHours'] ?></td>
                        <td><?php echo $value['Attendance']['totalOtHours'] ?></td>
                        <td><?php echo $value['Attendance']['totalBasicPay'] ?></td>
                        <td><?php echo $value['Attendance']['totalOtPay'] ?></td>
                        <!-- <td><?php echo $value['Attendance']['totalCola'] ?></td>
                        <td><?php echo $value['Attendance']['totalSea'] ?></td> -->
                        <td><?php echo $value['Attendance']['totalNightPay'] ?></td>
                        <td><?php echo $value['Attendance']['totalSpecialHours'] ?></td>
                        <td><?php echo $value['Attendance']['totalSpecialPay'] ?></td>
                        <td><?php echo $value['Attendance']['totalLegalHours'] + $value['Attendance']['totalLpHours'] ?></td>
                        <td><?php echo $this->Number->currency($value['Attendance']['totalLegalPay'] + $value['Attendance']['totalLpPay'],'') ?></td>
                        <td><?php echo $value['Attendance']['totalOmitHours'] ?></td>
                        <td style="color:#000;font-weight:bold"><?php echo $value['Attendance']['grossPay'] ?></td>
                        <td><?php echo $value['Attendance']['grandSss']['Deduction']['sss'] ?></td>
                        <td><?php echo $value['Attendance']['grandSss']['Deduction']['philhealth'] ?></td>
                        <td><?php echo $value['Attendance']['grandSss']['Deduction']['pagibig'] ?></td>
                        <td><?php echo $value['Attendance']['sss_loan'] ?></td>
                        <td><?php echo $value['Attendance']['pagibig_loan'] ?></td>
                        <td><?php echo $value['Attendance']['calamity_loan'] ?></td>
                        <td style="color:#000;font-weight:bold"><?php echo $value['Attendance']['netPay'] ?></td>
                    </tr>
                <?php endforeach ?>
                <?php } ?>

                <tr class="grandTR">
                    <td colspan="8"></td>
                    <td><?php echo $grand['Attendance']['totalWorkHours'] ?></td>
                    <td><?php echo $grand['Attendance']['totalOtHours'] ?></td>
                    <td><?php echo $grand['Attendance']['totalBasicPay'] ?></td>
                    <td><?php echo $grand['Attendance']['totalOtPay'] ?></td>
                    <!-- <td><?php echo $grand['Attendance']['totalCola'] ?></td>
                    <td><?php echo $grand['Attendance']['totalSea'] ?></td> -->
                    <td><?php echo $grand['Attendance']['totalNightPay'] ?></td>
                    <td><?php echo $grand['Attendance']['totalSpecialHours'] ?></td>
                    <td><?php echo $grand['Attendance']['totalSpecialPay'] ?></td>
                    <td><?php echo $grand['Attendance']['totalLegalHours'] + $grand['Attendance']['totalLpHours'] ?></td>
                    <td><?php echo $this->Number->currency(str_replace(',', '', $grand['Attendance']['totalLegalPay']) + str_replace(',','',$grand['Attendance']['totalLpPay']),'') ?></td>
                    <td><?php echo $grand['Attendance']['totalOmitHours'] ?></td>
                    <td style="color:#000;font-weight:bold"><?php echo $grand['Attendance']['grossPay'] ?></td>
                    <td><?php echo $grand['Attendance']['grandDedudction']['Deduction']['sss'] ?></td>
                    <td><?php echo $grand['Attendance']['grandDedudction']['Deduction']['philhealth'] ?></td>
                    <td><?php echo $grand['Attendance']['grandDedudction']['Deduction']['pagibig'] ?></td>
                    <td><?php echo $grand['Attendance']['sss_loan'] ?></td>
                    <td><?php echo $grand['Attendance']['pagibig_loan'] ?></td>
                    <td><?php echo $grand['Attendance']['calamity_loan'] ?></td>
                    <td style="color:#000;font-weight:bold"><?php echo $grand['Attendance']['netPay'] ?></td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
