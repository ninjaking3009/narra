<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left"><?php echo $title; ?> Attendance
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="rangeSelectBox">
                <div class="perSb weekRange">
                    <span>Select Week</span>
                    <?php echo $this->Form->input('Date.range',array('options' => $weeks,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>
                <div class="perSb deptRange">
                    <span>Department</span>
                    <?php echo $this->Form->input('Department.name',array('options' => $deptlist,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>
                <div class="perSbBtn">
                    <!-- <button class="btn btn-primary showWeekTotal">Week Total</button> -->
                </div>
                <div class="clearfix"></div>
                <div class="weeksTotalWrap"></div>
            </div>
        </div>
        <div class="totalWrap"></div>
    </div>
</div>



<!-- Matter ends -->

<script type="text/javascript">
jQuery(document).ready(function($) {

    var weeks = $('#DateRange').val();
    var depart = $('#DepartmentName').val();
    var curWeek = $('#DateRange').val();

    $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "weekstotal",$isExcess)); ?>',
        data: {
            week: weeks,
        },
        success: function(data) {
            $('.weeksTotalWrap').html('');
            $('.weeksTotalWrap').html(data);
        },
        type: 'POST'
    });

    $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "weekreport",$isExcess)); ?>',
        data: {
            week: weeks,
            dept: depart,
        },
        success: function(data) {
            $('.totalWrap').html('');
            $('.totalWrap').html(data);
        },
        type: 'POST'
    });
    
    $('.totalWrap').on('click','.generateBtn', function() {
        window.location = '<?php echo $this->Html->url(array("action" => "printreport")) ?>/'+$('#DateRange').val()+'/'+depart+'/<?php echo $isExcess ?>';
    });
    
    $('.totalWrap').on('click','.generateBtnBilling', function() {
        window.location = '<?php echo $this->Html->url(array("action" => "generate")) ?>/'+$('#DateRange').val()+'/'+depart+'/<?php echo $isExcess ?>';
        alert('<?php echo $this->Html->url(array("action" => "generate")) ?>/'+$('#DateRange').val()+'/'+depart+'/<?php echo $isExcess ?>')
    });

    $('.totalWrap').on('click','.generateBtnSignature', function() {
        window.location = '<?php echo $this->Html->url(array("action" => "gensig")) ?>/'+$('#DateRange').val()+'/'+depart+'/<?php echo $isExcess ?>';
    });

    $('#DateRange').on('change', function() {
        var week1 = this.value;
        curWeek = week1;

        $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "weekstotal")); ?>',
            data: {
                week: week1,
            },
            success: function(data) {
                $('.weeksTotalWrap').html('');
                $('.weeksTotalWrap').html(data);
            },
            type: 'POST'
        });

        $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "weekreport",$isExcess)); ?>',
            data: {
                week: week1,
                dept: depart,
            },
            success: function(data) {
                $('.totalWrap').html('');
                $('.totalWrap').html(data);
            },
            type: 'POST'
        });

    });

    $('#DepartmentName').on('change', function() {
        var depart1 = this.value;
        depart = depart1;

        $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "weekstotal",$isExcess)); ?>',
            data: {
                week: curWeek,
            },
            success: function(data) {
                $('.weeksTotalWrap').html('');
                $('.weeksTotalWrap').html(data);
            },
            type: 'POST'
        });

        $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "weekreport",$isExcess)); ?>',
            data: {
                week: curWeek,
                dept: depart1,
            },
            success: function(data) {
                $('.totalWrap').html('');
                $('.totalWrap').html(data);
            },
            type: 'POST'
        });

    });

    $('.perSbBtn button').click(function() {
        if ($(this).hasClass('btn-primary')) {
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-warning');

            if ($(this).hasClass('showWeekTotal')) {
                $(this).text('Hide Week Total');
                $('.weeksTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "weekstotal",$isExcess)); ?>',
                    data: {
                        week: curWeek,
                    },
                    success: function(data) {
                        $('.weeksTotalWrap').html('');
                        $('.weeksTotalWrap').html(data);
                        $('.weeksTotalWrap').slideDown('fast');
                        $('.daysTotalWrap').slideUp('fast');
                        $('.showDayTotal').text('Week Total').removeClass('btn-warning').addClass('btn-primary');
                    },
                    type: 'POST'
                });
            } else {
                $(this).text('Hide Day Total');
                
                $('.daysTotalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
                
                $.ajax({
                    url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
                    data: {
                        date: curDay,
                        week: curWeek,
                    },
                    success: function(data) {
                        $('.daysTotalWrap').html('');
                        $('.daysTotalWrap').html(data);
                        $('.daysTotalWrap').slideDown('fast');
                        $('.weeksTotalWrap').slideUp('fast');
                        $('.showWeekTotal').text('Week Total').removeClass('btn-warning').addClass('btn-primary');
                    },
                    type: 'POST'
                });
            }

        } else {
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-primary');

            if ($(this).hasClass('showWeekTotal')) {
                $(this).text('Week Total');
                $('.weeksTotalWrap').slideUp('fast');
            } else {
                $(this).text('Day Total');
                $('.daysTotalWrap').slideUp('fast');
            }

        }
    });

});
</script>
