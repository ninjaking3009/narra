<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left"><a class="backBtn" href="<?php echo $this->Html->url(array('action' => 'select',$week)) ?>"><i class="fa fa-arrow-left"></i></a>  Attendance for <?php echo $day1 ?>
  </h2>
    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">

        <?php echo $this->Session->flash(); ?>

            <div class="span4">
                <div class="selectBox">
                    <label for="posSelect">Select Position</label>
                    <?php echo $this->Form->input('User.role_id', array( 'label'=>false, 'options' => $rolelist, 'id' => 'posSelect' )); ?>
                </div>
                <?php foreach ($positions as $key=> $value) { ?>
                <div class="widget wgreen positionBoxWrap" id="position<?php echo $value['Role']['id'] ?>">

                    <div class="widget-head">
                        <div class="pull-left"><?php echo $value['Role']['role'] ?></div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">
                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($value['Employee'] as $key=> $data) { ?>
                                <tr>
                                    <td>
                                        <?php echo ucfirst($data['User'][ 'lname']).", ".ucfirst($data['User']['fname'])." ".ucfirst($data['User']['mname']) ?>
                                    </td>
                                    <td>
                                        <?php if(empty($data['payroll'])) { ?>
                                            <a class="btn btn-mini btn-success makePayrollBtn" data-id="<?php echo $data['User']['id'] ?>" href="javascript:;"><i class="icon-calendar"></i> Add Payroll</a>
                                        <?php } else { ?>
                                            <a class="btn btn-mini btn-warning makePayrollBtn" data-id="<?php echo $data['User']['id'] ?>" href="javascript:;"><i class="icon-calendar"></i> Edit Payroll</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php } ?>

            </div>

            <div class="span8">
            <div class="attendanceContainer">
                <?php //echo $this->element('attendance') ?>
            </div>
            </div>
        </div>
    </div>
</div>

<!-- Matter ends -->

<script type="text/javascript">
    jQuery(document).ready(function($){
        var id = $('#posSelect').val();
        $('#position'+id).show();

        $('#posSelect').on('change', function() {
          var id = this.value;
          $('.positionBoxWrap').hide();
          $('#position'+id).show();
        });

        $('.row-fluid').on('click','.makePayrollBtn',function(){
            var userid = $(this).data('id');
            $.ajax({
               url: '<?php echo $this->html->url(array("action" => "emp")); ?>',
               data: {
                id:userid,
                date: '<?php echo $day; ?>',
                week: '<?php echo $week; ?>',
               },
               cache: false,
               success: function(data) {
                    $('.attendanceContainer').html('');
                    $('.attendanceContainer').html(data);
               },
               type: 'POST'
            });
        });

    });
</script>

<script type="text/javascript">
    $(function() {
        $('.datetimepicker2').datetimepicker({
            pick12HourFormat: true,
            pickSeconds: false,
            pickDate: false
        });

        $('.datetimepicker1').datetimepicker({
            pickTime: false
        });

        $('.attendanceContainer').on('click','.absentChk',function(){
            if($(this).is(':checked')) {
                $('.pField').attr('disabled','disabled');
                $('.hrsWorked').val('');
            } else {
                $('.pField').removeAttr('disabled');
            }
        });
        
        $('.attendanceContainer').on('click','.omitChk',function(){
            if($(this).is(':checked')) {
                $('.omitBox').show();
            } else {
                $('.omitBox').hide();
            }
        });
        
        $('.attendanceContainer').on('click','.relieverChk',function(){
            if($(this).is(':checked')) {
                $('.relieverBox').show();
            } else {
                $('.relieverBox').hide();
            }
        });

        $('.attendanceContainer').on('click','.otChk',function(){
            if($(this).is(':checked')) {
                $('.otBox').show();
            } else {
                $('.otBox').hide();
            }
        });

        $('.hrsWorked').on('input propertychange paste', function() {

        });

        $('.attendanceContainer').on('submit','#addEmpForm',function(event){

            $.post("<?php echo $this->Html->url(array('action' => 'proccess')) ?>",$("#addEmpForm").serialize(), function(data){
                window.location.href = $(location).attr('href');
            });

            event.preventDefault();
            return false;
        });

    });
</script>