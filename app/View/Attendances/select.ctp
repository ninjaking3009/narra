<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left"><a class="backBtn" href="<?php echo $this->Html->url(array('action' => 'range')) ?>"><i class="fa fa-arrow-left"></i></a>  Attendance
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">


            <div class="span6">

                <div class="widget wgreen">

                    <div class="widget-head">
                        <div class="pull-left">Date</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('register',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">Select Date</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Date.range',array('options' => $days,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <a href="<?php $this->Html->url(array('action' => 'select')) ?>" class="btn btn-success" id="procceedNext">Next</a>
                                <a href="<?php echo $this->Html->url(array('action' => 'ommit',$week)) ?>" class="btn btn-warning" id="procceedNext">Ommit</a>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

                <div class="widget worange">

                    <div class="widget-head">
                        <div class="pull-left">Omit list</div>
                        <div class="widget-icons pull-right">
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Position</th>
                                    <th>Hours Worked</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>



                                <?php foreach ($omit as $key => $value) { ?>
                                <tr>
                                    <td>
                                        <?php echo  $value['User']['fname']." ".$value['User']['lname']; ?>
                                    </td>
                                    <td>
                                        <?php echo  date('D m d, Y',strtotime($value['Attendance'][ 'date'])); ?>
                                    </td> 
                                    <td>
                                        <?php echo  $value['r']['Role'][ 'role']; ?>
                                    </td>
                                    <td>
                                        <?php echo  $value['Attendance'][ 'hours_work']; ?>
                                    </td>
                                    <td>
                                        <?php echo  $value['Attendance'][ 'total']; ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-mini btn-danger" data-toggle="modal" href="#confirmModal<?php echo $value['Attendance']['id'] ?>"><i class="icon-remove"></i></a>
                                    
                                        <div id="confirmModal<?php echo $value['Attendance']['id'] ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Confirm Delete</h3>
                                          </div>
                                          <div class="modal-body">
                                            <p>You are about to completely remove this. Are you sure you want to continue?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <a class="btn btn-danger" href="<?php echo $this->Html->url(array('action' => 'deleteomit',$week,$value['Attendance']['id'])); ?>">Yes, Delete Permanently</a>
                                          </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>



                    </div>
                </div>

            </div>
            
            <div class="span6 daysTotalWrap">
            </div>

        </div>

    </div>
</div>

<!-- Matter ends -->

<script type="text/javascript">
    jQuery(document).ready(function($){

        var day = $('#DateRange').val();
        var linkmain = "<?php echo $this->Html->url(array('action' => 'add')) ?>"+"/"+day+"/<?php echo $week ?>";
        $('#procceedNext').attr("href",linkmain);

        $.ajax({
           url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
           data: {
            date: day,
            week: '<?php echo $week; ?>',
           },
           success: function(data) {
                $('.daysTotalWrap').html('');
                $('.daysTotalWrap').html(data);
           },
           type: 'POST'
        });

        $('#DateRange').on('change', function() {
          var week = this.value;
          var link = "<?php echo $this->Html->url(array('action' => 'add')) ?>"+"/"+week+"/<?php echo $week ?>";
          $('#procceedNext').attr('href',link);

          $.ajax({
             url: '<?php echo $this->html->url(array("action" => "daystotal")); ?>',
             data: {
              date: week,
              week: '<?php echo $week; ?>',
             },
             success: function(data) {
                  $('.daysTotalWrap').html('');
                  $('.daysTotalWrap').html(data);
             },
             type: 'POST'
          });
          
        });
    });
</script>