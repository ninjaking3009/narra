<div class="row-fluid">
<div class="span12">

    <div class="widget wgreen">

        <div class="widget-head">
            <div class="pull-left">Payroll for Employee <strong><?php echo ucfirst($emp['User']['lname']).", ".ucfirst($emp['User']['fname'])." ".ucfirst($emp['User']['mname']) ?></strong>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="widget-content">

            <div class="padd">
                <?php echo $this->form->create('add',array('url' => '/attendances/ommitsubmit', 'class'=>'form-horizontal')) ?>

                <?php if(!empty($this->request->data['Attendance']['id'])) {
                    echo $this->Form->hidden('Attendance.id',array('div' => false,'label'=>false));
                    echo $this->Form->hidden('Attendance.user_id',array('div' => false,'label'=>false));
                    echo $this->Form->hidden('Attendance.per_hour',array('div' => false,'label'=>false));
                    echo $this->Form->hidden('Attendance.position',array('div' => false,'label'=>false));
                    echo $this->Form->hidden('Attendance.month',array('div' => false,'label'=>false));
                    echo $this->Form->hidden('Attendance.week',array('div' => false,'label'=>false));
                } else { ?>

                <input type="hidden" class="input-large" value="<?php echo $emp['User']['id'] ?>" name="data[Attendance][user_id]">
                <input type="hidden" class="input-large" value="<?php echo $emp['Role']['per_hour'] ?>" name="data[Attendance][per_hour]">
                <input type="hidden" class="input-large" value="<?php echo $emp['Role']['id'] ?>" name="data[Attendance][position]">
                <input type="hidden" class="input-large" value="<?php echo $week ?>" name="data[Attendance][week]">

                <?php } ?>

                <div class="omitBox">
                    <div class="control-group">
                        <label class="control-label" for="name1">Omit Date</label>
                        <div class="controls">
                            <div class="input-append datetimepicker1">
                              <input type="text" class="date_schedule1 " name="data[Attendance][omitDate]" data-format="MM-dd-yyyy">
                              <span class="add-on">
                                <i data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar">
                                </i>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Reliever</label>
                    <div class="controls">
                        <div class="input text">
                            <input type="checkbox" class="pField relieverChk" name="data[Attendance][is_reliever]">
                        </div>
                    </div>
                </div>

                <div class="relieverBox" >
                    <div class="control-group">
                        <label class="control-label" for="name1">Select Position</label>
                        <div class="controls">
                            <?php echo $this->Form->input('Attendance.reliever_position', array( 'label'=>false, 'options' => $rolelist )); ?>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Over Time</label>
                    <div class="controls">
                        <div class="input text">
                        <input type="checkbox" class="pField otChk" name="data[Attendance][is_ot]">
                        </div>
                    </div>
                </div>

                <div class="otBox" >
                    <div class="control-group">
                        <label class="control-label" for="name1">Over Time Hours</label>
                        <div class="controls">
                            <?php echo $this->Form->input('Attendance.ot_work',array('label'=>false,'class' => 'pField input-large hrsWorked')); ?>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label for="name1" class="control-label">Time In</label>
                    <div class="controls">
                        <div class="input-append datetimepicker2">
                            <?php echo $this->Form->input('Attendance.time_in',array('label'=>false,'class' => 'time_schedule pField', 'data-format' => 'HH:mm PP')); ?>
                            <span class="add-on">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label for="name1" class="control-label">Time Out</label>
                    <div class="controls">
                        <div class="input-append datetimepicker2">
                            <?php echo $this->Form->input('Attendance.time_out',array('label'=>false,'class' => 'time_schedule pField', 'data-format' => 'HH:mm PP')); ?>
                            <span class="add-on">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Hours Worked</label>
                    <div class="controls">
                        <div class="input text">
                            <?php echo $this->Form->input('Attendance.hours_work',array('label'=>false,'class' => 'pField input-large hrsWorked')); ?>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name1">Night Shift</label>
                    <div class="controls">
                        <div class="input text">
                            <input type="checkbox" class="pField" name="data[Attendance][is_night]">
                        </div>
                    </div>
                </div>

                <!-- Buttons -->
                <div class="form-actions">
                    <!-- Buttons -->
                    <?php echo $this->Form->button('Submit', array('type' => 'submit','class' => 'btn btn-success')); ?>

                    <?php if(!empty($this->request->data['Attendance']['id'])) { ?>
                        <a href="<?php echo $this->Html->url(array('action' => 'delete',$day,$week,$this->request->data['Attendance']['id'])) ?>" class="btn btn-danger">Delete</a>
                    <?php } ?>
                </div>
                </form>
            </div>

        </div>

    </div>

</div>
</div>
<div class="row-fluid">
<div class="span12">
    <div class="widget wblue">
        <!-- Widget title -->
        <div class="widget-head">
            <div class="pull-left">Total Summary from <?php echo $range['start']." to ".$range['end'] ?></div>
            <div class="clearfix"></div>
        </div>

        <div class="widget-content">
            <!-- Widget content -->
            <div class="padd">
                <!-- Contact box -->
                <div class="support-contact totalBox">
                    <div class="tb-lbl">Total Basic Hours <strong>:</strong></div> <span class="totalHoursWorked"><?php echo $totals['Attendance']['totalWorkHours'] ?> hrs</span>
                    <hr> 
                    <div class="tb-lbl">Total Basic Pay<strong>:</strong></div> <span class="totalBasicPay"><?php echo $totals['Attendance']['totalBasicPay'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Total Night Pay<strong>:</strong></div> <span class="totalNightPay"><?php echo $totals['Attendance']['totalNightPay'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Total OT Hours<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtHours'] ?> hrs</span>
                    <hr> 
                    <div class="tb-lbl">Total OT Pay<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtPay'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Total Omitted Pay<strong>:</strong></div> <span class="totalOmitPay">0</span>
                    <hr> 
                    <div class="tb-lbl">COLA<strong>:</strong></div> <span class="totalCola"><?php echo $totals['Attendance']['totalCola'] ?></span>
                    <hr> 
                    <div class="tb-lbl">SEA<strong>:</strong></div> <span class="totalSea"><?php echo $totals['Attendance']['totalSea'] ?></span>
                    <hr> 
                    <strong><div class="tb-lbl">Gross Pay:</div> <span class="totalGross bold"><?php echo $this->Number->currency($totals['Attendance']['grossPay'], ""); ?></span></strong>
                    <hr>
                    <br>

                    <strong>Deductions</strong>
                    <hr> 
                    <div class="tb-lbl">SSS Prem.<strong>:</strong></div> <span class="totalSss"><?php echo $totals['Attendance']['totalSss'] ?></span>
                    <hr> 
                    <div class="tb-lbl">SSS Loan<strong>:</strong></div> <span class="totalSssLoan">0</span>
                    <hr> 
                    <div class="tb-lbl">PhilHealth<strong>:</strong></div> <span class="totalPhilHealth"><?php echo $totals['Attendance']['totalPhilhealth'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Pag-Ibig<strong>:</strong></div> <span class="totalPagIbig"><?php echo $totals['Attendance']['totalPagibig'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Pag-Ibig Loan<strong>:</strong></div> <span class="totalPagIbigLoan">0</span>
                    <hr> 
                    <strong><div class="tb-lbl">NET Pay:</div> <span class="totalNetPay bold"><?php echo $this->Number->currency($totals['Attendance']['netPay'], ""); ?></span></strong> 
                    <hr>


                </div>
            </div>
        </div>

    </div>
</div>
</div>

<script type="text/javascript">
    $(function() {
        $('.datetimepicker2').datetimepicker({
            pick12HourFormat: true,
            pickSeconds: false,
            pickDate: false
        });

        $('.datetimepicker1').datetimepicker({
            pickTime: false
        });
    });
</script>