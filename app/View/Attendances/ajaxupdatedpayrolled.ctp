<table class="table  table-bordered ">
    <thead>
        <tr>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($payrolled as $key => $data) { ?>
        <tr>
            <td>
                <?php echo ucfirst($data['User']['lname']). ", ".ucfirst($data['User']['fname']). " ".ucfirst($data['User'][ 'mname']) ?>
            </td>
            <td>
                <a class="btn btn-mini btn-warning makePayrollBtn" id="emp<?php echo $data['User']['id'] ?>" data-id="<?php echo $data['User']['id'] ?>" href="javascript:;"><i class="icon-calendar"></i> Edit Payroll</a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>