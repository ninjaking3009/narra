<?php
foreach ($attendance as $key => $value) {
	if ($key % 2 == 0) {
        echo "<div class=\"row-fluid\">";
    }
    echo '<div class="span6">';
    echo $this->element('weeklyusertotal',array('attendance' => $value));
    echo '</div>';
    //if this is third value in row, end row
    if ($key % 2 == 1) {
        echo "</div>";
    }

}
?>