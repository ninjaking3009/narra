
    <div class="widget wblue">
        <!-- Widget title -->
        <div class="widget-head">
            <div class="pull-left">Total Summary For <?php echo $day ?></div>
            <div class="clearfix"></div>
        </div>

        <div class="widget-content">
            <!-- Widget content -->
            <div class="padd">
                <!-- Contact box -->
                <div class="support-contact totalBox">
                    <div class="tb-lbl">Total Workers <strong>:</strong></div> <span class="totalHoursWorked"><?php echo $totals['Attendance']['totalEmployee'] ?></span>
                    <hr>
                    <div class="tb-lbl">Total Basic Hours <strong>:</strong></div> <span class="totalHoursWorked"><?php echo $totals['Attendance']['totalWorkHours'] ?> hrs</span>
                    <hr> 
                    <div class="tb-lbl">Total Basic Pay<strong>:</strong></div> <span class="totalBasicPay"><?php echo $totals['Attendance']['totalBasicPay'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Total Night Hours<strong>:</strong></div> <span class="totalNightHr"><?php echo $totals['Attendance']['totalNightHours'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Total Night Pay<strong>:</strong></div> <span class="totalNightPay"><?php echo $totals['Attendance']['totalNightPay'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Total Omitted Hours<strong>:</strong></div> <span class="totalOmitHr"><?php echo $totals['Attendance']['totalOmitHours'] ?></span>
                    <hr>
                    <div class="tb-lbl">Total Omitted Pay<strong>:</strong></div> <span class="totalOmitPay"><?php echo $totals['Attendance']['totalOmitPay'] ?></span>
                    <hr>
                    <div class="tb-lbl">Total OT Hours<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtHours'] ?> hrs</span>
                    <hr> 
                    <div class="tb-lbl">Total OT Pay<strong>:</strong></div> <span class="totalOt"><?php echo $totals['Attendance']['totalOtPay'] ?></span>
                    <hr>
                    <div class="tb-lbl">Total Legal Hours<strong>:</strong></div> <span class="totalLegalHr"><?php echo $totals['Attendance']['totalLegalHours'] ?></span>
                    <hr>
                    <div class="tb-lbl">Total Legal Pay<strong>:</strong></div> <span class="totalLegalPay"><?php echo $totals['Attendance']['totalLegalPay'] ?></span>
                    <hr>
                    <div class="tb-lbl">COLA<strong>:</strong></div> <span class="totalCola"><?php echo $totals['Attendance']['totalCola'] ?></span>
                    <hr> 
                    <div class="tb-lbl">SEA<strong>:</strong></div> <span class="totalSea"><?php echo $totals['Attendance']['totalSea'] ?></span>
                    <hr> 
                    <strong><div class="tb-lbl">Gross Pay:</div> <span class="totalGross bold"><?php echo $totals['Attendance']['grossPay']; ?></span></strong>
                    <hr>
                    <br>

                    <strong>Deductions</strong>
                    <hr> 
                    <div class="tb-lbl">SSS Prem.<strong>:</strong></div> <span class="totalSss"><?php echo $totals['Attendance']['totalSss'] ?></span>
                    <hr>
                    <div class="tb-lbl">PhilHealth<strong>:</strong></div> <span class="totalPhilHealth"><?php echo $totals['Attendance']['totalPhilhealth'] ?></span>
                    <hr> 
                    <div class="tb-lbl">Pag-Ibig<strong>:</strong></div> <span class="totalPagIbig"><?php echo $totals['Attendance']['totalPagibig'] ?></span>
                    <hr>
                    <strong><div class="tb-lbl">NET Pay:</div> <span class="totalNetPay bold"><?php echo $totals['Attendance']['netPay']; ?></span></strong> 
                    <hr>


                </div>
            </div>
        </div>

    </div>