<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left"><a class="backBtn" href="<?php echo $this->Html->url(array('action' => 'select',$week)) ?>"><i class="fa fa-arrow-left"></i></a> Ommit Attendance to week <?php echo $week; ?>
  </h2>
    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

        <?php echo $this->Session->flash(); ?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">


            <div class="span4">
                <div class="selectBox">
                    <label for="posSelect">Search Employee</label>
                    <?php echo $this->Form->input('q', array( 'div'=>false, 'label'=>false, 'id' => 'posSelect' )); ?>
                    <div class="ajaxBox"></div>
                    <!-- <div class="sb-btn btn btn-success">Submit</div> -->
                </div>
            </div>

            <div class="span8">
            <div class="attendanceContainer">
                <?php //echo $this->element('attendance') ?>
            </div>
            </div>
        </div>
    </div>
</div>

<!-- Matter ends -->

<script type="text/javascript">
    jQuery(document).ready(function($){

        $('#posSelect').focus();

        $('#posSelect').on('keypress', function() {

            if(query == "" || query == " ") {
                $('.ajaxBox').slideUp('fast');
            }

          var query = $(this).val();
          if(query.length >= 3) {
              delay(function(){
                    $('.ajaxBox').slideUp('fast');
                    $('.ajaxBox').html('');
                    $.ajax({
                       url: '<?php echo $this->html->url(array("controller" => "users", "action" => "ajax")); ?>',
                       data: {
                        q:query,
                       },
                       cache: false,
                       success: function(data) {
                            $('.ajaxBox').slideDown('fast');
                            $('.ajaxBox').html(data);
                       },
                       type: 'POST'
                    });
                    
                  }, 700 );
            }
        });

        $('.row-fluid').on('click','.ajaxBox ul li',function(){
            var userid = $(this).data('id');

            $('#posSelect').val($(this).text());
            $('.ajaxBox').slideUp('fast');

            $.ajax({
               url: '<?php echo $this->html->url(array("action" => "omview")); ?>',
               data: {
                id:userid,
                week: '<?php echo $week; ?>',
               },
               cache: false,
               success: function(data) {
                    $('.attendanceContainer').html('');
                    $('.attendanceContainer').html(data);
               },
               type: 'POST'
            });

        });

    });
</script>

<script type="text/javascript">
    $(function() {
        $('.datetimepicker2').datetimepicker({
            pick12HourFormat: true,
            pickSeconds: false,
            pickDate: false
        });

        $('.datetimepicker1').datetimepicker({
            pickTime: false
        });

        $('.attendanceContainer').on('click','.absentChk',function(){
            if($(this).is(':checked')) {
                $('.pField').attr('disabled','disabled');
                $('.hrsWorked').val('');
            } else {
                $('.pField').removeAttr('disabled');
            }
        });
        
        $('.attendanceContainer').on('click','.omitChk',function(){
            if($(this).is(':checked')) {
                $('.omitBox').show();
            } else {
                $('.omitBox').hide();
            }
        });
        
        $('.attendanceContainer').on('click','.relieverChk',function(){
            if($(this).is(':checked')) {
                $('.relieverBox').show();
            } else {
                $('.relieverBox').hide();
            }
        });

        $('.attendanceContainer').on('click','.otChk',function(){
            if($(this).is(':checked')) {
                $('.otBox').show();
            } else {
                $('.otBox').hide();
            }
        });

        $('.hrsWorked').on('input propertychange paste', function() {

        });

        $('.attendanceContainer').on('submit','#addOmviewForm',function(event){

            $.post("<?php echo $this->Html->url(array('action' => 'omproccess')) ?>",$("#addOmviewForm").serialize(), function(data){
                window.location.href = $(location).attr('href');
            });

            event.preventDefault();
            return false;
        });

    });
</script>