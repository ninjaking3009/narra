<div class="widget wred">

    <div class="widget-head">
        <div class="pull-left">Attendance</div>
        <div class="clearfix"></div>
    </div>

    <div class="widget-content tblvs">

        <table class="table  table-bordered ">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Total Basic Hrs.</td>
                    <td>Total OT</td>
                    <td>Total Basic Pay</td>
                    <td>OT Pay</td>
                    <td>COLA</td>
                    <td>SEA</td>
                    <td>Night Pay</td>
                    <td>Legal Hours</td>
                    <td>Legal Pay</td>
                    <td>Omitted Pay</td>
                    <td>Gross Pay</td>
                    <td>SSS Prem.</td>
                    <td>Philhealth</td>
                    <td>SSS Loan</td>
                    <td>Pagibig Loan</td>
                    <td>Calamity Loan</td>
                    <td>Net Pay</td>
                </tr>
            </thead>
            <tbody>



                <?php foreach ($attendances as $key => $value) { ?>
                <tr>
                    <td><?php echo ucfirst($value['User']['lname']).", ".ucfirst($value['User']['fname']) ?></td>
                    <td><?php echo $value['Attendance']['totalWorkHours'] ?></td>
                    <td><?php echo $value['Attendance']['totalOtHours'] ?></td>
                    <td><?php echo $value['Attendance']['totalBasicPay'] ?></td>
                    <td><?php echo $value['Attendance']['totalOtPay'] ?></td>
                    <td><?php echo $value['Attendance']['totalCola'] ?></td>
                    <td><?php echo $value['Attendance']['totalSea'] ?></td>
                    <td><?php echo $value['Attendance']['totalNightPay'] ?></td>
                    <td><?php echo $value['Attendance']['totalLegalHours'] ?></td>
                    <td><?php echo $value['Attendance']['totalLegalPay'] ?></td>
                    <td><?php echo $value['Attendance']['totalOmitPay'] ?></td>
                    <td style="color:#000;font-weight:bold"><?php echo $value['Attendance']['grossPay'] ?></td>
                    <td><?php echo $value['Attendance']['totalSss'] ?></td>
                    <td><?php echo $value['Attendance']['totalPhilhealth'] ?></td>
                    <td><?php echo $value['Attendance']['sss_loan'] ?></td>
                    <td><?php echo $value['Attendance']['pagibig_loan'] ?></td>
                    <td><?php echo $value['Attendance']['calamity_loan'] ?></td>
                    <td style="color:#000;font-weight:bold"><?php echo $value['Attendance']['netPay'] ?></td>
                </tr>
                <?php } ?>

            </tbody>
        </table>

    </div>
</div>
