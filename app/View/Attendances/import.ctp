<!-- Page heading -->
<div class="page-head">
  <!-- Page heading -->
  <h2 class="pull-left">Attendance
    <!-- page meta -->
    <span class="page-meta">Import</span>
  </h2>

  <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
  <div class="container-fluid">
    <div class="row-fluid">
        <div class="attendanceImportWrap">
            <?php echo $this->Form->create('Import',['enctype' => 'multipart/form-data']); ?>
				<div class="attendanceFile">
				    <span>Select File</span>
				    <?php echo $this->Form->input('Import.file',array('label'=>false,'class' => 'input-large', 'required' => 'required','type' => 'file')); ?>
				    <input type="submit" class="btn btn-primary" value="Process Attendance">
				</div>
        	</form>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="row-fluid">
    	<?php if (!isset($data)): ?>
    		<div class="row-fluid">
	            <div class="span12">
	                <div class="widget wgreen">
	                    <div class="widget-head">
	                        <div class="pull-left">Import Logs</div>
	                        <div class="clearfix"></div>
	                    </div>

	                    <div class="widget-content">
	                    </div>
	                </div>
	            </div>
            </div>
    	<?php endif ?>
    </div>
  </div>
</div>

<!-- Matter ends -->