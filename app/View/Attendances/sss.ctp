<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">SSS Report
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="rangeSelectBox">
                <div class="perSb weekRange">
                    <span>Select Month</span>
                    <?php echo $this->Form->input('Date.range',array('options' => $month,'div'=>false,'label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 totalWrap"></div>
        </div>
    </div>
</div>



<!-- Matter ends -->

<script type="text/javascript">
jQuery(document).ready(function($) {

    var month = $('#DateRange').val();
    var curMonth = $('#DateRange').val();

    $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url: '<?php echo $this->html->url(array("action" => "ssstotal")); ?>',
        data: {
            month: month,
        },
        success: function(data) {
            $('.totalWrap').html('');
            $('.totalWrap').html(data);
        },
        type: 'POST'
    });
    
    $('#DateRange').on('change', function() {
        var month1 = this.value;
        curMonth = month1;

        $('.totalWrap').html('<div class="loaderBox"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax({
            url: '<?php echo $this->html->url(array("action" => "ssstotal")); ?>',
            data: {
                month: month1,
            },
            success: function(data) {
                $('.totalWrap').html('');
                $('.totalWrap').html(data);
            },
            type: 'POST'
        });

    });

});
</script>
