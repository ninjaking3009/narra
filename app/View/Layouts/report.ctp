<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
	<?php 

	  echo $this->Html->css(array('bootstrap','print'));
	  
	  echo $this->fetch('css');
	  
	  echo $this->Html->script(array('jquery'));
	  
	  echo $this->fetch('script');

	?>
	<!--[if lt IE 9]><script src="flashcanvas.js"></script><![endif]-->
</head>
<body>

<div class="A4Portrait A4Report">
<div class="header">
	<div class="reportTitle">Narra Man Power Agency</div>
	<span>Candelaria Quezon</span>
</div>
<?php echo $this->fetch('content'); ?>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.followerPrint').click(function(){
		      window.print();
		});
		if($('.showAdvance').is(':checked')) {
			$('.advanceCont').hide();
		}
		$('.showAdvance').on('click',function(){
			if($(this).is(':checked')) {
				$('.advanceCont').hide();
			} else {
				$('.advanceCont').show();
			}
		});
	});
</script>
	
</body>
</html>