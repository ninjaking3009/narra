<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Payroll Slip</title>
	<?php 

	  echo $this->Html->css(array('bootstrap','printstyle'));
	  
	  echo $this->fetch('css');
	  
	  echo $this->Html->script(array('jquery'));
	  
	  echo $this->fetch('script');

	?>
	<!--[if lt IE 9]><script src="flashcanvas.js"></script><![endif]-->
</head>
<body>

<div class="printOptionBox">
	<div class="checkbox">
		<input type="checkbox" name="" class="showAdvance" value="">
		<label>Show/Hide Advance Pay Slip</label>
	</div>
</div>

<div class="A4Portrait">
<?php echo $this->fetch('content'); ?>
</div>

<div class="followerPrint">
	Print
</div>


<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.followerPrint').click(function(){
		      window.print();
		});
		if($('.showAdvance').is(':checked')) {
			$('.advanceCont').hide();
		}
		$('.showAdvance').on('click',function(){
			if($(this).is(':checked')) {
				$('.advanceCont').hide();
			} else {
				$('.advanceCont').show();
			}
		});
	});
</script>
	
</body>
</html>