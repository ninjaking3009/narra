<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Narra Payroll System</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Stylesheets -->
  <?php 

    echo $this->Html->css(array(
    	'bootstrap',
        'font-awesome',
        'jquery-ui',
        'fullcalendar.min',
        'prettyPhoto',
        'rateit',
        'bootstrap-datetimepicker.min',
        'jquery.gritter',
        'jquery.cleditor',
        'bootstrap-toggle-buttons',
        'style',
        'widget',
        'bootstrap-responsive',
        'bluesorter',
        'override',
    ));
    
    echo $this->fetch('css');

    echo $this->Html->script(array(
      'jquery.min',
      'jquery-ui-1.10.2.custom.min'
    ));

  ?>
</head>

<body>

<div class="navbar navbar-fixed-top navbar-inverse">
  <div class="navbar-inner">
    <div class="container">
      <!-- Menu button for smallar screens -->
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
      </a>
      <!-- Site name for smallar screens -->
      <a href="#" class="brand">Narra Payroll System</a>
      <!-- Navigation starts -->
      <div class="nav-collapse collapse">        



        <!-- Links -->
        <ul class="nav pull-right">
          <li class="dropdown pull-right">            
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <!-- <img src="img/user.jpg" alt="" class="nav-user-pic" /> --> Administrator <b class="caret"></b>              
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'logout')) ?>"><i class="icon-off"></i> Logout</a></li>
            </ul>
          </li>
          
        </ul>
      </div>

    </div>
  </div>
</div>


<!-- Main content starts -->

<div class="content">

    <!-- Sidebar -->
    <?php 
      
      echo $this->element('sidebar');
    
    ?>
    <!-- Sidebar ends -->

        <!-- Main bar -->
        <div class="mainbar">
          <?php echo $this->fetch('content'); ?>
        </div>

       <!-- Mainbar ends -->        
       <div class="clearfix"></div>

    </div>
    <!-- Content ends -->

<?php echo $this->element('notification_box') ?>

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

<!-- JS -->
<?php 
  echo $this->Html->script(array(
    'bootstrap',
    'moment.min',
    'jquery-ui.custom.min',
    'fullcalendar.min',
    'jquery.rateit.min',
    'jquery.prettyPhoto',
    ));
?>

<!-- jQuery Float -->
<?php 
  echo $this->Html->script(array(
    'excanvas.min',
    'jquery.flot',
    'jquery.flot.resize',
    'jquery.flot.pie',
    'jquery.flot.stack',
    'jquery.gritter.min',
    'sparklines',
    'jquery.cleditor.min',
    'bootstrap-datetimepicker.min',
    'jquery.toggle.buttons',
    'jquery.maskedinput.min',
    'bootstrap-formhelpers.min',
    'filter',
    'jquery.tablesorter.min',
    'jquery.filtertable.min',
    'custom',
    ));

    echo $this->fetch('script');
  
?>



</body>
</html>