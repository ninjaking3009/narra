<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Stylesheets -->
  <?php 

    echo $this->Html->css(array('bootstrap',
        'font-awesome',
        'bootstrap-responsive',
        'style'
    ));
    
    echo $this->fetch('css');
    echo $this->fetch('script');

  ?>
</head>

<body>

<?php echo $this->fetch('content'); ?>
	
		

<!-- JS -->
<?php 
  echo $this->Html->script(array('jquery','bootstrap'));
?>

</body>
</html>