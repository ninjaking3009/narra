<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Users
    <!-- page meta -->
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">

        <?php echo $this->Session->flash(); ?>

        <div class="row-fluid">

            <div class="span12">

                <div class="widget wgreen">
                    <div class="widget-head">
                        <div class="pull-left">Loan</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">
                        <div class="padd">

                            <!-- Profile form -->

                            <div class="form profile">

                                <?php echo $this->form->create('loan',array('class'=>'form-horizontal')) ?>

                                <div class="control-group">
                                    <label for="name1" class="control-label">Employee ID </label>
                                    <div class="controls">
                                        <?php echo $this->Form->input('User.empid',array('label'=>false,'class' => 'input-large')); ?>
                                    </div>
                                </div>

                                <!-- Buttons -->
                                <div class="form-actions">
                                    <!-- Buttons -->
                                    <?php echo $this->Form->button('Save', array('type' => 'submit','class' => 'btn btn-success')); ?>
                                    <button class="btn" type="reset">Reset</button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
