<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">PhilHealth
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
			<?php echo $this->Session->flash(); ?>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget wgreen">

                    <div class="widget-head">
                        <div class="pull-left">PhilHealth Weekly Bracket</div>
                        <div class="widget-icons pull-right">
                            <a href="<?php echo $this->Html->url(array('action' => 'addw')); ?>"><i class="fa fa-plus"></i> Add Bracket</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align:center">Weekly Salary Credit</th>
                                    <th colspan="2" style="text-align:center"></th>
                                </tr>
                                <tr>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Premium</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($datas as $key=> $value) { ?>
                                <tr>
                                    <td>
                                        <?php echo  $this->Number->currency($value['Philhealth'][ 'from'], ''); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Number->currency($value['Philhealth'][ 'to'], ''); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Number->currency($value['Philhealth'][ 'premium'], ''); ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-mini btn-warning" href="<?php echo $this->Html->url(array('action' => 'editw',$value['Philhealth']['id'])); ?>"><i class="icon-pencil"></i> </a>
                                        <a class="btn btn-mini btn-danger" data-toggle="modal" href="#confirmModal<?php echo $value['Philhealth']['id'] ?>"><i class="icon-remove"></i></a>
                                    
                                        <div id="confirmModal<?php echo $value['Philhealth']['id'] ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Confirm Delete</h3>
                                          </div>
                                          <div class="modal-body">
                                            <p>You are about to completely remove this. Are you sure you want to continue?</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <a class="btn btn-danger" href="<?php echo $this->Html->url(array('action' => 'deletew',$value['Philhealth']['id'])); ?>">Yes, Delete Permanently</a>
                                          </div>
                                        </div>
                                    </td>

                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>



                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
