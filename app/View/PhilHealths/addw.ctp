<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">PhilHealth
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
            <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">


            <div class="span6">

                <div class="widget wgreen">

                    <div class="widget-head">
                        <div class="pull-left">Add SSS Weekly Bracket</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('register',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">From</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.from',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">To</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.to',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">Premium</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.premium',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <?php echo $this->Form->button('Add Bracket', array('type' => 'submit','class' => 'btn btn-success')); ?>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

            <div class="span6">
                <div class="widget wgreen">

                    <div class="widget-head">
                        <div class="pull-left">PhilHealth Weekly Bracket</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <table class="table  table-bordered ">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align:center">Weekly Salary Credit</th>
                                    <th colspan="4" style="text-align:center"></th>
                                </tr>
                                <tr>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Premium</th>
                                </tr>
                            </thead>
                            <tbody>



                                <?php foreach ($datas as $key=> $value) { ?>
                                <tr>
                                    <td>
                                        <?php echo  $this->Number->currency($value['Philhealth'][ 'from'], ''); ?>
                                    </td>
                                    <td>
                                        <?php echo $this->Number->currency($value['Philhealth'][ 'to'], ''); ?>
                                    </td> 
                                    <td>
                                        <?php echo $this->Number->currency($value['Philhealth'][ 'premium'], ''); ?>
                                    </td>
                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>



                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
