<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">PhilHealth
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
            <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">

            <div class="span6">

                <div class="widget wgreen">

                    <div class="widget-head">
                        <div class="pull-left">Edit PhilHealth Monthly Bracket</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('edit',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">From</label>
                                <div class="controls">
                                    <?php echo $this->Form->hidden('Philhealth.id',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                    <?php echo $this->Form->input('Philhealth.from',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">To</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.to',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">MSC</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.msc',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">EE</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.ee',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="name1" class="control-label">ER</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Philhealth.er',array('label'=>false,'class' => 'input-large','type' => 'text','maxlength' => '100', 'required' => 'required')); ?>
                                </div>
                            </div>


                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <?php echo $this->Form->button('Update Bracket', array('type' => 'submit','class' => 'btn btn-success')); ?>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- Matter ends -->
