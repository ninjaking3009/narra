<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Positions
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">

        <?php echo $this->Session->flash(); ?>

            <div class="span6">

                <div class="widget wviolet">

                    <div class="widget-head">
                        <div class="pull-left">Edit Position</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('edit',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">Position</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Role.role',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                    <?php echo $this->Form->hidden('Role.id',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name1" class="control-label">Contract Rate</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Role.per_hour',array('label'=>false,'class' => 'input-large', 'required' => 'required', 'type' => 'text')); ?>
                                </div>
                            </div>
                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <?php echo $this->Form->button('Update Position', array('type' => 'submit','class' => 'btn btn-success')); ?>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
