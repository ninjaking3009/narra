<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">Ceiling
  </h2>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->



<!-- Matter -->

<div class="matter">
    <div class="container-fluid">
        <?php echo $this->Session->flash(); ?>
        <div class="row-fluid">


            <div class="span6">

                <div class="widget wviolet">

                    <div class="widget-head">
                        <div class="pull-left">Update Ceiling</div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="widget-content">

                        <div class="padd">
                            <?php echo $this->form->create('edit',array('class'=>'form-horizontal')) ?>

                            <div class="control-group">
                                <label for="name1" class="control-label">Ceiling</label>
                                <div class="controls">
                                    <?php echo $this->Form->input('Ceiling.ciel',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                    <?php echo $this->Form->hidden('Ceiling.id',array('label'=>false,'class' => 'input-large', 'required' => 'required')); ?>
                                </div>
                            </div>
                            
                            <!-- Buttons -->
                            <div class="form-actions">
                                <!-- Buttons -->
                                <?php echo $this->Form->button('Update Ceiling', array('type' => 'submit','class' => 'btn btn-success')); ?>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!-- Matter ends -->
