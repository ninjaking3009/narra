-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2018 at 05:11 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `narradb`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(99) NOT NULL,
  `user_id` int(99) NOT NULL,
  `date` varchar(255) NOT NULL,
  `week` varchar(99) NOT NULL,
  `month` varchar(99) NOT NULL,
  `year` varchar(99) NOT NULL,
  `is_reliever` int(1) NOT NULL DEFAULT '0',
  `reliever_position` int(99) NOT NULL,
  `reliever_pay` varchar(255) NOT NULL,
  `position` varchar(99) NOT NULL,
  `is_ot` int(1) NOT NULL DEFAULT '0',
  `is_omit` int(1) NOT NULL DEFAULT '0',
  `is_deduct` int(1) NOT NULL DEFAULT '0',
  `omit_realdate` varchar(99) NOT NULL,
  `time_in` varchar(255) NOT NULL,
  `time_out` varchar(255) NOT NULL,
  `hours_work` varchar(99) NOT NULL,
  `night_hours` varchar(99) NOT NULL,
  `night_pay` varchar(99) NOT NULL,
  `ot_work` varchar(99) NOT NULL,
  `ot_pay` varchar(99) NOT NULL,
  `rdlegal_work` varchar(255) NOT NULL,
  `rdlegal_pay` varchar(255) NOT NULL,
  `otrdlegal_work` varchar(255) NOT NULL,
  `otrdlegal_pay` varchar(255) NOT NULL,
  `restday_work` varchar(255) NOT NULL,
  `restday_pay` varchar(255) NOT NULL,
  `is_lastpayroll` int(1) NOT NULL DEFAULT '0',
  `ot_lp_work` varchar(9) NOT NULL,
  `ot_lp_pay` varchar(9) NOT NULL,
  `legal_hours` varchar(99) NOT NULL,
  `ot_legal_work` varchar(99) NOT NULL,
  `ot_legal_pay` varchar(255) NOT NULL,
  `legal_pay` varchar(99) NOT NULL,
  `lastpayroll_hours` varchar(255) NOT NULL,
  `lastpayroll_pay` varchar(255) NOT NULL,
  `special_hours` varchar(99) NOT NULL,
  `special_pay` varchar(99) NOT NULL,
  `omit_hours` int(99) NOT NULL,
  `omit_pay` varchar(99) NOT NULL,
  `deduct_hours` varchar(99) NOT NULL,
  `deduct_basic` varchar(99) NOT NULL,
  `deduct_pay` varchar(99) NOT NULL,
  `is_night` int(1) NOT NULL DEFAULT '0',
  `per_hour` varchar(99) NOT NULL,
  `cola` varchar(99) NOT NULL,
  `sea` varchar(99) NOT NULL,
  `total` varchar(99) NOT NULL,
  `grandtotal` varchar(99) NOT NULL,
  `sss` varchar(99) NOT NULL,
  `philhealth` varchar(99) NOT NULL,
  `pagibig` varchar(99) NOT NULL,
  `is_absent` int(1) NOT NULL DEFAULT '0',
  `is_restday` int(1) NOT NULL DEFAULT '0',
  `is_holiday` int(1) NOT NULL DEFAULT '0',
  `is_legal` int(1) NOT NULL DEFAULT '0',
  `date_added` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `user_id`, `date`, `week`, `month`, `year`, `is_reliever`, `reliever_position`, `reliever_pay`, `position`, `is_ot`, `is_omit`, `is_deduct`, `omit_realdate`, `time_in`, `time_out`, `hours_work`, `night_hours`, `night_pay`, `ot_work`, `ot_pay`, `rdlegal_work`, `rdlegal_pay`, `otrdlegal_work`, `otrdlegal_pay`, `restday_work`, `restday_pay`, `is_lastpayroll`, `ot_lp_work`, `ot_lp_pay`, `legal_hours`, `ot_legal_work`, `ot_legal_pay`, `legal_pay`, `lastpayroll_hours`, `lastpayroll_pay`, `special_hours`, `special_pay`, `omit_hours`, `omit_pay`, `deduct_hours`, `deduct_basic`, `deduct_pay`, `is_night`, `per_hour`, `cola`, `sea`, `total`, `grandtotal`, `sss`, `philhealth`, `pagibig`, `is_absent`, `is_restday`, `is_holiday`, `is_legal`, `date_added`) VALUES
(1, 18, '1514674800', '01', '01', '2018', 0, 0, '', '6', 0, 0, 0, '', '', '', '8', '', '0', '0', '0', '', '0', '', '0', '', '0', 0, '', '', '', '', '', '', '', '', '', '', 0, '0', '', '', '', 0, '510', '', '', '510', '510', '', '', '', 0, 0, 0, 0, '2018-09-16 04:35:01 am'),
(2, 18, '1515279600', '02', '01', '2018', 0, 0, '', '6', 0, 0, 0, '', '', '', '8', '', '0', '0', '0', '', '0', '', '0', '', '0', 0, '', '', '', '', '', '', '', '', '', '', 0, '0', '', '', '', 0, '510', '', '', '510', '510', '', '', '', 0, 0, 0, 0, '2018-09-16 04:36:35 am'),
(9, 18, '1515884400', '03', '01', '2018', 1, 15, '67.76', '6', 0, 0, 0, '', '', '', '8', '', '0', '0', '0', '', '0', '', '0', '', '0', 0, '', '', '', '', '', '', '', '', '', '', 0, '0', '', '', '', 0, '510', '', '', '510', '510', '', '', '', 0, 0, 0, 0, '2018-09-17 02:07:31 am'),
(10, 3, '1515884400', '03', '01', '2018', 0, 0, '0', '2', 1, 0, 0, '', '', '', '8', '', '0', '4', '318.75', '', '0', '', '0', '', '0', 0, '', '', '', '', '', '', '', '', '', '', 0, '0', '', '', '', 0, '510', '', '', '510', '510', '', '', '', 0, 0, 0, 0, '2018-09-17 02:13:45 am'),
(11, 3, '1515798000', '03', '01', '2018', 0, 0, '0', '2', 0, 1, 0, '1515884400', '', '', '8', '', '0', '0', '0', '', '0', '', '0', '', '0', 0, '', '', '', '', '', '', '', '', '', '', 8, '510', '', '', '', 0, '510', '', '', '0', '510', '', '', '', 0, 0, 0, 0, '2018-09-17 02:15:59 am');

-- --------------------------------------------------------

--
-- Table structure for table `attendance_imports`
--

CREATE TABLE `attendance_imports` (
  `id` int(99) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_added` varchar(255) NOT NULL,
  `user_id` int(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ceilings`
--

CREATE TABLE `ceilings` (
  `id` int(1) NOT NULL,
  `ciel` int(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ceilings`
--

INSERT INTO `ceilings` (`id`, `ciel`) VALUES
(1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `deductions`
--

CREATE TABLE `deductions` (
  `id` int(99) NOT NULL,
  `user_id` int(99) NOT NULL,
  `day` varchar(99) NOT NULL,
  `week` int(99) NOT NULL,
  `month` int(99) NOT NULL,
  `year` int(99) NOT NULL,
  `sss` varchar(99) NOT NULL,
  `philhealth` varchar(99) NOT NULL,
  `pagibig` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deductions`
--

INSERT INTO `deductions` (`id`, `user_id`, `day`, `week`, `month`, `year`, `sss`, `philhealth`, `pagibig`) VALUES
(1, 18, '', 1, 1, 2018, '20', '25', '0'),
(2, 18, '', 2, 1, 2018, '20', '25', '0'),
(3, 18, '', 3, 1, 2018, '30', '25', '10.2'),
(4, 3, '', 3, 1, 2018, '33.3', '25', '10.2');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(99) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date_added` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `status`, `date_added`) VALUES
(1, 'SPD DEPT.', 1, ''),
(2, 'DCN-FINISHING DEPT.', 1, ''),
(3, 'DRILLING DEPT.', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(99) NOT NULL,
  `name` varchar(99) NOT NULL,
  `date` varchar(99) NOT NULL,
  `is_legal` int(1) NOT NULL DEFAULT '0',
  `is_special` int(1) NOT NULL,
  `date_added` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `name`, `date`, `is_legal`, `is_special`, `date_added`) VALUES
(3, 'National Day', '1424991600', 1, 0, '2015-02-27 03:28:07 am'),
(4, 'Labor Day', '1428184800', 0, 1, '2015-03-20 10:19:47 pm'),
(5, 'leg hold', '1420930800', 1, 0, '2015-03-21 11:48:20 pm'),
(6, 'last hold', '1421017200', 1, 0, '2015-03-21 11:49:06 pm'),
(7, 'spec hold', '1421190000', 0, 1, '2015-03-21 11:49:22 pm'),
(8, 'Labor Day', '1428876000', 1, 0, '2015-05-15 07:56:41 am'),
(9, 'bryan birthday', '1441922400', 1, 0, '2015-08-04 01:56:17 am'),
(10, 'Quezon Day', '1439935200', 0, 1, '2015-09-01 01:17:04 am'),
(11, 'Aquino Day', '1440108000', 0, 1, '2015-09-01 01:17:35 am'),
(12, 'National Heroes Day', '1440972000', 1, 0, '2015-09-10 12:41:29 am'),
(13, 'All Saints Day', '1446332400', 0, 1, '2015-11-15 11:43:30 pm'),
(14, 'Hermano Puli', '1446591600', 0, 1, '2015-11-15 11:43:48 pm'),
(15, 'End of Ramadan', '1443132000', 1, 0, '2015-11-16 01:33:49 am'),
(16, 'Chrismast Day', '1453676400', 1, 0, '2016-02-03 01:09:23 am'),
(17, 'Chrismast Eve', '1453590000', 0, 1, '2016-02-03 01:10:49 am'),
(18, 'Legal 1', '1458774000', 1, 0, '2016-05-06 08:21:39 pm'),
(19, 'Legal 2', '1458860400', 1, 0, '2016-05-06 08:22:11 pm'),
(20, 'Special', '1458946800', 0, 1, '2016-05-06 08:22:47 pm');

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` int(99) NOT NULL,
  `user_id` int(99) NOT NULL,
  `sss_loan` varchar(99) NOT NULL,
  `calamity_loan` varchar(99) NOT NULL,
  `pagibig_loan` varchar(99) NOT NULL,
  `totalsss_loan` varchar(99) NOT NULL,
  `totalpagibig_loan` varchar(99) NOT NULL,
  `totalcalamity_loan` varchar(99) NOT NULL,
  `advance` varchar(99) NOT NULL,
  `date_added` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mdeductions`
--

CREATE TABLE `mdeductions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `gross` int(11) NOT NULL,
  `basic_income` int(11) NOT NULL,
  `sss` int(11) NOT NULL,
  `sss_msc` varchar(99) NOT NULL,
  `sss_ecc` varchar(99) NOT NULL,
  `sss_ee` int(11) NOT NULL,
  `sss_er` int(11) NOT NULL,
  `sss_total` int(11) NOT NULL,
  `philhealth` int(11) NOT NULL,
  `phil_ee` int(11) NOT NULL,
  `phil_er` int(11) NOT NULL,
  `phil_msc` int(11) NOT NULL,
  `phil_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mdeductions`
--

INSERT INTO `mdeductions` (`id`, `user_id`, `month`, `year`, `gross`, `basic_income`, `sss`, `sss_msc`, `sss_ecc`, `sss_ee`, `sss_er`, `sss_total`, `philhealth`, `phil_ee`, `phil_er`, `phil_msc`, `phil_total`) VALUES
(1, 3, 1, 2018, 1339, 0, 0, '1500', '10', 55, 121, 175, 0, 100, 100, 8000, 200);

-- --------------------------------------------------------

--
-- Table structure for table `ot_exccesses`
--

CREATE TABLE `ot_exccesses` (
  `id` int(99) NOT NULL,
  `user_id` int(99) NOT NULL,
  `hours_work` varchar(255) NOT NULL,
  `per_hour` varchar(255) NOT NULL,
  `ot_hours` varchar(99) NOT NULL,
  `ot_pay` varchar(99) NOT NULL,
  `cola` varchar(255) NOT NULL,
  `sea` varchar(255) NOT NULL,
  `date` varchar(99) NOT NULL,
  `week` varchar(99) NOT NULL,
  `month` varchar(99) NOT NULL,
  `year` varchar(99) NOT NULL,
  `date_added` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paidloans`
--

CREATE TABLE `paidloans` (
  `id` int(99) NOT NULL,
  `user_id` int(99) NOT NULL,
  `week` varchar(99) NOT NULL,
  `year` varchar(99) NOT NULL,
  `sss_loan` varchar(99) NOT NULL,
  `calamity_loan` varchar(99) NOT NULL,
  `pagibig_loan` varchar(99) NOT NULL,
  `advance` varchar(99) NOT NULL,
  `date_added` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `philhealths`
--

CREATE TABLE `philhealths` (
  `id` int(99) NOT NULL,
  `from` varchar(99) NOT NULL,
  `to` varchar(99) NOT NULL,
  `premium` varchar(99) NOT NULL,
  `msc` varchar(99) NOT NULL,
  `er` varchar(99) NOT NULL,
  `ee` varchar(99) NOT NULL,
  `ecc` varchar(99) NOT NULL,
  `total` varchar(99) NOT NULL,
  `date_added` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `philhealths`
--

INSERT INTO `philhealths` (`id`, `from`, `to`, `premium`, `msc`, `er`, `ee`, `ecc`, `total`, `date_added`) VALUES
(1, '0', '199', '25', '', '', '', '', '', ''),
(2, '200', '799', '25', '', '', '', '', '', ''),
(3, '800', '1099', '25', '', '', '', '', '', ''),
(4, '1100', '1299', '25', '', '', '', '', '', ''),
(5, '1300', '1599', '25', '', '', '', '', '', ''),
(6, '1600', '1999', '25', '', '', '', '', '', ''),
(7, '2000', '2099', '25', '', '', '', '', '', ''),
(8, '2100', '2599', '25', '', '', '', '', '', ''),
(9, '2600', '2999', '25', '', '', '', '', '', ''),
(10, '3000', '99999', '50', '', '', '', '', '', ''),
(14, '0', '8999.99', '', '8000', '100', '100', '', '200', ''),
(15, '9000', '9999.99', '', '9000', '112.50', '112.50', '', '225', ''),
(16, '10000', '10999.99', '', '10000', '125', '125', '', '250', ''),
(17, '11000', '11999.99', '', '11000', '137.50', '137.50', '', '275', ''),
(18, '12000', '12999.99', '', '12000', '150', '150', '', '300', ''),
(19, '13000', '13999.99', '', '13000', '162.50', '162.50', '', '325', ''),
(20, '14000', '14999.99', '', '14000', '175', '175', '', '350', ''),
(21, '15000', '15999.99', '', '15000', '187.50', '187.50', '', '375', ''),
(22, '16000', '16999.99', '', '16000', '200', '200', '', '400', ''),
(23, '17000', '17999.99', '', '17000', '212.50', '212.50', '', '425', ''),
(24, '18000', '18999.99', '', '18000', '225', '225', '', '450', ''),
(25, '19000', '19999.99', '', '19000', '237.50', '237.50', '', '475', ''),
(26, '20000', '20999.99', '', '20000', '250', '250', '', '500', ''),
(27, '21000', '21999.99', '', '21000', '262.50', '262.50', '', '525', ''),
(28, '22000', '22999.99', '', '22000', '275', '275', '', '550', ''),
(29, '23000', '23999.99', '', '23000', '287.50', '287.50', '', '575', ''),
(30, '24000', '24999.99', '', '24000', '300', '300', '', '600', ''),
(31, '25000', '25999.99', '', '25000', '312.50', '312.50', '', '625', ''),
(32, '26000', '26999.99', '', '26000', '325', '325', '', '650', ''),
(33, '27000', '27999', '', '27000', '337.50', '337.50', '', '675', ''),
(34, '28000', '28999.99', '', '28000', '350', '350', '', '700', ''),
(35, '29000', '29999.99', '', '29000', '362.50', '362.50', '', '725', ''),
(36, '30000', '30999.99', '', '30000', '375', '375', '', '750', ''),
(37, '31000', '31999.99', '', '31000', '387.50', '387.50', '', '775', ''),
(38, '32000', '32999.99', '', '32000', '400', '400', '', '800', ''),
(39, '33000', '33999.99', '', '33000', '412.50', '412.50', '', '825', ''),
(40, '34000', '34999.99', '', '34000', '425', '425', '', '850', ''),
(41, '35000', '99999', '', '35000', '437.50', '437.50', '', '875', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `per_hour` varchar(11) NOT NULL,
  `date_added` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `per_hour`, `date_added`, `status`) VALUES
(2, 'Micro Analyst - SPD', '510', '', 1),
(3, 'Special Analyst - A', '500', '', 1),
(4, 'Sanitary Inspector', '380.5', '', 1),
(5, 'Shift Mechanics', '338.5', '', 1),
(6, 'SPD Chem Analyst', '510', '', 1),
(7, 'SPD Lab Assistant', '348.5', '', 1),
(8, 'Boiler', '316.5', '', 1),
(9, 'Hygiene Checker - A', '342.5', '', 1),
(10, 'Hygiene Checker - B', '333.75', '', 1),
(11, 'Shift Mechanic - A', '338.5', '', 1),
(12, 'Shift Mechanic - B', '330.15', '', 1),
(13, 'SPD Analyst', '510', '', 1),
(14, 'Chem Supervisor', '481.47', '', 1),
(15, 'Micro Supervisor', '577.76', '', 1),
(16, 'SPD Clerk', '390.5', '', 1),
(17, 'Q.A Clerk - B', '368.85', '', 1),
(18, 'SPD Q.A - A', '342.5', '', 1),
(19, 'SPD Q.A - B', '333.75', '', 1),
(20, 'Special Analyst - B', '452.55', '', 1),
(21, 'SPD Lab Aide II', '332.5', '', 1),
(22, 'Utility ', '316.5', '', 1),
(23, 'Warehouse man ', '420', '', 1),
(24, 'Warehouse Clerk ', '381.5', '', 1),
(25, 'Forklift Operator ', '335.5', '', 1),
(26, 'Warehouse Crew ', '355.5', '', 1),
(27, 'SPD Crew - Green ', '316.5', '', 1),
(28, 'SPD Crew - Yellow', '316.5', '', 1),
(29, 'SPD Crew - Red ', '316.5', '', 1),
(30, 'Blancherman ', '335.5', '', 1),
(31, 'Boiler Mechanic ', '338.5', '', 1),
(32, 'Boiler Tender ', '390', '', 1),
(33, 'DCn Chem Analyst ', '510', '', 1),
(34, 'DCN Special Chem Analyst ', '452.55', '', 1),
(35, 'DCN Tester', '325.5', '', 1),
(36, 'Dryerman ', '360.5', '', 1),
(37, 'Grinderman ', '348.5', '', 1),
(38, 'Hygiene Checker ', '342.5', '', 1),
(39, 'Janitor ', '316.5', '', 1),
(40, 'Janitress ', '316.5', '', 1),
(41, 'Packing ', '316.5', '', 1),
(42, 'QA Inspector ', '342.5', '', 1),
(43, 'Safety Officer ', '390.5', '', 1),
(44, 'Sanitary Inspector ', '380.5', '', 0),
(45, 'Scanner ', '316.5', '', 1),
(46, 'Shift Electrician ', '338.5', '', 1),
(47, 'Shift Mechanic ', '338.5', '', 0),
(48, 'SO2 Analyst', '332.5', '', 1),
(49, 'Warehouse Crew - B ', '316.5', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sses`
--

CREATE TABLE `sses` (
  `id` int(99) NOT NULL,
  `from` varchar(99) NOT NULL,
  `to` varchar(99) NOT NULL,
  `premium` varchar(99) NOT NULL,
  `msc` varchar(99) NOT NULL,
  `er` varchar(99) NOT NULL,
  `ee` varchar(99) NOT NULL,
  `ecc` varchar(99) NOT NULL,
  `total` varchar(99) NOT NULL,
  `date_added` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sses`
--

INSERT INTO `sses` (`id`, `from`, `to`, `premium`, `msc`, `er`, `ee`, `ecc`, `total`, `date_added`) VALUES
(2, '0', '199', '20', '', '', '', '', '', ''),
(3, '200', '799', '30', '', '', '', '', '', ''),
(4, '800', '1099', '33.3', '', '', '', '', '', ''),
(5, '1100', '1299', '45', '', '', '', '', '', ''),
(6, '1300', '1599', '50', '', '', '', '', '', ''),
(7, '1600', '1999', '75', '', '', '', '', '', ''),
(8, '2000', '2099', '80', '', '', '', '', '', ''),
(9, '2100', '2599', '85', '', '', '', '', '', ''),
(10, '2600', '2999', '90', '', '', '', '', '', ''),
(12, '1000', '1249.99', '', '1000', '83.70', '36.30', '10', '120', ''),
(13, '1250', '1749.99', '', '1500', '120.50', '54.50', '10', '175', ''),
(14, '1750', '2249.99', '', '2000', '157.30', '72.70', '10', '230', ''),
(15, '2250', '2749.99', '', '2500', '194.20', '90.80', '10', '285', ''),
(16, '2750', '3249.99', '', '3000', '231.00', '109.00', '10', '340', ''),
(17, '3250', '3749.99', '', '3500', '267.80', '127.20', '10', '395', ''),
(18, '3750', '4249.99', '', '4000', '304.70', '145.30', '10', '450', ''),
(19, '4250', '4749.99', '', '4500', '341.50', '163.50', '10', '505', ''),
(20, '4750', '5249.99', '', '5000', '378.30', '181.70', '10', '560', ''),
(21, '5250', '5749.99', '', '5500', '415.20', '199.80', '10', '615', ''),
(22, '5750', '6249.99', '', '6000', '452.00', '218.00', '10', '670', ''),
(23, '6250', '6749.99', '', '6500', '488.80', '236.20', '10', '725', ''),
(24, '6750', '7249.99', '', '7000', '525.70', '254.30', '10', '780', ''),
(25, '7250', '7749.99', '', '7500', '562.50', '272.50', '10', '835', ''),
(26, '7750', '8249.99', '', '8000', '599.30', '290.70', '10', '890', ''),
(27, '8250', '8749.99', '', '8500', '636.20', '308.80', '10', '945', ''),
(28, '8750', '9249.99', '', '9000', '673.00', '327.00', '10', '1000', ''),
(29, '9250', '9749.99', '', '9500', '709.80', '345.20', '10', '1055', ''),
(30, '9750', '10249.99', '', '10000', '746.70', '363.30', '10', '1110', ''),
(31, '10250', '10749.99', '', '10500', '783.50', '381.50', '10', '1165', ''),
(32, '10750', '11249.99', '', '11000', '820.30', '399.70', '10', '1220', ''),
(33, '11750', '11749.99', '', '11500', '857.20', '417.80', '10', '1275', ''),
(34, '11750', '12249.99', '', '12000', '894.00', '436.00', '10', '1330', ''),
(35, '12250', '12749.99', '', '12500', '930.80', '454.20', '10', '1385', ''),
(36, '12750', '13249.99', '', '13000', '967.70', '472.30', '10', '1440', ''),
(37, '13250', '13749.99', '', '13500', '1004.50', '490.50', '10', '1495', ''),
(38, '13750', '14249.99', '', '14000', '1041.30', '508.70', '10', '1550', ''),
(39, '14250', '14749.99', '', '14500', '1078.20', '526.80', '10', '1605', ''),
(40, '14750', '15249.99', '', '15000', '1135.00', '545.00', '30', '1680', ''),
(41, '15250', '15749.99', '', '15500', '1171.80', '563.20', '30', '1735', ''),
(42, '15750', '16249.99', '', '16000', '1208.70', '581.30', '30', '1790', ''),
(43, '3000', '9999', '100', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`username`, `password`, `status`) VALUES
('admin', 'admin123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `empid` varchar(99) NOT NULL,
  `department_id` int(99) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `rest_day` varchar(255) NOT NULL,
  `sss` varchar(255) NOT NULL,
  `philhealth` varchar(255) NOT NULL,
  `pagibig` varchar(255) NOT NULL,
  `tin` varchar(255) NOT NULL,
  `date_hired` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `empid`, `department_id`, `fname`, `lname`, `mname`, `gender`, `address`, `contact`, `role_id`, `rest_day`, `sss`, `philhealth`, `pagibig`, `tin`, `date_hired`, `birthday`, `status`, `date`) VALUES
(3, '', 2, 'Alexie', 'Bastida', '', 'female', '', '', 2, 'Tue', '', '', '443', '', '02-14-2015', '02-14-2015', 1, '2015-02-14 03:04:15 am'),
(4, '', 2, 'Jojilyn Rose', 'Mabunting', 'S', 'female', '', '', 2, 'Thu', '11-1111111-1', '22-222222222-2', '33333333', '444-444-444-444', '01-27-2015', '02-14-2015', 1, '2015-02-14 03:05:42 am'),
(5, '', 1, 'Cristina', 'Manalo', '', 'female', '', '', 2, 'Sun', '', '', '1112', '', '02-05-2015', '02-20-2015', 1, '2015-02-14 03:23:12 am'),
(6, '', 1, 'Marychell', 'Ona', '', 'female', '', '', 2, 'Sun', '', '', '112515', '', '02-05-2015', '', 1, '2015-02-14 03:24:57 am'),
(7, '', 0, 'Laarnie', 'Salido', '', 'female', '', '', 2, 'Fri', '77747', '62363211', '6272723', '63112', '02-05-2015', '', 1, '2015-02-14 03:25:39 am'),
(8, '', 0, 'Elizabeth', 'Granada', '', 'female', '', '', 3, 'Sun', '74372', '627237', '72723727', '16163723', '02-07-2015', '', 1, '2015-02-14 03:26:33 am'),
(9, '', 2, 'Adrian', 'Manalo', '', 'male', '', '', 4, 'Tue', '', '', '4684351', '', '02-05-2015', '', 1, '2015-02-14 03:27:31 am'),
(10, '', 0, 'julius', 'pemilar', '', 'male', '', '', 4, 'Sat', '553215', '515125125', '12512512621621', '1612612612', '01-31-2015', '', 1, '2015-02-14 03:54:24 am'),
(11, '', 0, 'bernal', 'pereras', '', 'male', '', '', 2, 'Mon', '61616161', '616126126', '361616', '13613631613', '02-16-2015', '', 1, '2015-02-14 03:55:15 am'),
(12, '', 0, 'mark anthony', 'pereras', '', 'male', '', '', 4, 'Wed', '', '', '12512512521', '512-512-512-512', '02-24-2015', '02-26-2015', 1, '2015-02-14 03:55:48 am'),
(13, '', 0, 'argyllie nico', 'pinca', '', 'male', '', '', 4, 'Mon', '123124124', '21-412412421-4', '12412421421', '412-412-412-421', '02-04-2015', '', 1, '2015-02-14 04:02:42 am'),
(14, '', 0, 'zenaida', 'Ricohermoso', '', 'female', '', '', 4, 'Sun', '1561616', '16-161626364-7', '671631613613', '613-671-363-163', '02-18-2015', '', 1, '2015-02-14 04:03:34 am'),
(15, '', 0, 'Jr. Norberto', 'Garcia', '', 'male', '', '', 5, 'Mon', '631261612', '22-525526361-2', '12512512521512', '512-512-532-511', '02-17-2015', '', 1, '2015-02-14 04:05:49 am'),
(16, '', 0, 'Jayson', 'Garchitorena', '', 'male', '', '', 5, 'Mon', '51-2512512-5', '51-251251251-2', '123123123', '321-312-312-252', '02-25-2015', '', 1, '2015-02-14 04:06:23 am'),
(17, '', 0, 'nestor', 'lindo', '', 'male', '', '', 5, 'Mon', '51-5125125-1', '21-512521515-2', '1261261612612512', '125-125-125-125', '02-25-2015', '', 1, '2015-02-14 04:14:10 am'),
(18, '', 2, 'april', 'Abastillas', '', 'female', '', '', 6, 'Mon', '12-6126126-2', '12-612612612-5', '125125125125', '125-125-215-125', '02-18-2015', '', 1, '2015-02-14 04:16:51 am'),
(19, '', 0, 'kimberlie', 'elerjerio', '', 'male', '', '', 6, 'Fri', '15-6126126-1', '61-261261251-2', '25125125125', '125-125-126-126', '02-20-2015', '02-18-2015', 1, '2015-02-14 04:17:32 am'),
(20, '', 0, 'alexon christopher', 'francisco', '', 'male', '', '', 6, 'Fri', '11-3616165-1', '61-261263737-1', '1612612626', '216-126-126-124', '02-14-2015', '', 1, '2015-02-14 04:18:14 am'),
(21, '', 1, 'charmaine rassele', 'noves', '', 'male', '', '', 2, 'Fri', '51-2616125-1', '25-125125161-2', '1265125612612', '126-126-125-125', '02-18-2015', '', 1, '2015-02-14 04:19:05 am'),
(22, '', 0, 'miriam', 'carurucan', '', 'female', '', '', 7, 'Thu', '51-5212512-6', '61-261261261-6', '16161261126', '126-126-126-162', '02-26-2015', '', 1, '2015-02-14 04:22:56 am'),
(23, '', 0, 'lorelie', 'de leon', '', 'male', '', '', 7, 'Wed', '51-5251251-2', '12-512512512-5', '5125125125', '124-241-251-251', '02-27-2015', '', 1, '2015-02-14 04:23:35 am'),
(24, '', 2, 'Rufino', 'Cruzat', '', 'male', '', '', 8, 'Fri', '11-1111111-1', '11-111111111-1', '111111111', '111-111-111-111', '05-06-2016', '', 1, '2016-05-06 08:44:29 pm'),
(25, '', 2, 'George', 'Evangelista', '', 'male', '', '', 8, 'Fri', '11-1111111-1', '22-222222222-2', '', '333-333-333-333', '05-06-2016', '', 1, '2016-05-06 08:45:16 pm'),
(26, '', 2, 'John Kevin', 'Evangelista', '', 'male', '', '', 8, 'Fri', '', '', '', '', '05-06-2016', '', 1, '2016-05-06 08:46:01 pm'),
(27, '', 2, 'Rommel', 'Evangelista', '', 'male', '', '', 8, 'Tue', '', '', '', '', '05-06-2016', '', 1, '2016-05-06 08:46:30 pm'),
(28, '', 2, 'Romulo', 'Malolos', '', 'male', '', '', 8, 'Sun', '', '', '', '', '05-06-2016', '', 1, '2016-05-06 08:47:03 pm'),
(29, '', 2, 'June Francis', 'Pasumbal', '', 'male', '', '', 8, 'Fri', '', '', '', '', '05-06-2016', '', 1, '2016-05-06 08:48:02 pm'),
(30, '', 2, 'Leonilo', 'Ulan', '', 'male', '', '', 8, 'Fri', '', '', '', '', '05-06-2016', '', 1, '2016-05-06 08:48:29 pm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_imports`
--
ALTER TABLE `attendance_imports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ceilings`
--
ALTER TABLE `ceilings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deductions`
--
ALTER TABLE `deductions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdeductions`
--
ALTER TABLE `mdeductions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ot_exccesses`
--
ALTER TABLE `ot_exccesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paidloans`
--
ALTER TABLE `paidloans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `philhealths`
--
ALTER TABLE `philhealths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sses`
--
ALTER TABLE `sses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attendance_imports`
--
ALTER TABLE `attendance_imports`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ceilings`
--
ALTER TABLE `ceilings`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deductions`
--
ALTER TABLE `deductions`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mdeductions`
--
ALTER TABLE `mdeductions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ot_exccesses`
--
ALTER TABLE `ot_exccesses`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paidloans`
--
ALTER TABLE `paidloans`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `philhealths`
--
ALTER TABLE `philhealths`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `sses`
--
ALTER TABLE `sses`
  MODIFY `id` int(99) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
